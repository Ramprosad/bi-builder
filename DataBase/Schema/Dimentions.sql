USE BI_Builder
GO

----------------------------------------------------------------
--Customer Grouping
---------------------------------------------------------------
CREATE TABLE BIClientGroup 
(
	ClientCWCode	VARCHAR(100) CONSTRAINT PK_BIClientGroup_ClientCWCode PRIMARY KEY, 
	ClientName		VARCHAR(500), 
	GroupName		VARCHAR(500)

)
---------------------------------------------------------------
--1  BIClient
----------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE NAME='BIClient')
BEGIN
	
	CREATE TABLE BIClient
	(
	
		ConsigneeEDICode	VARCHAR(255) NOT NULL CONSTRAINT PK_BIClient_ConsigneeEDICode PRIMARY KEY,
		ClientName			VARCHAR(1000) NOT NULL, 
		ClientGroupName		VARCHAR(1000)

	)


END 

---------------------------------------------------------------
--2  BISupplier
----------------------------------------------------------------

CREATE TABLE BISupplier 
(	
	SupplierCode					VARCHAR(255) NOT NULL,
	SupplierConsigneeEDICode		Varchar(255) NOT NULL,
	ClientConsigneeEDICode			varchar(255) NOT NULL,
	SupplierName					VARCHAR(100) NOT NULL,
	CONSTRAINT PK_Supplier_ConsigneeEDICode_ClientConsigneeEDICode PRIMARY KEY (SupplierConsigneeEDICode, ClientConsigneeEDICode)
)


---------------------------------------------------------------
--3  BITransport
----------------------------------------------------------------

CREATE TABLE BITransport
(
	TransPortCode			VARCHAR(100) CONSTRAINT PK_Transport_TransPortCode PRIMARY KEY,
	TransportName			VARCHAR(1000)

)

---------------------------------------------------------------
--4  BICountry
----------------------------------------------------------------
CREATE TABLE BICountry 
(

	CountryCode CHAR(2) CONSTRAINT PK_Country_CountryCode PRIMARY KEY,
	CountryName VARCHAR(500)

)

---------------------------------------------------------------
--5  BIPort
----------------------------------------------------------------

CREATE TABLE BIPort
(
	
	PortCode		VARCHAR(10) NOT NULL,
	CountryCode		CHAR(10),
	PortName		VARCHAR(1000),
	CityCode		VARCHAR(10), 
	CityName		VARCHAR(1000), 
	CountryName		VARCHAR(1000), 
	CONSTRAINT PK_Port_PortCode PRIMARY KEY (PortCode)
)

---------------------------------------------------------------
--6  BIDeliveryMode
----------------------------------------------------------------

CREATE TABLE BIDeliveryMode
(
	DeliveryModeCode		VARCHAR(100) CONSTRAINT PK_BIDeliveryMode_DeliveryModeCode PRIMARY KEY, 
	DeliveryModeName		VARCHAR(1000) 
)


---------------------------------------------------------------
--7  BIHOB
----------------------------------------------------------------


CREATE TABLE BIHOB
(
	HOBName						VARCHAR(500), 
	ClientConsigneeEDICode		VARCHAR(50), 
	CONSTRAINT PK_BIHOB_HOBName_ClientConsigneeEDICode PRIMARY KEY (HOBName, ClientConsigneeEDICode)

)


---------------------------------------------------------------
--8  Vessel
----------------------------------------------------------------

CREATE TABLE BIVessel
(
	VesselCode VARCHAR(100) NOT NULL CONSTRAINT PK_BIVessel_VesselCode PRIMARY KEY ,
	VesselName VARCHAR(1000)

)

---------------------------------------------------------------
--10  Carrier
----------------------------------------------------------------

CREATE TABLE BICarrier
(
	CarrierCode VARCHAR(100) NOT NULL CONSTRAINT PK_BICarrier_CarrierCode PRIMARY KEY ,
	CarrierName VARCHAR(1000)

)

---------------------------------------------------------------
--9  City 
----------------------------------------------------------------


CREATE TABLE [dbo].[BICity]
(
	CityCode		CHAR(5) CONSTRAINT PK_BICity_CityCode PRIMARY KEY, 
	[CityName]		[nvarchar](255) NOT NULL,
	[CountryCode]	[nvarchar](255) NOT NULL

) 


--------------------------------------------------------------------
--10 DateValue
--------------------------------------------------------------------
CREATE TABLE BIDate
(
	DateValue DATE CONSTRAINT PK_BIDate_DateValue PRIMARY KEY, 
	[YEAR]		INT, 
	[MONTH]		INT ,
	[DAY]		INT 

)

------------------------------------------------------
--Container Size
-------------------------------------------------------

CREATE TABLE BIContainerSize
(

	SizeCode			VARCHAR(30) NOT NULL CONSTRAINT PK_BIContainerSize_SizeCode PRIMARY KEY,
	SizeDescription		VARCHAR(100), 
	Volume				VARCHAR(100)		

)

----------------------------------------------------------
--MileStone
---------------------------------------------------------

CREATE TABLE BIMileStone
(
	ClientCWEDICode		VARCHAR(100) NOT NULL,
	MileStoneKey		VARCHAR(100) NOT NULL,
	IsStandard			BIT, 
	CONSTRAINT PK_BIMileStone_ClientCWEDICode_MileStoneKey PRIMARY KEY (ClientCWEDICode,MileStoneKey)

)

CREATE TABLE BIMileStoneFact
(
	Customer			VARCHAR(100) NOT NULL,
	MileStoneKey		VARCHAR(100) NOT NULL,
	OrderNumber			VARCHAR(100) NOT NULL,
	Lot					VARCHAR(10),
	MileStoneDate		DATETIME,
	CONSTRAINT		PK_BIMileStoneFact_ClientCWEDICode_MileStoneKey_OrderNumber_Lot PRIMARY KEY
	(ClientCWEDICode, MileStoneKey, OrderNumber, Lot)
)




SELECT  top 100 
	*
FROM 
	[CW_ONE].[cw1test1db].[dbo].OrgHeader OH
WHERE 
	OH_IsActive=1 --AND OH_IsConsignee=1
	and Oh_FullName like 'POOLE LIGHTING LTD%'

	sp_help '[dbo].[OrderAudit_Test]'

	select top 10 * from [dbo].[CargoWise]


	SELECT * FROM  City 


	SELECT Distinct RC_CODE FROM CargoWise