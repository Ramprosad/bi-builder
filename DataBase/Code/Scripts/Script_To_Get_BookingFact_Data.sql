

USE [BI_Builder]
GO



	IF NOT EXISTS (SELECT 1 FROM sys.objects where name ='BIBookingFact')
	BEGIN

		CREATE TABLE BIBookingFact
		(
			Customer				 VARCHAR(100) NOT NULL, 
			BookingNumber			 VARCHAR(100) NOT NULL,
			BookingSubmitedDate		 DATE,
			TotalBookedCBM			 FLOAT, 
			TotalBookedQuantity		 FLOAT, 
			CONSTRAINT PK_BIBookingFact_Customer_BookingNumber PRIMARY KEY (Customer, BookingNumber)

		)
	END 

GO


INSERT INTO BIBookingFact(Customer, BookingNumber, BookingSubmitedDate, TotalBookedCBM, TotalBookedQuantity)
SELECT 
	C.ConsigneeEDICode										AS Customer,
	BH.Bk_No												AS BookingNumber, 
	CAST(isnull(BH.submit_date, '1900-01-01')AS DATE)		AS BookingSubmitedDate, 
	SUM(BI.CBM)												AS TotalBookedCBM, 
	SUM(BI.INPKG)											AS TotalBookedQuantity
FROM 
	Ligentix4.dbo.BookingHeader BH 
	JOIN Ligentix4.dbo.BookingItem BI ON BH.BookingId=BI.BookingId
	JOIN Ligentix4.dbo.Client C ON BH.ClientId=C.ClientId
	JOIN [BI_Builder].dbo.BIClient  BC ON C.ConsigneeEDICode=BC.ConsigneeEDICode
WHERE 
	EXISTS 
	(
		SELECT 1 FROM [BI_Builder].DBO.ShipOrder SO JOIN Ligentix4.dbo.Shipment S ON 
		SO.ShipmentId=S.ShipmentId 
		WHERE 
			BH.CW_REF=S.UniqueReference
	)
	AND BH.Bk_No!=''
GROUP BY 
	C.ConsigneeEDICode,
	BH.Bk_No, 
	BH.submit_date
	
	
	--EXEC SP_RENAME 'BIBookingFact' , 'BIBookingFact_BKP'
	--EXEC SP_RENAME 'BIBookingFact_NEW' , 'BIBookingFact'


	--SELECT * FROM BIBookingFact ORDER BY BookingSubmitedDate ASC

	--SELECT * FROM Ligentix4.dbo.BookingHeader WHERE BK_NO='271218265'

	--SELECT TOP 10 * FROM Ligentix4.dbo.Shipment_PurchaseOrder 

	--select * from Ligentix4.dbo.BookingHeader WHERE bk_no='251218257'
	--select   OrderNumber, Lot, * from Ligentix4.dbo.BookingItem where BookingId=387624

	--SELECT * FROM [Ligentix_Purchase_Orders].dbo.OrderHeader WHERE OrderNumber
	--IN 
	--(
	--	'ORD00180516_01',
	--	'ORD00180518_01',
	--	'ORD00180588_01',
	--	'ORD00180590_01',
	--	'ORD00184340_01',
	--	'ORD00184621_01',
	--	'ORD00184721_01',
	--	'ORD00184966_01',
	--	'ORD00184983_01',
	--	'ORD00185037_01'
	--)

	--select OrderNumber,  * from Ligentix4.dbo.BookingItem where OrderNumber IS NULL 

	--select * from [Ligentix_Purchase_Orders].dbo.OrderLine WHERE LineId 
	--in
	--(
	--	19257478,
	--	19207491,
	--	19257504,
	--	19207492
	--)