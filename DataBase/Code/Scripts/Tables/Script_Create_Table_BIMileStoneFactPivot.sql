USE [BI_Builder]
GO


/****** Object:  Table [dbo].[BIMileStoneFactPivot]    Script Date: 06-27-2019 13:09:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BIMileStoneFactPivot_New]
(
	[Customer] [varchar](50)						NOT NULL,
	[OrderNumber] [varchar](50)						NOT NULL,
	[LOT] [varchar](10)								NOT NULL,
	[EX_WORKS_ACT] [date]							NULL,
	[EX_WORKS_EST] [datetime]						NULL,
	[EX_WORKS_ORG] [date]							NULL,
	[SUPPLIER_BOOKED_GOODS_ACT] [date]				NULL,
	[SUPPLIER_BOOKED_GOODS_EST] [datetime]			NULL,
	[SUPPLIER_BOOKED_GOODS_ORG] [date]				NULL,
	[RequiredHandover_ACT] [date]					NULL,
	[RequiredHandover_EST] [datetime]				NULL,
	[RequiredHandover_ORG] [date]					NULL,
	[ETD_ACT] [date]								NULL,
	[ETD_EST] [datetime]							NULL,
	[ETD_ORG] [date]								NULL,
	[ETA_ACT] [date]								NULL,
	[ETA_EST] [datetime]							NULL,
	[ETA_ORG] [date]								NULL,
	[CYContainerGatedDeparturePort_ACT] [date]		NULL,
	[CYContainerGatedDeparturePort_EST] [datetime]  NULL,
	[CYContainerGatedDeparturePort_ORG] [date]		NULL,
	[ProductionDate_ACT] [date]						NULL,
	[ProductionDate_EST] [datetime]					NULL,
	[ProductionDate_ORG] [date]						NULL,
	[ATPInspection_ACT] [date]						NULL,
	[ATPInspection_EST] [datetime]					NULL,
	[ATPInspection_ORG] [date]						NULL,
	[SOConfirmedByLigentiaOriginOffice_ACT] [date]  NULL,
	[SOConfirmedByLigentiaOriginOffice_EST] [datetime]  NULL,
	[SOConfirmedByLigentiaOriginOffice_ORG] [date]		NULL,
	[BookingWindowClosedDate_ACT] [date]				NULL,
	[BookingWindowClosedDate_EST] [datetime]			NULL,
	[BookingWindowClosedDate_ORG] [date]				NULL,
	[CargoReadyDate_ACT] [date]							NULL,
	[CargoReadyDate_EST] [datetime]						NULL,
	[CargoReadyDate_ORG] [date]							NULL,
	[PhysicalHandoverOfTheGoods_ACT] [date]				NULL,
	[PhysicalHandoverOfTheGoods_EST] [datetime]			NULL,
	[PhysicalHandoverOfTheGoods_ORG] [date]				NULL,
	[RequiredDeliveryDate_ACT] [date]					NULL,
	[RequiredDeliveryDate_EST] [datetime]				NULL,
	[RequiredDeliveryDate_ORG] [date]					NULL,
	[SailingDateOrigin_ACT] [date]						NULL,
	[SailingDateOrigin_EST] [datetime]					NULL,
	[SailingDateOrigin_ORG] [date]						NULL,
	[CFSLOADED_ACT] [date]								NULL,
	[CFSLOADED_EST] [datetime]							NULL,
	[CFSLOADED_ORG] [date]								NULL,
	[POBooked_ACT] [date]								NULL,
	[POBooked_EST] [datetime]							NULL,
	[POBooked_ORG] [date]								NULL,
	[CustomClearanceDate_ACT] [date]					NULL,
	[CustomClearanceDate_EST] [datetime]				NULL,
	[CustomClearanceDate_ORG] [date]					NULL,
	[MerchSAApproval_ACT] [date]						NULL,
	[MerchSAApproval_EST] [datetime]					NULL,
	[MerchSAApproval_ORG] [date]						NULL,
	[NOA_ACT] [date]									NULL,
	[NOA_EST] [datetime]								NULL,
	[NOA_ORG] [date]									NULL,
	[SFD_ACT] [date]									NULL,
	[SFD_EST] [datetime]								NULL,
	[SFD_ORG] [date]									NULL,
	[Approved_ACT] [date]								NULL,
	[Approved_EST] [datetime]							NULL,
	[Approved_ORG] [date]								NULL,
	[BookingWindowOpen_ACT] [date]						NULL,
	[BookingWindowOpen_EST] [datetime]					NULL,
	[BookingWindowOpen_ORG] [date]						NULL,
	[GoldSealApproval_ACT] [date]						NULL,
	[GoldSealApproval_EST] [datetime]					NULL,
	[GoldSealApproval_ORG] [date]						NULL,
	[ILCSAApproval_ACT] [date]							NULL,
	[ILCSAApproval_EST] [datetime]						NULL,
	[ILCSAApproval_ORG] [date]							NULL,
	[ContainerReleasedCR_ACT] [date]					NULL,
	[ContainerReleasedCR_EST] [datetime]				NULL,
	[ContainerReleasedCR_ORG] [date]					NULL,
	[DCbookingmade_ACT] [date]							NULL,
	[DCbookingmade_EST] [datetime]						NULL,
	[DCbookingmade_ORG] [date]							NULL,
	[DCbookingconfirmed_ACT] [date]						NULL,
	[DCbookingconfirmed_EST] [datetime]					NULL,
	[DCbookingconfirmed_ORG] [date]						NULL, 
	PORaised_ACT	[datetime]							NULL, 
	PORaised_EST	[datetime]							NULL, 
	PORaised_ORG	[datetime]							NULL		
	CONSTRAINT PK_BIMileStoneFactPivot_New_Customer_PONumber_Lot PRIMARY KEY(Customer,OrderNumber,LOT)  
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


