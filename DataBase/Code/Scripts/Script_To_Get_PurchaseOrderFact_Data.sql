
USE [BI_Builder]
GO


IF NOT EXISTS (SELECT 1 FROM Sys.objects WHERE name ='BIPurchaseOrder')
BEGIN
	 CREATE TABLE BIPurchaseOrder
	 (
			Customer					VARCHAR(100) NOT NULL, 
			PONumber					VARCHAR(100) NOT NULL,
			lot							VARCHAR(100) NOT NULL,
			Supplier					VARCHAR(100) NOT NULL,
			OrderQty					FLOAT,
			UnitVolume					FLOAT,
			UnitWeight					FLOAT, 
			OrderDate					DATE,
			[OrderStatus]				VARCHAR(100) NOT NULL,
			CONSTRAINT PK_BIPurchaseOrder_Customer_PONumber_lot PRIMARY KEY (Customer, PONumber, lot)
	 )
 END 
---------------------------------------------------------
--EXTRACT FOR PO FACT 
---------------------------------------------------------

		DECLARE @ClientId INT =17799
		DECLARE @JLPClient INT =2
		--INSERT INTO BIPurchaseOrder(Customer, PONumber, lot, Supplier, OrderQty, UnitVolume, UnitWeight, OrderDate, [OrderStatus])
		SELECT DISTINCT 
				LC.ConsigneeEDICode								AS Customer,
				M.OrderNumber,
				A.LOT, 
				ISNULL(BSUP.SupplierCode, 'N/A')				AS Supplier,
				SUM(A.OrderQty)									AS OrderQty,
				ISNULL(
					ISNULL(
						CASE C.LigClientId
							WHEN @ClientId THEN SUM(A.UnitVolume)
							ELSE SUM(ISNULL(A.UnitVolume, 1) * A.OrderQty) 
						END
					,M.TotalVolume)
				, 0)											AS UnitVolume,
				ISNULL(SUM(A.UnitWeight), 0)					AS UnitWeight, 
				M.OrderDate										AS OrderDate,
				ISNULL(NULLIF(M.Status, ''), 'N/A')				AS [OrderStatus] --As suggested by Theren 			
			FROM 
				Ligentix_Purchase_Orders.dbo.OrderHeader M WITH(NOLOCK)
				JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH(NOLOCK) ON M.HeaderID=A.HeaderID AND M.IsLatestVersion = 1 
				JOIN Ligentix_Purchase_Orders.dbo.ClientFeed CF WITH(NOLOCK) ON M.OrderFeedId=CF.FeedId
				JOIN Ligentix_Purchase_Orders.dbo.Client C WITH(NOLOCK) ON cf.ClientID = C.ClientID
				JOIN Ligentix4.dbo.Client LC WITH(NOLOCK) ON C.LigClientId=LC.ClientId
				JOIN [BI_Builder].dbo.BIClient BIC WITH(NOLOCK) ON LC.ConsigneeEDICode=BIC.[ConsigneeEDICode]
				INNER JOIN 
				(
					SELECT DISTINCT OrderNumber,ClientId,Lot FROM [BI_Builder].dbo.ShipOrder WITH(NOLOCK) --List of shipped order for last n number of days. 
				)SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=C.LigClientId 
				AND (CASE SH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(SH.Lot,'1') END) = (CASE A.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)	
				left JOIN Ligentix_Purchase_Orders.dbo.Client SUP WITH(NOLOCK) ON M.SupplierClientId=SUP.ClientId
				left join Ligentix4.dbo.Client SC WITH(NOLOCK) ON SUP.LigClientId=SC.ClientId
				left JOIN BI_Builder.dbo.BISupplier BSUP WITH(NOLOCK) ON SC.ConsigneeEDICode=BSUP.SupplierCode
			WHERE
				M.IsLatestVersion = 1
				AND M.IsCancelled=0
				AND A.IsCancelled=0
				AND ((LC.ClientId=@JLPClient AND  M.OrderDate IS NOT NULL) OR LC.ClientId!=@JLPClient)
				and A.[LineStatus]!='CANCELLED'
				AND ((LC.ConsigneeEDICode	 IN ('SOAKCOGBNTN', 'WAITROGBLON') AND  A.UnitVolume != 0)
				OR LC.ConsigneeEDICode	 NOT IN('SOAKCOGBNTN', 'WAITROGBLON'))
			GROUP BY 
				LC.ConsigneeEDICode, 
				M.OrderNumber,
				A.LOT, 
				M.Status,
				C.LigClientId,
				M.TotalVolume, 
				BSUP.SupplierCode,
				SC.ConsigneeEDICode,	
				OrderDate 
			ORDER BY 
				LC.ConsigneeEDICode, 
				M.OrderNumber, 
				A.LOT
		

		--SELECT TOP 10 * FROM [Ligentix4].dbo.BookingHeader
