


--TARGET VOOLUME FORMULA (20GOH=28  20GP=28  20RE=28  40GOH=58 40GP=58 40HC=66 40HCGOH=66 40RE=58 45GOH=75 45HC=75 45HCGOH= 75)

USE [BI_Builder]
GO
	IF NOT EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE NAME ='BIConsoleContainerTeuFact_New')
	BEGIN 

		CREATE TABLE BIConsoleContainerTeuFact_New
		(
			ConsoleId					VARCHAR(100), 
			ContainerNumber				VARCHAR(100), 
			ContainerSizeCode			VARCHAR(50),
			TEU							FLOAT, 
			ActualVolume				FLOAT, 
			TargetVolume				FLOAT
			CONSTRAINT PK_BIConsoleContainerTeuFact_New_ConsoleId_ContainerNumber PRIMARY KEY (ConsoleId, ContainerNumber)
		)
	END 
	GO


	IF OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL
	BEGIN
		DROP TABLE #TEMP  
	END  
	
	CREATE TABLE #TEMP
	(
		OA_PK UNIQUEIDENTIFIER,
		OA_OH UNIQUEIDENTIFIER,

		CONSTRAINT PK_TEMP03 PRIMARY KEY CLUSTERED 
		(
			OA_PK ASC
		)
	)
	INSERT INTO #TEMP
	SELECT OA_PK, OA_OH FROM openquery([CW_ONE],'SELECT OA_PK, OA_OH FROM [cw1test1db].[dbo].OrgAddress')

	DECLARE @CutOffDate DATE
	DECLARE @CutOffStartDate VARCHAR(25)

	SET @CutOffDate = DATEADD(DAY, -500, GETDATE())

	SET @CutOffStartDate = '01-' + Right('0' + CAST(MONTH(@CutOffDate) AS VARCHAR), 2) + '-' + CONVERT(VARCHAR,YEAR(@CutOffDate))


		INSERT INTO BIConsoleContainerTeuFact_New(ConsoleId,  ContainerNumber, ContainerSizeCode, TEU, ActualVolume, TargetVolume)
		SELECT   
		JK.JK_PK				AS ConsoleId,
		JC.JC_ContainerNum		AS ContainerNumber,
		CASE WHEN RC_CODE IN ('20GOH', '20PBF')
			 THEN '20GOH'

			 WHEN RC_CODE IN ('20FR','20FR','20FT 1M OH','20FT 30 OH','20G0','20G1','20GP','20H0','20HAZ','20HC','20HCSUB','20K0','20K1','20K2',
						 '20K8','20NOR','20OT','20P1','20T0','20T1','20T2','20T3','20T4','20T5','20T6','20T7','20T8','22B0','22G1','22H0',
						 '22K0','22K1','22K2','22K8','22P1','22P3','22P7','22P8','22P9','22R1','22R7','22R9','22S1','22T0','22T1','22T2',
						 '22T3','22T4','20T5','22T6','22T7','22T8','22U1','22U6','22V0','22V2','22V3','25G0','25K0','25K1','25K2','25K8',
						 '26G0','26H0','28T8','28U1','28V0','29P0')
			 THEN '20GP'

			 WHEN RC_CODE ='20RE'
			 THEN '20RE'

			 WHEN RC_CODE IN('40GOH','40HCGOHSUB','40PBF')
			 THEN '40GOH'

			 WHEN RC_CODE IN('40DC','40FR','40FT 1M OH','40FT 30 OH','40GP','40HAZ','40K0','40K1','40K2','40K8','40NOR','40OT',
							 '42G0','42G1','42H0','42K0','42K1','42K2','42K8','42P1','42P3','42P6','42P8','42P9','42R1','42R3',
							 '42R9','42S1','42T2','42T5','42T6','42T8','42U1','42U6' )
			 THEN '40GP'
	
			 WHEN RC_CODE IN('40HC',' 40HC RF','40HCHAZ','40HCNOR','40HCOT','40HCPW','40HCSUB')
			 THEN '40HC'

			 WHEN RC_CODE IN('40HCGOH','40HPBF','40HPBFSUB')
			 THEN '40HCGOH'

			 WHEN RC_CODE IN('40RE','40REHC')
			 THEN '40RE'

	
			 WHEN RC_CODE ='45FT GOH'
			 THEN '45GOH'

			 WHEN RC_CODE IN('45B3','45G0','45G1','45HC','45HCSUB','45K0','45K1','45K2','45K8','45P3','45P8','45R1','45R9',
							 '45U1','45U6','46H0','48K8','48T8','49P0' )
			 THEN '45HC'
			 WHEN RC_CODE ='45PBF' 
			 THEN '45HCGOH'
		ELSE RC_CODE END								AS ContainerSizeCode,
		RC.RC_TEU										AS TUE, 
		SUM(JO.JO_ActualVolume)							AS ActualVolume,	
		NULL											AS TargetVolume
	FROM
		[CW_ONE].[cw1test1db].[dbo].JobOrderLine JO WITH(NOLOCK) 
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobOrderHeader JD WITH(NOLOCK)  On JD.JD_PK=JO.JO_JD
		INNER JOIN #TEMP OA WITH(NOLOCK)  On JD.JD_OA_BuyerAddress=OA.OA_PK
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].OrgHeader OH WITH(NOLOCK)  On OH.OH_PK=OA.OA_OH --JD_OH_BuyerAddress
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobShipment JS WITH(NOLOCK)  On JS.JS_PK=JD.JD_JS
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConShipLink JN WITH(NOLOCK)  On JN.JN_JS=JS.JS_PK
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsol JK WITH(NOLOCK)  On JK.JK_PK=JN.JN_JK		
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobContainer JC WITH(NOLOCK)  ON JC.JC_JK=	JK.JK_PK
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefContainer RC WITH(NOLOCK)  On JC.JC_RC=RC.RC_PK AND JC.JC_JK=JK.JK_PK AND JC.JC_ContainerNum=JO.JO_ContainerNumber
		JOIN BI_Builder.DBO.BIShipmentFact SF WITH(NOLOCK)  ON JS.JS_UniqueConsignRef COLLATE SQL_Latin1_General_CP1_CI_AS=SF.ShipmentNumber COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE
		JS.JS_SystemCreateTimeUtc >=@CutOffStartDate
		--AND JK.JK_PK='000565A7-EDC2-4C8F-929A-C7D4F4A8FD52' AND JC.JC_ContainerNum='FSCU5031765'
	GROUP BY 
		JK.JK_PK,			
		JC.JC_ContainerNum,	
		RC.RC_Code,			
		RC.RC_TEU		

	UPDATE BIConsoleContainerTeuFact_New
	SET TargetVolume=CASE ContainerSizeCode
		WHEN '20GOH'	THEN 28
		WHEN '20GP'		THEN 28
		WHEN '20RE'		THEN 28
		WHEN '40GOH'	THEN 58
		WHEN '40GP'		THEN 58
		WHEN '40HC'		THEN 66
		WHEN '40HCGOH'	THEN 66
		WHEN '40RE'		THEN 58
		WHEN '45GOH'	THEN 75
		WHEN '45HC'		THEN 75
		WHEN '45HCGOH'	THEN 75
		ELSE				0
		END						

		--SELECT TOP 10 * FROM [CW_ONE].[cw1test1db].[dbo].JobShipment
		--SELECT  JK_UniqueConsignRef, COUNT(1) FROM [CW_ONE].[cw1test1db].[dbo].JobConsol
		--GROUP BY JK_UniqueConsignRef
		--HAVING COUNT(1) > 1
		--SELECT  JK_PK, COUNT(1) FROM [CW_ONE].[cw1test1db].[dbo].JobConsol
	--GROUP BY 
	--	JK.JK_PK,			
	--	JC.JC_ContainerNum	,
	--	CF.ContainerSizeCode,
	--	RC.RC_TEU,			
	--	CF.ActualVolume	
	--HAVING COUNT(1) > 1

		--select TOP 100 * from BIConsoleContainerTeuFact
		--drop table #TEMP

	--select  
	--	ConsoleId,
	--	ContainerNumber,
	--	count(1) 
	--from CON_TEMP
	--group by 
	--	ConsoleId,
	--	ContainerNumber		
	--having count(1) > 1

	--SELECT * FROM  BIConsoleContainerTeuFact
	--WHERE ConsoleId='8ED0E546-B839-48BF-9956-AC5E896AD608'
	--AND ContainerNumber='FCIU7352723'

	--SELECT * FROM CON_TEMP WHERE ConsoleId='8ED0E546-B839-48BF-9956-AC5E896AD608'
	--AND ContainerNumber='FCIU7352723'

	--SELECT * FROM sys.columns where name like 'Volume%'

	--select count(1) from  BIConsoleContainerTeuFact
	--select count(1) from  BIConsoleContainerTeuFact_new

	--EXEC SP_RENAME 'BIConsoleContainerTeuFact', 'BIConsoleContainerTeuFact_BKP'
	--EXEC SP_RENAME 'BIConsoleContainerTeuFact_New', 'BIConsoleContainerTeuFact'

	