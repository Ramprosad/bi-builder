
--ALTER TABLE BIShipmentFact
--DROP CONSTRAINT PK_BIShimentFact_Customer_ShipmentNumber_New

USE [BI_Builder]
GO
	IF NOT EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE NAME ='BIShipmentFact')
	BEGIN 

		CREATE TABLE BIShipmentFact
		(
			Customer					VARCHAR(100), 
			ShipmentModeCode			VARCHAR(100),  
			ShippedCBM					FLOAT, 
			ShippedQty					FLOAT, 
			ShipWeight					FLOAT, 
			ETA							DATE, 
			ETD							DATE, 
			POD							VARCHAR(100), 	
			ShipmentNumber				VARCHAR(100),
			TransportationMethodCode	VARCHAR(100),
			FirstContainerNumber		VARCHAR(100), 
			FirstContainerSize			VARCHAR(100), 
			POL							VARCHAR(100),	
			Vessel						VARCHAR(500), 
			Voyage						VARCHAR(100),
			CarrierCode					VARCHAR(100),	
			BookingNumber				VARCHAR(100), 
			ShippedTEU					FLOAT, 
			ColnsoleId					VARCHAR(100), 
			CONSTRAINT PK_BIShimentFact_Customer_ShipmentNumber PRIMARY KEY (Customer, ShipmentNumber)
		)
	END 
	GO


	IF OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL
	BEGIN
		DROP TABLE #TEMP  
	END  
	
	CREATE TABLE #TEMP
	(
		OA_PK UNIQUEIDENTIFIER,
		OA_OH UNIQUEIDENTIFIER,

		CONSTRAINT PK_TEMP03 PRIMARY KEY CLUSTERED 
		(
			OA_PK ASC
		)
	)
	INSERT INTO #TEMP
	SELECT OA_PK, OA_OH FROM openquery([CW_ONE],'SELECT OA_PK, OA_OH FROM [cw1test1db].[dbo].OrgAddress')

	DECLARE @CutOffDate DATE
	DECLARE @CutOffStartDate VARCHAR(25)

	SET @CutOffDate = DATEADD(DAY, -500, GETDATE())

	SET @CutOffStartDate = '01-' + Right('0' + CAST(MONTH(@CutOffDate) AS VARCHAR), 2) + '-' + CONVERT(VARCHAR,YEAR(@CutOffDate))

	INSERT INTO BIShipmentFact
	(
		Customer, ShipmentModeCode, ShippedCBM, ShippedQty, ShipWeight, ETA, ETD, POD, ShipmentNumber,
		TransportationMethodCode, FirstContainerNumber, FirstContainerSize, POL, Vessel, Voyage, CarrierCode, 
		BookingNumber, ShippedTEU, ColnsoleId
	)

	SELECT DISTINCT
		SHIP.Customer														AS Customer,
		ISNULL((
			SELECT TOP 1 ISNULL(NULLIF(JC.JC_DeliveryMode, ''), 'N/A') 
			FROM 
				[CW_ONE].[cw1test1db].[dbo].JobContainer JC 
			WHERE 
				JC.JC_JK=JK_PK 
				AND JC.JC_ContainerNum=FirstContainerNumber 
		), 'N/A')															AS ShipmentModeCode,						
		SHIP.ShippedCBM														AS ShippedCBM,
		SHIP.ShippedQty														AS ShippedQty,
		SHIP.ShipWeight														AS ShipWeight,
		ISNULL(SHIP.ETA, '2019-01-01')										AS ETA,
		ISNULL(SHIP.ETD, '2019-01-01')										AS ETD,						
		SHIP.POD															AS POD,
		SHIP.ShipmentNumber													AS ShipmentNumber,
		ISNULL(SHIP.TransportationMethodCode, 'N/A')						AS TransportationMethodCode,
		SHIP.FirstContainerNumber											AS FirstContainerNumber,
		CASE WHEN FirstContainerSize IN ('20GOH', '20PBF')
			 THEN '20GOH'

			 WHEN FirstContainerSize IN ('20FR','20FR','20FT 1M OH','20FT 30 OH','20G0','20G1','20GP','20H0','20HAZ','20HC','20HCSUB','20K0','20K1','20K2',
						 '20K8','20NOR','20OT','20P1','20T0','20T1','20T2','20T3','20T4','20T5','20T6','20T7','20T8','22B0','22G1','22H0',
						 '22K0','22K1','22K2','22K8','22P1','22P3','22P7','22P8','22P9','22R1','22R7','22R9','22S1','22T0','22T1','22T2',
						 '22T3','22T4','20T5','22T6','22T7','22T8','22U1','22U6','22V0','22V2','22V3','25G0','25K0','25K1','25K2','25K8',
						 '26G0','26H0','28T8','28U1','28V0','29P0')
			 THEN '20GP'

			 WHEN FirstContainerSize ='20RE'
			 THEN '20RE'

			 WHEN FirstContainerSize IN('40GOH','40HCGOHSUB','40PBF')
			 THEN '40GOH'

			 WHEN FirstContainerSize IN('40DC','40FR','40FT 1M OH','40FT 30 OH','40GP','40HAZ','40K0','40K1','40K2','40K8','40NOR','40OT',
							 '42G0','42G1','42H0','42K0','42K1','42K2','42K8','42P1','42P3','42P6','42P8','42P9','42R1','42R3',
							 '42R9','42S1','42T2','42T5','42T6','42T8','42U1','42U6' )
			 THEN '40GP'
	
			 WHEN FirstContainerSize IN('40HC',' 40HC RF','40HCHAZ','40HCNOR','40HCOT','40HCPW','40HCSUB')
			 THEN '40HC'

			 WHEN FirstContainerSize IN('40HCGOH','40HPBF','40HPBFSUB')
			 THEN '40HCGOH'

			 WHEN FirstContainerSize IN('40RE','40REHC')
			 THEN '40RE'

	
			 WHEN FirstContainerSize ='45FT GOH'
			 THEN '45GOH'

			 WHEN FirstContainerSize IN('45B3','45G0','45G1','45HC','45HCSUB','45K0','45K1','45K2','45K8','45P3','45P8','45R1','45R9',
							 '45U1','45U6','46H0','48K8','48T8','49P0' )
			 THEN '45HC'
		ELSE FirstContainerSize
		END																	AS FirstContainerSize,
		ISNULL((
				SELECT TOP 1 ISNULL(JA.JA_RL_NKPortOfLoading, 'N/A') 
				FROM  [CW_ONE].[cw1test1db].[dbo].JobConsolTransport JW WITH(NOLOCK)
				Left Join  [CW_ONE].[cw1test1db].[dbo].JobSailing JX WITH(NOLOCK) On JX.JX_PK=JW.JW_JX
				Left Join  [CW_ONE].[cw1test1db].[dbo].JobVoyOrigin JA WITH(NOLOCK) On JA.JA_PK=JX.JX_JA
				Left Join  [CW_ONE].[cw1test1db].[dbo].JobVoyage JV WITH(NOLOCK) On JV.JV_PK=JA.JA_JV
				WHERE 
					JW.JW_ParentGUID = JK_PK AND
					JW.JW_TransportType = 'MAI ' 
		), 'N/A')														AS POL,
		ISNULL(NULLIF((
				SELECT TOP 1 ISNULL(JV.JV_RV_NKVessel,JW.JW_Vessel) 
				FROM  [CW_ONE].[cw1test1db].[dbo].JobConsolTransport JW WITH(NOLOCK)
				Left Join  [CW_ONE].[cw1test1db].[dbo].JobSailing JX WITH(NOLOCK) On JX.JX_PK=JW.JW_JX
				Left Join  [CW_ONE].[cw1test1db].[dbo].JobVoyOrigin JA WITH(NOLOCK) On JA.JA_PK=JX.JX_JA
				Left Join  [CW_ONE].[cw1test1db].[dbo].JobVoyage JV WITH(NOLOCK) On JV.JV_PK=JA.JA_JV
				WHERE 
					JW.JW_ParentGUID = JK_PK AND
					JW.JW_TransportType = 'MAI ' 
		),''), 'N/A')														AS Vessel,
		ISNULL(NULLIF((
			SELECT TOP 1 ISNULL(JV.JV_VoyageFlight, 'N/A') 
			FROM  [CW_ONE].[cw1test1db].[dbo].JobConsolTransport JW WITH(NOLOCK)
			Left Join  [CW_ONE].[cw1test1db].[dbo].JobSailing JX WITH(NOLOCK) On JX.JX_PK=JW.JW_JX
			Left Join  [CW_ONE].[cw1test1db].[dbo].JobVoyOrigin JA WITH(NOLOCK) On JA.JA_PK=JX.JX_JA
			Left Join  [CW_ONE].[cw1test1db].[dbo].JobVoyage JV WITH(NOLOCK) On JV.JV_PK=JA.JA_JV
			WHERE 
				JW.JW_ParentGUID = JK_PK 
				AND JW.JW_TransportType = 'MAI ' 
		), ''), 'N/A')														AS Voyage, 
		ISNULL((
			SELECT TOP 1 OH_Code OH2 
			FROM 
				[CW_ONE].[cw1test1db].dbo.Orgheader OH2 WITH(NOLOCK)
				JOIN #TEMP OA2 ON OA2.OA_OH=OH2.OH_PK
			WHERE 
				OA2.OA_PK = JK_OA_ShippingLineAddress 
		), 'N/A')												AS CarrierCode,
		ISNULL(nullif(SHIP2.BookingNumber, ''), 'N/A')			AS BookingNumber, 
		TEU														AS ShippedTEU, 
		ConsoleId												AS ConsoleId
	--INTO TEMP_SHIPMENT
	FROM
		(
		SELECT  DISTINCT 
				OH_CODE											AS Customer,
				SUM(JO_ActualVolume)							AS ShippedCBM,
				SUM(JO_QtyReceived)								AS ShippedQty,
				SUM(JO_ActualWeight)							AS ShipWeight,
				JS_E_Arv										AS ETA,
				JS_E_DEP										AS ETD,									
				JS_RL_NKDestination								AS POD,
				MAX(RC_Code)									AS FirstContainerSize,
				JS.JS_UniqueConsignRef							AS ShipmentNumber,
				CASE  JK.JK_TransportMode
				WHEN 'RAI' THEN 'RAIL'
				WHEN 'ROA' THEN 'ROAD'
				ELSE JK.JK_TransportMode END					AS TransportationMethodCode,
				MAX(JO_ContainerNumber)							AS FirstContainerNumber,
				JK.JK_PK										AS JK_PK, 
				JK_OA_ShippingLineAddress						AS JK_OA_ShippingLineAddress,
				TEU.RC_TEU										AS TEU, 
				JK.JK_PK										AS ConsoleId
			FROM
				[CW_ONE].[cw1test1db].[dbo].JobOrderLine JO WITH(NOLOCK) 
				INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobOrderHeader JD WITH(NOLOCK)  On JD.JD_PK=JO.JO_JD
				INNER JOIN #TEMP OA WITH(NOLOCK)  On JD.JD_OA_BuyerAddress=OA.OA_PK
				INNER JOIN [CW_ONE].[cw1test1db].[dbo].OrgHeader OH WITH(NOLOCK)  On OH.OH_PK=OA.OA_OH --JD_OH_BuyerAddress
				INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobShipment JS WITH(NOLOCK)  On JS.JS_PK=JD.JD_JS
				INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConShipLink JN WITH(NOLOCK)  On JN.JN_JS=JS.JS_PK
				INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsol JK WITH(NOLOCK)  On JK.JK_PK=JN.JN_JK		
				INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobContainer JC WITH(NOLOCK)  ON JC.JC_JK=	JK.JK_PK
				INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefContainer RC WITH(NOLOCK)  On JC.JC_RC=RC.RC_PK AND JC.JC_JK=JK.JK_PK AND JC.JC_ContainerNum=JO.JO_ContainerNumber
				--INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsolTransport JW WITH(NOLOCK) ON JW.JW_TransportType = 'MAI' AND JW.JW_ParentGUID = JK.JK_PK 
				--INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefUNLOCO UN WITH(NOLOCK)  ON UN.RL_Code = JS.JS_RL_NKOrigin
				--INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobSailing JX WITH(NOLOCK)  On JX.JX_PK=JW.JW_JX
				--INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobVoyOrigin JA WITH(NOLOCK)  On JA.JA_PK=JX.JX_JA
				LEFT JOIN 
				(
					SELECT JS_UniqueConsignRef, SUM(RC_TEU) AS RC_TEU
					FROM 
					(
						SELECT DISTINCT 
							JO.JO_ContainerNumber,
							JS.JS_UniqueConsignRef,
							RC.RC_TEU
						FROM
							[CW_ONE].[cw1test1db].[dbo].JobOrderLine JO WITH(NOLOCK) 
							INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobOrderHeader JD WITH(NOLOCK)  On JD.JD_PK=JO.JO_JD
							INNER JOIN #TEMP OA WITH(NOLOCK)  On JD.JD_OA_BuyerAddress=OA.OA_PK
							INNER JOIN [CW_ONE].[cw1test1db].[dbo].OrgHeader OH WITH(NOLOCK)  On OH.OH_PK=OA.OA_OH --JD_OH_BuyerAddress
							INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobShipment JS WITH(NOLOCK)  On JS.JS_PK=JD.JD_JS
							INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConShipLink JN WITH(NOLOCK)  On JN.JN_JS=JS.JS_PK
							INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsol JK WITH(NOLOCK)  On JK.JK_PK=JN.JN_JK		
							INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobContainer JC WITH(NOLOCK)  ON JC.JC_JK=	JK.JK_PK
							INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefContainer RC WITH(NOLOCK)  On JC.JC_RC=RC.RC_PK AND JC.JC_JK=JK.JK_PK AND JC.JC_ContainerNum=JO.JO_ContainerNumber
						WHERE
							--JS.JS_UniqueConsignRef='S00559085'
							JS.JS_SystemCreateTimeUtc >=@CutOffStartDate
					)X
					GROUP BY 
						JS_UniqueConsignRef

				)TEU ON JS.JS_UniqueConsignRef=TEU.JS_UniqueConsignRef
			WHERE 
				--JS.JS_UniqueConsignRef='S00559085'
				JS.JS_SystemCreateTimeUtc >=@CutOffStartDate
			GROUP BY 
				OH_CODE, 
				--C_DeliveryMode, 
				JS_E_Arv,
				JS_E_DEP,				
				JS_RL_NKDestination,		
				OH_FullName,				
				--RC_Code,					
				JS.JS_UniqueConsignRef,
				JK.JK_TransportMode,		
				--JO_ContainerNumber, 
				JK.JK_PK, 
				JK.JK_OA_ShippingLineAddress, 
				TEU.RC_TEU, 
				JK.JK_PK	
				

		)SHIP
		JOIN BI_Builder.dbo.BIClient C ON SHIP.Customer  COLLATE Latin1_General_CI_AS =C.ConsigneeEDICode COLLATE Latin1_General_CI_AS
		LEFT JOIN 
		(
			SELECT BH.BookingNumber, SH.UniqueReference, C2.ConsigneeEDICode AS Customer
			FROM
				Ligentix4.dbo.Shipment SH 
				JOIN Ligentix4.dbo.Client C2 ON  C2.ClientId=SH.[LocalClientID] 
				JOIN BI_Builder.dbo.BIBookingFact BH ON SH.BookingRef COLLATE Latin1_General_CI_AS=BH.BookingNumber COLLATE Latin1_General_CI_AS
				AND BH.Customer=C2.ConsigneeEDICode
		)SHIP2 ON SHIP.ShipmentNumber COLLATE Latin1_General_CI_AS=SHIP2.UniqueReference COLLATE Latin1_General_CI_AS
		AND SHIP.Customer COLLATE Latin1_General_CI_AS =SHIP2.Customer COLLATE Latin1_General_CI_AS
		LEFT JOIN 
		(

			[BI_Builder].[dbo].[BIPurchaseOrder]
		)
		--WHERE 
		--SHIP.ShipmentNumber='S00574734'


	----------------------------------------------
	--Remove special character from end of Voyage
	----------------------------------------------
	UPDATE BIShipmentFact
	SET Voyage =CASE WHEN ASCII(RIGHT(Voyage, 1))
				  IN (44, 46, 34, 39,59 ) THEN REPLACE(Voyage, CHAR(ASCII(RIGHT(Voyage, 1))), '')
				ELSE Voyage END 
	WHERE
		 ASCII(RIGHT(Voyage, 1)) IN (44, 46, 34, 39,59)
			 	 
	----------------------------------------------
	--Remove special character from end of Voyage
	----------------------------------------------
	UPDATE BIShipmentFact
	SET  Voyage=	CASE WHEN ASCII(RIGHT(Voyage, 1))
				    IN (44, 46, 34, 39,59) THEN REPLACE(Voyage, CHAR(ASCII(RIGHT(Voyage, 1))), '')
				    ELSE ISNULL(NULLIF(Voyage, ''), 'N/A') END 
	WHERE
		ASCII(RIGHT(Voyage, 1)) IN (44, 46, 34, 39,59)


	UPDATE BIShipmentFact SET Voyage='N/A' WHERE isnull(Voyage, '')=''


	--SELECT * FROM BIShipmentFact where ShippedTEU > 2.25
	
	--SELECT TOP 10 * FROM [CW_ONE].[cw1test1db].[dbo].JobContainer JS

	--SELECT TOP 10 * FROM Ligentix4.dbo.BookingHeader

	--select * from  BI_Builder.dbo.BIBookingFact
	--SELECT TOP 10 * FROM Ligentix4.dbo.Shipment

	--select * from BI_Builder.dbo.BIClient 

	--SELECT Customer, ShipmentNumber, Count(1) FROM TEMP_SHIPMENT
	--group by Customer, ShipmentNumber
	--having count(1) > 1

	--select * from TEMP_SHIPMENT where ShipmentNumber='S00618260'
	----drop table TEMP_SHIPMENT

	--SELECT TOP 10 * FROM Ligentix4.dbo.Shipment where uniquereference='S00574734'

	--SELECT * FROM Ligentix4.dbo.Client where ConsigneeEDICode='BRANTHAUALX'

	--select * from  BI_Builder.dbo.BIBookingFact where BookingNumber in ('101018131', '241018241')

	--SELECT * FROM Ligentix4.dbo.BookingHeader WHERE BK_NO IN ('101018131', '241018241')
	--TRUNCATE TABLE BIShipmentFact


	--select * from TEMP_SHIPMENT where ShipmentNumber='S00559085'
	--DROP TABLE TEMP_SHIPMENT

	--exec sp_rename 'BIShipmentFact', 'BIShipmentFact_bkp'
	--exec sp_rename 'BIShipmentFact_new', 'BIShipmentFact'	
	--drop table BIShipmentFact_bkp

	--SELECT top 10 * FROM BIShipmentFact
	--where ShipmentNumber='S00565731'





--select top 10 * from BIShipmentFact

-- where ColnsoleId is  null