		DECLARE @ClientId INT
		SET @ClientId = (SELECT TOP 1 ClientId FROM Client WITH(NOLOCK) WHERE ClientName='Pepco');

		TRUNCATE TABLE [BI_Builder].dbo.Orders_Test
		
		INSERT INTO [BI_Builder].dbo.Orders_Test
		SELECT DISTINCT
				I.LigClientId AS ClientId,
				M.OrderNumber,
				A.LOT, 
				I.ClientName,
				M.SupplierName,
				M.DestinationWareHouseName,
				CASE M.MOT
					WHEN 'A' THEN 'Air'
					WHEN 'AIR' THEN 'Air'
					WHEN 'Air - Supplier Paid' THEN 'Air'
					WHEN 'AIR-COLLECT' THEN 'Air'
					WHEN 'AIR-PREPAID' THEN 'Air'
					WHEN 'CIF' THEN 'Sea'
					WHEN 'COURIER' THEN 'Air'
					WHEN 'DIRECT - AIR' THEN 'Air'
					WHEN 'DIRECT - COURIER' THEN 'Air'
					WHEN 'DIRECT - SEA' THEN 'Sea'
					WHEN 'DIRECT SEA' THEN 'Sea'
					WHEN 'EXW' THEN 'Sea'
					WHEN 'FCA' THEN 'Sea'
					WHEN 'FOB' THEN 'Sea'
					WHEN 'LAND' THEN 'Road'
					WHEN 'LIGENTIA - AIR' THEN 'Air'
					WHEN 'LIGENTIA - SEA' THEN 'Sea'
					WHEN 'MOT' THEN 'N/A'
					WHEN 'Ocean' THEN 'Sea'
					WHEN 'Other' THEN 'N/A'
					WHEN 'R' THEN 'Road'
					WHEN 'ROA' THEN 'Road'
					WHEN 'ROAD' THEN 'Road'
					WHEN 'S' THEN 'Sea'
					WHEN 'SEA' THEN 'Sea'
					WHEN 'SEA AIR' THEN 'Sea Air'
					WHEN 'T' THEN 'Road'
					WHEN 'TRUCK' THEN 'Road'
					WHEN 'Land' THEN 'Road'
					WHEN 'Rail' THEN 'Rail'
					WHEN 'Air-C' THEN 'Air'
					WHEN 'Air-P' THEN 'Air'
				ELSE M.MOT END AS MOT,
				M.POL,
				M.Exfactory,
				M.Status,
				SUM(A.OrderQty) AS OrderQty,
				COUNT(A.LineNum) AS LineNumCount, 
				ISNULL(
					ISNULL(
						CASE I.LigClientId
							WHEN @ClientId THEN SUM(A.UnitVolume)
							ELSE SUM(ISNULL(A.UnitVolume, 1) * A.OrderQty) 
						END
					,M.TotalVolume)
				, 0) AS UnitVolume,
				SUBSTRING(M.OrderNumber, 1, 3) AS Dissection,
				M.ShipWindowStartDate,
				M.ShipWindowEndDate,
				ASN.ExportDate,
				SUM(A.UnitWeight) AS UnitWeight
			--INTO [BI_Builder].dbo.Orders_Test
			FROM 
				Ligentix_Purchase_Orders.dbo.OrderHeader M WITH(NOLOCK)
				INNER JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH(NOLOCK) ON M.HeaderID=A.HeaderID AND M.IsLatestVersion = 1
				INNER JOIN Ligentix4.dbo.BookingItem B WITH(NOLOCK) ON A.LineId=B.OrderLineId AND (CASE A.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END) = (CASE B.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(B.Lot,'1') END)
				INNER JOIN Ligentix4.dbo.BookingHeader C WITH(NOLOCK) ON B.BookingId=C.BookingId
				INNER JOIN Ligentix_Purchase_Orders.dbo.SourceFile G WITH(NOLOCK) ON G.FileID = M.FileID
				INNER JOIN Ligentix_Purchase_Orders.dbo.ClientFeed H WITH(NOLOCK) ON H.FeedID = G.FeedID
				INNER JOIN Ligentix_Purchase_Orders.dbo.Client I WITH(NOLOCK) ON I.ClientID = H.ClientID
				INNER JOIN 
					(SELECT DISTINCT OrderNumber,ClientId,Lot FROM [BI_Builder].dbo.ShipOrder)
				SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=I.LigClientId AND (CASE SH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(SH.Lot,'1') END) = (CASE A.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)
				LEFT JOIN 
				(
					SELECT OrderNumber, ClientId, MAX(ExportDate) AS ExportDate FROM [ISI_ASN_ExportQueue] WITH (NOLOCK)
					GROUP BY OrderNumber, ClientId
				)ASN ON M.OrderNumber=ASN.OrderNumber AND I.LigClientId=ASN.ClientId
			GROUP BY 
				I.LigClientId, M.OrderNumber,
				A.LOT,
				I.ClientName,
				M.SupplierName,
				M.DestinationWareHouseName,
				CASE M.MOT
				WHEN 'A' THEN 'Air'
				WHEN 'AIR' THEN 'Air'
				WHEN 'Air - Supplier Paid' THEN 'Air'
				WHEN 'AIR-COLLECT' THEN 'Air'
				WHEN 'AIR-PREPAID' THEN 'Air'
				WHEN 'CIF' THEN 'Sea'
				WHEN 'COURIER' THEN 'Air'
				WHEN 'DIRECT - AIR' THEN 'Air'
				WHEN 'DIRECT - COURIER' THEN 'Air'
				WHEN 'DIRECT - SEA' THEN 'Sea'
				WHEN 'DIRECT SEA' THEN 'Sea'
				WHEN 'EXW' THEN 'Sea'
				WHEN 'FCA' THEN 'Sea'
				WHEN 'FOB' THEN 'Sea'
				WHEN 'LAND' THEN 'Road'
				WHEN 'LIGENTIA - AIR' THEN 'Air'
				WHEN 'LIGENTIA - SEA' THEN 'Sea'
				WHEN 'MOT' THEN 'N/A'
				WHEN 'Ocean' THEN 'Sea'
				WHEN 'Other' THEN 'N/A'
				WHEN 'R' THEN 'Road'
				WHEN 'ROA' THEN 'Road'
				WHEN 'ROAD' THEN 'Road'
				WHEN 'S' THEN 'Sea'
				WHEN 'SEA' THEN 'Sea'
				WHEN 'SEA AIR' THEN 'Sea Air'
				WHEN 'T' THEN 'Road'
				WHEN 'TRUCK' THEN 'Road'
				WHEN 'Land' THEN 'Road'
				WHEN 'Rail' THEN 'Rail'
				WHEN 'Air-C' THEN 'Air'
				WHEN 'Air-P' THEN 'Air'
				ELSE M.MOT END,
				M.POL,
				M.Exfactory,
				M.Status,
				M.ShipWindowStartDate,
				M.ShipWindowEndDate,
				ASN.ExportDate,
				M.TotalVolume
			ORDER BY 
				I.LigClientId, 
				M.OrderNumber, 
				A.LOT