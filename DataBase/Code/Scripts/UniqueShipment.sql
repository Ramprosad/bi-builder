DECLARE @UserDessesionTmp TABLE (GroupId INT NOT NULL,
											GroupName VARCHAR(50) NULL,
											ShipId INT NOT NULL,
											ShipName VARCHAR(50) NULL,
											Diss VARCHAR(6) NULL,
											ClientID INT);
		INSERT INTO @UserDessesionTmp
		SELECT DISTINCT A.Id GroupId,A.Name GroupName,B.Id ShipId,B.Name ShipName,C.diss Diss,A.ClientID
		FROM dbo.BuyingGroup A WITH (NOLOCK)
			INNER JOIN dbo.Buyership B WITH (NOLOCK) ON A.Id = B.BuyinggroupId
			INNER JOIN dbo.SubBuyer C WITH (NOLOCK) ON B.Id = C.BuyershipID
			INNER JOIN dbo.UserDissection D WITH (NOLOCK) ON D.DissectionID = C.Id
		
		SELECT MIN(ClientID) AS ClientID
			,MIN(OrderNumber) AS OrderNumber
			,MAX(LOT) AS LOT
			,MAX(SHPR_ADDR_01) AS SHPR_ADDR_01
			,MAX(SHPR_CODE) AS SHPR_CODE
			,SUM(CBM) AS CBM
			,SUM(BookedQty) AS BookedQty
			,MAX(SUBMIT_DATE) AS SUBMIT_DATE
			,MAX(BK_NO) AS BK_NO
			,MAX(HOB) AS HOB
			,ShipmentID
			,MAX(MOT) AS MOT
			,'0' FileStatus 
			,SYSDATETIME() AS LastUpdateDate
		INTO [BI_Builder].dbo.UniqueShipment_Test
		FROM
		(SELECT
			ClientID,
			OrderNumber,
			LOT,
			SHPR_ADDR_01 AS SHPR_ADDR_01,
			SHPR_CODE AS SHPR_CODE,
			SUM(CBM) AS CBM,
			SUM(INPKG) AS BookedQty,
			SUBMIT_DATE,
			BK_NO,
			(
				SELECT TOP (1) T.GroupName FROM @UserDessesionTmp T 
				WHERE T.Diss = SUBSTRING(BOOKING.OrderNumber, 1, 3) AND T.ClientID=BOOKING.ClientID
			) HOB
			,ShipmentID
			,MOT
		FROM
		(
			SELECT 
				I.LigClientID AS ClientID,
				M.OrderNumber,
				A.LOT,
				C.SHPR_ADDR_01 AS SHPR_ADDR_01,
				C.SHPR_CODE AS SHPR_CODE,
				ISNULL(B.CBM, 0) AS CBM,
				ISNULL(B.INPKG, 0) AS INPKG,
				ISNULL(C.SUBMIT_DATE, '') AS SUBMIT_DATE,
				C.BK_NO,
				C.CW_REF AS ShipmentID,
				CASE C.MOT
					WHEN 'A' THEN 'Air'
					WHEN 'AIR' THEN 'Air'
					WHEN 'Air - Supplier Paid' THEN 'Air'
					WHEN 'AIR-COLLECT' THEN 'Air'
					WHEN 'AIR-PREPAID' THEN 'Air'
					WHEN 'CIF' THEN 'Sea'
					WHEN 'COURIER' THEN 'Air'
					WHEN 'DIRECT - AIR' THEN 'Air'
					WHEN 'DIRECT - COURIER' THEN 'Air'
					WHEN 'DIRECT - SEA' THEN 'Sea'
					WHEN 'DIRECT SEA' THEN 'Sea'
					WHEN 'EXW' THEN 'Sea'
					WHEN 'FCA' THEN 'Sea'
					WHEN 'FOB' THEN 'Sea'
					WHEN 'LAND' THEN 'Road'
					WHEN 'LIGENTIA - AIR' THEN 'Air'
					WHEN 'LIGENTIA - SEA' THEN 'Sea'
					WHEN 'MOT' THEN 'N/A'
					WHEN 'Ocean' THEN 'Sea'
					WHEN 'Other' THEN 'N/A'
					WHEN 'R' THEN 'Road'
					WHEN 'ROA' THEN 'Road'
					WHEN 'ROAD' THEN 'Road'
					WHEN 'S' THEN 'Sea'
					WHEN 'SEA' THEN 'Sea'
					WHEN 'SEA AIR' THEN 'Sea Air'
					WHEN 'T' THEN 'Road'
					WHEN 'TRUCK' THEN 'Road'
					WHEN 'Land' THEN 'Road'
					WHEN 'Rail' THEN 'Rail'
					WHEN 'Air-C' THEN 'Air'
					WHEN 'Air-P' THEN 'Air'
					ELSE C.MOT
				END AS MOT
			FROM 
				Ligentix_Purchase_Orders.dbo.OrderHeader M WITH(NOLOCK)
				INNER JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH(NOLOCK) ON M.HeaderID = A.HeaderID
				INNER JOIN Ligentix4.dbo.BookingItem B WITH(NOLOCK) ON A.LineId = B.OrderLineId
				INNER JOIN Ligentix4.dbo.BookingHeader C WITH(NOLOCK) ON B.BookingId = C.BookingId
				INNER JOIN Ligentix_Purchase_Orders.dbo.SourceFile G WITH(NOLOCK) ON G.FileID = M.FileID
				INNER JOIN Ligentix_Purchase_Orders.dbo.ClientFeed H WITH(NOLOCK) ON H.FeedID = G.FeedID
				INNER JOIN Ligentix_Purchase_Orders.dbo.Client I WITH(NOLOCK) ON I.ClientID = H.ClientID
				INNER JOIN 
					(SELECT DISTINCT OrderNumber,ClientId,Lot FROM [BI_Builder].dbo.ShipOrder)
				SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=I.LigClientId AND (CASE SH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(SH.Lot,'1') END) =  (CASE A.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)
			WHERE
				ISNULL(C.CW_REF, '')!=''
		)BOOKING
		GROUP BY 
			OrderNumber,
			ClientID,
			LOT,
			SHPR_ADDR_01,
			SHPR_CODE,
			SUBMIT_DATE,
			BK_NO,
			ShipmentID,
			MOT
		)UniqueShip
		GROUP BY
			 ShipmentID