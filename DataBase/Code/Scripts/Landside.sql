SELECT  DISTINCT
		 OH.LigClientId AS ClientId
		,OH.OrderNumber
		,OH.LOT
		,LSB.LandSideBookingId
		,LSB.LandSideContainerId
		,MAX(LSB.BookWarehouse) AS BookWarehouse
		,LSC.ContainerNumber
		,LSC.Size
		,LU.UserName AS ConfirmedBy
		,WHS.WarehouseName
INTO [BI_Builder].dbo.Landside_Test
FROM					
	(SELECT DISTINCT OrderNumber,ClientId AS LigClientId,Lot,ShipmentID FROM [BI_Builder].dbo.ShipOrder) OH
	INNER JOIN LandSideOrderLine LSOL WITH(NOLOCK) ON OH.OrderNumber = LSOL.OrderNumber AND OH.LigClientId = LSOL.ClientID
		AND (CASE OH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(OH.Lot,'1') END) = (CASE LSOL.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(LSOL.Lot,'1') END)
	INNER JOIN LandSideBooking LSB WITH(NOLOCK) ON LSOL.LandSideBookingId = LSB.LandSideBookingId
	INNER JOIN LandSideContainer LSC WITH(NOLOCK) ON LSC.LandSideContainerId = LSB.LandSideContainerId
	LEFT JOIN Lig_User LU WITH(NOLOCK) ON LSB.ConfirmedBy = LU.UserID
	LEFT JOIN warehouse WHS WITH(NOLOCK) ON LSB.WarehouseId = WHS.WarehouseId
WHERE ISNULL(LSOL.IsDelete, '0') = '0'
GROUP BY
	 OH.LigClientId
	,OH.OrderNumber
	,OH.LOT
	,LSB.LandSideBookingId
	,LSB.LandSideContainerId
	,LSC.ContainerNumber
	,LSC.Size
	,LU.UserName
	,WHS.WarehouseName
ORDER BY OH.LigClientId, OH.OrderNumber, OH.LOT