
/*******************************************************************

SELECT SUM(ShippedCBM) FROM POBookingShipmentView
GROUP BY 
ORDER BY 
	[ShipmentNumber]

SELECT SUM(ShippedCBM) 
FROM
(
	SELECT DISTINCT [ShipmentNumber], ShippedCBM
	FROM 
	POBookingShipmentView
	WHERE MONTH(ETA)=10 AND YEAR(ETA)=2018
)X

====================================================================*/
ALTER VIEW POBookingShipmentView
AS
	SELECT 
		SF.[Customer],
		SF.[ShipmentNumber],
		SF.[ConsoleId],
		ISNULL(BF.[BookingNumber], 'N/A')				AS BookingNumber, 
		ISNULL(BF.[BookingSubmitedDate], '1900-01-01')	AS BookingSubmitedDate,
		ISNULL(PF.[PONumber], 'N/A')					AS PONumber, 
		ISNULL(PF.[lot], 'NA/A')						AS Lot,
		ISNULL(PF.[Supplier], 'N/A')					AS Supplier,
		ISNULL(SF.[TransportationMethodCode], 'N/A')	AS TransportationMethodCode, 
		ISNULL(SF.[ShipmentModeCode], 'N/A')			AS ShipmentModeCode,
		ISNULL(SF.[ETA], '1900-01-01')					AS ETA, 
		ISNULL(SF.[ETD], '1900-01-01')					AS ETD, 
		ISNULL(SF.[POD], 'N/A')							AS POD,
		ISNULL(SF.[POL], 'N/A')							AS POL,
		ISNULL(SF.[FirstContainerNumber], 'N/A')		AS FirstContainerNumber, 
		ISNULL(SF.[FirstContainerSize], 'N/A')			AS FirstContainerSize, 
		ISNULL(SF.[Vessel], 'N/A')						AS Vessel, 
		ISNULL(SF.[Voyage], 'N/A')						AS Voyage, 
		ISNULL(SF.[CarrierCode], 'N/A')					AS CarrierCode, 
		ISNULL(SF.[ShippedQty], 0)						AS ShippedQty, 
		ISNULL(SF.[ShippedCBM], 0)						AS ShippedCBM, 
		ISNULL(SF.[ShipWeight], 0)						AS ShipWeight, 
		ISNULL(SF.[ShippedTEU], 0)						AS ShippedTEU, 
		ISNULL(BF.[TotalBookedCBM], 0)					AS TotalBookedCBM, 
		ISNULL(BF.[TotalBookedQuantity], 0)				AS TotalBookedQuantity, 
		ISNULL(PF.[OrderQty], 0)						AS OrderQty, 
		ISNULL(PF.[UnitVolume], 0)						AS UnitVolume, 
		ISNULL(PF.[UnitWeight], 0)						AS UnitWeight, 
		ISNULL(PF.[OrderDate], '1900-01-01')			AS OrderDate, 
		ISNULL(PF.[OrderStatus], 'N/A')					AS OrderStatus				
	FROM 
		[BI_Builder].[dbo].[BIShipmentFact] SF
		LEFT JOIN [BI_Builder].[dbo].BIBookingToShipmentFact BSF ON SF.ShipmentNumber=BSF.ShipmentNumber AND SF.Customer=BSF.Customer
		LEFT JOIN [BI_Builder].[dbo].[BIBookingFact] BF ON BSF.BookingNumber=BF.BookingNumber  AND bSF.Customer=BF.Customer
		LEFT JOIN [BI_Builder].[dbo].[BIPOToBookingFact] POB ON BF.BookingNumber=POB.BookingNumber AND BF.Customer=POB.Customer
		LEFT JOIN [BI_Builder].[dbo].[BIPurchaseOrder] PF ON  POB.Customer=PF.Customer AND POB.PONumber=PF.PONumber AND POB.Lot=PF.Lot 


	
SELECT sh.UniqueReference, COUNT(DISTINCT bh.bk_no) As BooingCount
FROM 
	Ligentix4.dbo.Shipment SH JOIN BookingHeader bh on sh.BookingRef=bh.bk_no
GROUP BY 
	sh.uniqueReference
HAVING COUNT(DISTINCT bh.bk_no)>1
---------------------------------------------------
SELECT BookingRef, COUNT(DISTINCT UniqueReference) As ShipmentCount
FROM 
	Ligentix4.dbo.Shipment SH 
WHERE 
	ISNULL(BookingRef, '')!=''
GROUP BY 
	BookingRef
HAVING COUNT(DISTINCT UniqueReference)>1


SELECT * FROM Ligentix4.dbo.Shipment WHERE UniqueReference='34 zn 0085'
SELECT * FROM Ligentix4.dbo.Shipment WHERE UniqueReference='S00574734'

SELECT BookingRef, COUNT(DISTINCT UniqueReference)
FROM 
	Ligentix4.dbo.Shipment SH 
WHERE 
	ISNULL(BookingRef, '')!=''
GROUP BY 
	BookingRef
HAVING COUNT(DISTINCT UniqueReference)>1

SELECT UniqueReference, COUNT( BookingRef)
FROM 
	Ligentix4.dbo.Shipment SH 
WHERE 
	ISNULL(BookingRef, '')!=''
GROUP BY 
	UniqueReference
HAVING COUNT( BookingRef)>1
order by datecreated