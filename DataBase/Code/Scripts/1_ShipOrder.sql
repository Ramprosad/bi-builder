DECLARE @CutOffDate DATE
DECLARE @CutOffStartDate VARCHAR(25)

SET @CutOffDate = DATEADD(DAY, -425, GETDATE())

SET @CutOffStartDate = '01-' + Right('0' + CAST(MONTH(@CutOffDate) AS VARCHAR), 2) + '-' + CONVERT(VARCHAR,YEAR(@CutOffDate))

--INSERT INTO [BI_Builder].[dbo].[ShipOrder]
SELECT DISTINCT 
	SP.ShipmentID,Ship.OrderNumber,Ship.ClientId,Ship.Lot
INTO [BI_Builder].[dbo].[ShipOrder]
FROM
	(
		SELECT DISTINCT SPO.DateCreated,SPO.ShipmentID,SPO.OrderNumber,S.[LocalClientID] AS ClientId,(CASE WHEN SPO.LOT='' THEN '1' ELSE ISNULL(SPO.LOT,'1') END) AS Lot
		FROM [dbo].[Shipment] S WITH (NOLOCK)
				INNER JOIN Shipment_PurchaseOrder SPO WITH (NOLOCK) ON SPO.ShipmentID = S.ShipmentID
		WHERE CONVERT(DATE,SPO.DateCreated)>=CONVERT(DATE,@CutOffStartDate)--DATEADD(DAY, -5, GETDATE())
	) Ship
	INNER JOIN [dbo].[Shipment] S1 WITH (NOLOCK) ON Ship.ClientId = S1.[LocalClientID]
	INNER JOIN Shipment_PurchaseOrder SP ON S1.ShipmentID = SP.ShipmentID AND Ship.OrderNumber = SP.OrderNumber AND (CASE WHEN SP.Lot='' THEN '1' ELSE ISNULL(SP.Lot,'1') END) = ISNULL(Ship.Lot,'1')