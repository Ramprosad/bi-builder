USE [Ligentix4]
GO
/****** Object:  StoredProcedure [dbo].[usp_IncrementalData_OrderAudit]    Script Date: 14-03-2019 17:23:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*====================================================================================================
Author		: Asish Kar
Date		: 03-Jan-2019

Desc		: This SP will be used to fetch Incremental data for OrderAudit from L4 database.
exec usp_IncrementalData_OrderAudit
======================================================================================================*/
CREATE PROCEDURE [dbo].[usp_IncrementalData_OrderAudit]
AS
BEGIN
	BEGIN TRY
		SELECT 
				 M.LigClientID AS ClientID
				,M.OrderNumber
				,J.LogType
				,J.UserName
				,J.DateCreated
				,'0' FileStatus, 
				SYSDATETIME() AS LastUpdateDate
			INTO [BI_Builder].dbo.OrderAudit
			FROM 
				(
					SELECT DISTINCT M.OrderNumber, I.LigClientId
					FROM Ligentix_Purchase_Orders.dbo.OrderHeader M WITH(NOLOCK)
						 INNER JOIN Ligentix_Purchase_Orders.dbo.SourceFile G WITH(NOLOCK) ON G.FileID = M.FileID
						 INNER JOIN Ligentix_Purchase_Orders.dbo.ClientFeed H WITH(NOLOCK) ON H.FeedID = G.FeedID
						 INNER JOIN Ligentix_Purchase_Orders.dbo.Client AS I WITH(NOLOCK) ON I.ClientID = H.ClientID
				)M
				INNER JOIN (
					SELECT X.OrderNumber
						,X.ClientID 
						,X.OrderAuditId
						,Y.LogType
						,Y.UserName
						,Y.DateCreated
					FROM
						(SELECT OrderNumber, ClientID, MAX(OrderAuditId) AS OrderAuditId FROM Ligentix4.dbo.OrderAudit X WITH(NOLOCK) GROUP BY OrderNumber, ClientID)X
						INNER JOIN Ligentix4.dbo.OrderAudit Y WITH(NOLOCK) ON X.OrderAuditId=Y.OrderAuditId
					) J ON J.OrderNumber =M.OrderNumber AND J.ClientID=M.LigClientID
				
					INNER JOIN 
						(SELECT DISTINCT OrderNumber,ClientId,Lot FROM [BI_Builder].dbo.ShipOrder)
					SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=M.LigClientId
			ORDER BY M.LigClientID, M.OrderNumber
	END TRY
	BEGIN CATCH
		--ROLLBACK TRANSACTION;  
		PRINT 'Error: Number: '+ CAST(ERROR_NUMBER() AS VARCHAR(100))+ ' Message:' + ERROR_MESSAGE();  
		SELECT 'Error: Number: '+ CAST(ERROR_NUMBER() AS VARCHAR(100))+ ' Message:' + ERROR_MESSAGE() AS Result;

	END CATCH  
END