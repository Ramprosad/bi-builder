
USE [BI_Builder]
GO

	IF NOT EXISTS (SELECT 1 FROM sys.objects where name ='BIMilestoneFact')
	BEGIN

		CREATE TABLE BIMilestoneFact
		(
			Customer				 VARCHAR(100) NOT NULL, 
			PONumber				 VARCHAR(100) NOT NULL,
			lot						 VARCHAR(100) NOT NULL,
			MilestoneKey			 VARCHAR(100) NOT NULL,
			MilestoneDate			 DATE,
			CONSTRAINT PK_MilestoneFact_Customer_PONumber_lot_MilestoneKey PRIMARY KEY (Customer, PONumber, lot, MilestoneKey)

		)
	END 

GO


	--INSERT INTO BIMilestoneFact (Customer, PONumber, lot, MilestoneKey,MilestoneDate)
	SELECT ConsigneeEDICode, OrderNumber, Lot, MileStoneKey, MileStoneDate
	FROM
	(
		SELECT 
			C.ConsigneeEDICode,
			MS.MileStoneKey, 
			OCD.OrderNumber, 
			OCD.Lot,
			CASE RIGHT(MS.MileStoneKey, 3)
			WHEN 'CUR' THEN  ISNULL(OCD.EstimatedDate, '1900-01-01')
			WHEN 'EST' THEN  ISNULL(OCD.CurrentDate, '1900-01-01')
			WHEN 'ACT' THEN  ISNULL(OCD.ActualDate, '1900-01-01')
			END AS MileStoneDate, 
			ROW_NUMBER() OVER (PARTITION BY C.ConsigneeEDICode, MS.MileStoneKey, OCD.OrderNumber,OCD.Lot ORDER BY ocd.OrderCriticaldateId DESC)  SLNO
		FROM
			[Ligentix4].dbo.OrderCriticaldate OCD (NOLOCK)
			JOIN [Ligentix4].dbo.ClientCriticalDate CCD (NOLOCK) ON OCD.ClientCriticaldateId=ccd.ClientCriticaldateId
			JOIN [Ligentix4].dbo.Client C (NOLOCK) ON CCD.ClientId=C.ClientId
			JOIN [BI_Builder].dbo.BIClient BIC (NOLOCK) ON C.ConsigneeEDICode=BIC.[ConsigneeEDICode]
			JOIN [BI_Builder].dbo.BIMileStone  MS (NOLOCK) ON LEFT(MS.MileStoneKey, LEN(MS.MileStoneKey)-4)=CCD.[KEY] AND  C.ConsigneeEDICode=MS.ClientCWEDICode	
			JOIN [BI_Builder].dbo.BIPurchaseOrder PO (NOLOCK) ON OCD.OrderNumber=PO.PONumber 
			AND (CASE OCD.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(OCD.Lot,'1') END)=PO.Lot
			AND C.ConsigneeEDICode=PO.Customer
	)X 
	WHERE 
		SLNO=1
	


	/*
	SELECT OrderFeedId, * FROM Ligentix_Purchase_Orders.dbo.OrderHeader WHERE OrderNumber IN 
	(
		'803/181449',
		'364/199615',
		'570/185960',
		'803/181449',
		'820/676978',
		'820/677019',
		'820/677156',
		'820/677163',
		'820/719373',
		'820/719498'
	)

	select * from Ligentix_Purchase_Orders.dbo.ClientFeed where FeedId in (21, 809)

	select * from Ligentix_Purchase_Orders.dbo.OrderLine WHERE HeaderId in (2048448, 2049651)

	SELECT OrderFeedId, * FROM Ligentix_Purchase_Orders.dbo.OrderHeader WHERE OrderNumber IN 
	('820/677156')

*/
