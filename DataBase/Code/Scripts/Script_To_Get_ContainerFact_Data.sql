

USE [BI_Builder]
GO


	IF NOT EXISTS (SELECT 1 FROM sys.objects where name ='BIContainerFact')
	BEGIN

		CREATE TABLE BIContainerFact
		(
			Customer				 VARCHAR(100) NOT NULL, 
			ShipmentNumber			 VARCHAR(100) NOT NULL,
			ContainerNumber			 VARCHAR(100) NOT NULL, 
			ContainerSizeCode		 VARCHAR(100) NOT NULL,
			ActualVolume			 FLOAT, 
			ConsoleId				 VARCHAR(100) NOT NULL,
			CONSTRAINT PK_BIContainerFact_Customer_New_ShipmentNumber_ContainerNumber PRIMARY KEY (Customer, ShipmentNumber, ContainerNumber)
		)
	END 

GO

	IF OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL
	BEGIN
		DROP TABLE #TEMP  
	END  
	
	CREATE TABLE #TEMP
	(
		OA_PK UNIQUEIDENTIFIER ,
		OA_OH UNIQUEIDENTIFIER,
		CONSTRAINT [PK_TEMP01] PRIMARY KEY CLUSTERED 
		(
			OA_PK ASC
		)
	)

	INSERT INTO #TEMP
	SELECT OA_PK, OA_OH FROM openquery([CW_ONE],'SELECT OA_PK, OA_OH FROM [cw1test1db].[dbo].OrgAddress')

	INSERT INTO BIContainerFact(Customer, ShipmentNumber, ContainerNumber, ContainerSizeCode, ActualVolume, ConsoleId)
	SELECT 
		BC.ConsigneeEDICode							AS Customer,
		JS.JS_UniqueConsignRef						AS ShipmentNumber, 
		JC.JC_ContainerNum							AS ContainerNumber, 
		CASE WHEN RC_CODE IN ('20GOH', '20PBF')
			 THEN '20GOH'

			 WHEN RC_CODE IN ('20FR','20FR','20FT 1M OH','20FT 30 OH','20G0','20G1','20GP','20H0','20HAZ','20HC','20HCSUB','20K0','20K1','20K2',
						 '20K8','20NOR','20OT','20P1','20T0','20T1','20T2','20T3','20T4','20T5','20T6','20T7','20T8','22B0','22G1','22H0',
						 '22K0','22K1','22K2','22K8','22P1','22P3','22P7','22P8','22P9','22R1','22R7','22R9','22S1','22T0','22T1','22T2',
						 '22T3','22T4','20T5','22T6','22T7','22T8','22U1','22U6','22V0','22V2','22V3','25G0','25K0','25K1','25K2','25K8',
						 '26G0','26H0','28T8','28U1','28V0','29P0')
			 THEN '20GP'

			 WHEN RC_CODE ='20RE'
			 THEN '20RE'

			 WHEN RC_CODE IN('40GOH','40HCGOHSUB','40PBF')
			 THEN '40GOH'

			 WHEN RC_CODE IN('40DC','40FR','40FT 1M OH','40FT 30 OH','40GP','40HAZ','40K0','40K1','40K2','40K8','40NOR','40OT',
							 '42G0','42G1','42H0','42K0','42K1','42K2','42K8','42P1','42P3','42P6','42P8','42P9','42R1','42R3',
							 '42R9','42S1','42T2','42T5','42T6','42T8','42U1','42U6' )
			 THEN '40GP'
	
			 WHEN RC_CODE IN('40HC',' 40HC RF','40HCHAZ','40HCNOR','40HCOT','40HCPW','40HCSUB')
			 THEN '40HC'

			 WHEN RC_CODE IN('40HCGOH','40HPBF','40HPBFSUB')
			 THEN '40HCGOH'

			 WHEN RC_CODE IN('40RE','40REHC')
			 THEN '40RE'

	
			 WHEN RC_CODE ='45FT GOH'
			 THEN '45GOH'

			 WHEN RC_CODE IN('45B3','45G0','45G1','45HC','45HCSUB','45K0','45K1','45K2','45K8','45P3','45P8','45R1','45R9',
							 '45U1','45U6','46H0','48K8','48T8','49P0' )
			 THEN '45HC'
			 WHEN RC_CODE ='45PBF' 
			 THEN '45HCGOH'
		ELSE RC_CODE END							AS ContainerSize,
		SUM(JO.JO_ActualVolume)						AS ActualVolume, 
		JK.JK_PK									AS ConsoleId
	FROM 
		[CW_ONE].[cw1test1db].[dbo].JobOrderLine JO WITH(NOLOCK) 
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobOrderHeader JD WITH(NOLOCK)  On JD.JD_PK=JO.JO_JD
		INNER JOIN #TEMP OA WITH(NOLOCK)  On JD.JD_OA_BuyerAddress=OA.OA_PK
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].OrgHeader OH WITH(NOLOCK)  On OH.OH_PK=OA.OA_OH --JD_OH_BuyerAddress
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobShipment JS WITH(NOLOCK)  On JS.JS_PK=JD.JD_JS
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConShipLink JN WITH(NOLOCK)  On JN.JN_JS=JS.JS_PK
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsol JK WITH(NOLOCK)  On JK.JK_PK=JN.JN_JK		
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobContainer JC WITH(NOLOCK)  ON JC.JC_JK=	JK.JK_PK
		INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefContainer RC WITH(NOLOCK)  On JC.JC_RC=RC.RC_PK AND JC.JC_JK=JK.JK_PK AND JC.JC_ContainerNum=JO.JO_ContainerNumber
		INNER JOIN [BI_Builder].dbo.BIClient  BC ON OH.OH_CODE COLLATE Latin1_General_CI_AS =BC.ConsigneeEDICode COLLATE Latin1_General_CI_AS
	WHERE 
		JS.JS_SystemCreateTimeUTC >= DATEADD(DD, -500, GETDATE())
	GROUP BY 
		BC.ConsigneeEDICode,
		JS.JS_UniqueConsignRef, 
		JC.JC_ContainerNum, 
		RC.RC_Code, 
		JK.JK_PK

	
	
	--SELECT * FROM BIBookingFact ORDER BY BookingSubmitedDate ASC

	--SELECT * FROM Ligentix4.dbo.BookingHeader WHERE BK_NO='271218265'
	--SELECT TOP 10 * FROM Ligentix4.dbo.Shipment_PurchaseOrder 
	--SELECT TOP 10 * FROM [CW_ONE].cw1test1db.DBO.JobShipment JS 
	--SELECT TOP 10 * FROM [CW_ONE].cw1test1db.DBO.JobContainer
	--SELECT TOP 10 * FROM [CW_ONE].cw1test1db.DBO.JobOrderLine
	--SELECT TOP 10 * FROM [CW_ONE].cw1test1db.DBO.JobOrderHeader
	--select * from BIContainerFact
	--select * from [dbo].[BIClient]


	--SELECT top 10 *  FROM BIContainerFact


	--exec sp_rename 'BIContainerFact', 'BIContainerFact_bkp'
	--exec sp_rename 'BIContainerFact_New', 'BIContainerFact'	
