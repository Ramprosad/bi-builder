	--SELECT * FROM [BI_Builder].dbo.Milestone_Test

	

	USE BI_Builder
	GO
		DECLARE @ClientId INT 
		
		SELECT @ClientId= ClientId FROM Ligentix4.dbo.Client WHERE ClientName LIKE 'John Lewis Partnership' 
		INSERT INTO [dbo].[BIMileStoneFactPivot_New]
		(
			Customer, OrderNumber, LOT, EX_WORKS_ACT, EX_WORKS_EST, EX_WORKS_ORG, SUPPLIER_BOOKED_GOODS_ACT, SUPPLIER_BOOKED_GOODS_EST, SUPPLIER_BOOKED_GOODS_ORG, 
			RequiredHandover_ACT, RequiredHandover_EST, RequiredHandover_ORG, ETD_ACT, ETD_EST, ETD_ORG, ETA_ACT, ETA_EST, ETA_ORG, CYContainerGatedDeparturePort_ACT, 
			CYContainerGatedDeparturePort_EST, CYContainerGatedDeparturePort_ORG, ProductionDate_ACT, ProductionDate_EST, ProductionDate_ORG, ATPInspection_ACT, 
			ATPInspection_EST, ATPInspection_ORG, SOConfirmedByLigentiaOriginOffice_ACT, SOConfirmedByLigentiaOriginOffice_EST, SOConfirmedByLigentiaOriginOffice_ORG, 
			BookingWindowClosedDate_ACT, BookingWindowClosedDate_EST, BookingWindowClosedDate_ORG, CargoReadyDate_ACT, CargoReadyDate_EST, 
			CargoReadyDate_ORG, PhysicalHandoverOfTheGoods_ACT, PhysicalHandoverOfTheGoods_EST, PhysicalHandoverOfTheGoods_ORG, RequiredDeliveryDate_ACT, 
			RequiredDeliveryDate_EST, RequiredDeliveryDate_ORG, SailingDateOrigin_ACT, SailingDateOrigin_EST, SailingDateOrigin_ORG, CFSLOADED_ACT, CFSLOADED_EST, 
			CFSLOADED_ORG, POBooked_ACT, POBooked_EST, POBooked_ORG, CustomClearanceDate_ACT, CustomClearanceDate_EST, CustomClearanceDate_ORG, MerchSAApproval_ACT, 
			MerchSAApproval_EST, MerchSAApproval_ORG, NOA_ACT, NOA_EST, NOA_ORG, SFD_ACT, SFD_EST, SFD_ORG, Approved_ACT, Approved_EST, Approved_ORG, BookingWindowOpen_ACT, 
			BookingWindowOpen_EST, BookingWindowOpen_ORG, GoldSealApproval_ACT, GoldSealApproval_EST, GoldSealApproval_ORG, ILCSAApproval_ACT, ILCSAApproval_EST, 
			ILCSAApproval_ORG, ContainerReleasedCR_ACT, ContainerReleasedCR_EST, ContainerReleasedCR_ORG, DCbookingmade_ACT, DCbookingmade_EST, DCbookingmade_ORG, 
			DCbookingconfirmed_ACT, DCbookingconfirmed_EST, DCbookingconfirmed_ORG, 
			PORaised_ACT, PORaised_EST, PORaised_ORG
		)
		SELECT DISTINCT 
				CL.ConsigneeEDICode						AS Customer,
				M.OrderNumber							as OrderNumber,
				M.LOT									AS LOT,
				EX_WORKS_ACT							AS EX_WORKS_ACT,
				EX_WORKS_EST							AS EX_WORKS_EST,		
				EX_WORKS_ORG							AS EX_WORKS_ORG,
				SUPPLIER_BOOKED_GOODS_ACT				AS SUPPLIER_BOOKED_GOODS_ACT,
				SUPPLIER_BOOKED_GOODS_EST				AS SUPPLIER_BOOKED_GOODS_EST,
				SUPPLIER_BOOKED_GOODS_ORG				AS SUPPLIER_BOOKED_GOODS_ORG,
				RequiredHandover_ACT					AS RequiredHandover_ACT,
				RequiredHandover_EST					AS RequiredHandover_EST,
				RequiredHandover_ORG					AS RequiredHandover_ORG,
				ETD_ACT									AS ETD_ACT,
				ETD_EST									AS ETD_EST,
				ETD_ORG									AS ETD_ORG,
				ETA_ACT									AS ETA_ACT,
				ETA_EST									AS ETA_EST,
				ETA_ORG									AS ETA_ORG,
				CYContainerGatedDeparturePort_ACT		AS CYContainerGatedDeparturePort_ACT,
				CYContainerGatedDeparturePort_EST		AS CYContainerGatedDeparturePort_EST,
				CYContainerGatedDeparturePort_ORG		AS CYContainerGatedDeparturePort_ORG,
				ProductionDate_ACT						AS ProductionDate_ACT,
				ProductionDate_EST						AS ProductionDate_EST,
				ProductionDate_ORG						AS ProductionDate_ORG,
				ATPInspection_ACT						AS ATPInspection_ACT,
				ATPInspection_EST						AS ATPInspection_EST,
				ATPInspection_ORG						AS ATPInspection_ORG,
				SOConfirmedByLigentiaOriginOffice_ACT	AS SOConfirmedByLigentiaOriginOffice_ACT,
				SOConfirmedByLigentiaOriginOffice_EST	AS SOConfirmedByLigentiaOriginOffice_EST,
				SOConfirmedByLigentiaOriginOffice_ORG	AS SOConfirmedByLigentiaOriginOffice_ORG,
				BookingWindowClosedDate_ACT				AS BookingWindowClosedDate_ACT,
				BookingWindowClosedDate_EST				AS BookingWindowClosedDate_EST,
				BookingWindowClosedDate_ORG				AS BookingWindowClosedDate_ORG,
				CargoReadyDate_ACT						AS CargoReadyDate_ACT,
				CargoReadyDate_EST						AS CargoReadyDate_EST,
				CargoReadyDate_ORG						AS CargoReadyDate_ORG,
				PhysicalHandoverOfTheGoods_ACT			AS PhysicalHandoverOfTheGoods_ACT,
				PhysicalHandoverOfTheGoods_EST			AS PhysicalHandoverOfTheGoods_EST,
				PhysicalHandoverOfTheGoods_ORG			AS PhysicalHandoverOfTheGoods_ORG,
				RequiredDeliveryDate_ACT				AS RequiredDeliveryDate_ACT,
				RequiredDeliveryDate_EST				AS RequiredDeliveryDate_EST,
				RequiredDeliveryDate_ORG				AS RequiredDeliveryDate_ORG,
				SailingDateOrigin_ACT					AS SailingDateOrigin_ACT,
				SailingDateOrigin_EST					AS SailingDateOrigin_EST,
				SailingDateOrigin_ORG					AS SailingDateOrigin_ORG,
				CFSLOADED_ACT							AS CFSLOADED_ACT, 
				CFSLOADED_EST							AS CFSLOADED_EST,
				CFSLOADED_ORG							AS CFSLOADED_ORG,
				POBooked_ACT							AS POBooked_ACT,
				POBooked_EST							AS POBooked_EST,
				POBooked_ORG							AS POBooked_ORG,
				CustomClearanceDate_ACT					AS CustomClearanceDate_ACT,
				CustomClearanceDate_EST					AS CustomClearanceDate_EST,
				CustomClearanceDate_ORG					AS CustomClearanceDate_ORG,
				MerchSAApproval_ACT						AS MerchSAApproval_ACT,
				MerchSAApproval_EST						AS MerchSAApproval_EST,
				MerchSAApproval_ORG						AS MerchSAApproval_ORG,
				NOA_ACT									AS NOA_ACT,
				NOA_EST									AS NOA_EST,
				NOA_ORG									AS NOA_ORG,
				SFD_ACT									AS SFD_ACT,
				SFD_EST									AS SFD_EST,
				SFD_ORG									AS SFD_ORG,
				Approved_ACT							AS Approved_ACT,
				Approved_EST							AS Approved_EST,
				Approved_ORG							AS Approved_ORG,
				BookingWindowOpen_ACT					AS BookingWindowOpen_ACT,
				BookingWindowOpen_EST					AS BookingWindowOpen_EST,
				BookingWindowOpen_ORG					AS BookingWindowOpen_ORG,
				GoldSealApproval_ACT					AS GoldSealApproval_ACT,
				GoldSealApproval_EST					AS GoldSealApproval_EST,
				GoldSealApproval_ORG					AS GoldSealApproval_ORG,
				ILCSAApproval_ACT						AS ILCSAApproval_ACT,
				ILCSAApproval_EST						AS ILCSAApproval_EST,
				ILCSAApproval_ORG						AS ILCSAApproval_ORG,
				ContainerReleasedCR_ACT					AS ContainerReleasedCR_ACT,
				ContainerReleasedCR_EST					AS ContainerReleasedCR_EST,
				ContainerReleasedCR_ORG					AS ContainerReleasedCR_ORG,
				DCbookingmade_ACT						AS DCbookingmade_ACT,
				DCbookingmade_EST						AS DCbookingmade_EST,
				DCbookingmade_ORG						AS DCbookingmade_ORG,
				DCbookingconfirmed_ACT					AS DCbookingconfirmed_ACT,
				DCbookingconfirmed_EST					AS DCbookingconfirmed_EST,
				DCbookingconfirmed_ORG					AS DCbookingconfirmed_ORG, 
				PORaised_ACT, 
				PORaised_EST, 
				PORaised_ORG
								
			--INTO #TEMP_MILESTONE --drop table #TEMP_MILESTONE
			FROM
				(SELECT DISTINCT 
					 M.OrderNumber
					,I.LigClientId
					,A.LOT
				FROM 
					Ligentix_Purchase_Orders.dbo.OrderHeader M WITH (NOLOCK)
					INNER JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH (NOLOCK) ON M.HeaderID=A.HeaderID
					INNER JOIN Ligentix_Purchase_Orders.dbo.SourceFile G WITH (NOLOCK) ON G.FileID = M.FileID
					INNER JOIN Ligentix_Purchase_Orders.dbo.ClientFeed H WITH (NOLOCK) ON H.FeedID = G.FeedID
					INNER JOIN Ligentix_Purchase_Orders.dbo.Client I WITH (NOLOCK) ON I.ClientID = H.ClientID
					INNER JOIN 
					(
						SELECT DISTINCT OrderNumber,ClientId,Lot 
						FROM [BI_Builder].dbo.ShipOrder
							--WHERE ClientId=@ClientId
					)SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=I.LigClientId 
					AND (CASE SH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(SH.Lot,'1') END) =  (CASE A.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)
					WHERE 
						M.IsLatestVersion=1
						AND M.IsCancelled=0
						AND A.IsCancelled=0
						AND ISNULL(M.Status, '')!='CANCELLED'
						AND ISNULL(A.LineStatus, '')!='CANCELLED'					
					
			)M
			JOIN Ligentix4.dbo.Client CL ON M.LigClientId=CL.ClientId
			JOIN BI_Builder.dbo.BIClient BC ON  CL.ConsigneeEDICode=BC.ConsigneeEDICode
			LEFT JOIN
			(
				SELECT pvt.OrderNumber, pvt.ClientId, pvt.LOT,
				[Ex-Works] AS EX_WORKS_ACT,
				COALESCE([BOOKED],[GoodsBookedinLigentixbysupplier],[GoodsBookedIntoLigentix],[SubmitDateofBooking],[Latest Booking]) AS SUPPLIER_BOOKED_GOODS_ACT,
				
				COALESCE([HANDOVER],[RequiredHandover],[PhysicalHandoverOfTheGoods],[Origin Port Cut Off / Cargo Hand Over]) AS RequiredHandover_ACT,
				
				COALESCE([ETD],[Departure]) AS ETD_ACT,
				COALESCE([ETA],[Arrived Date / Booked for Delivery],[Arrival],[Arrivaldatedestination]) AS ETA_ACT,		--Right
				COALESCE([CFSGATED],[CY],[CYcontainergatedinatdepartureport]) AS CYContainerGatedDeparturePort_ACT,
				COALESCE([ProductionDate],[Production Commenced],[Productionstartdate]) AS ProductionDate_ACT,
				COALESCE([ATPInspection],[Preshipment Inspection passed],[VendorInspectionDate],[Inspectioncompletedpasseddate],[Inspectionapprovaldate],[LibraInspectionDate]) AS ATPInspection_ACT,
				COALESCE([S/OconfirmedbyLigentiaoriginoffice],[SOCFRM],[SOconfirmedbyLigentiaoriginoffice]) AS SOConfirmedByLigentiaOriginOffice_ACT,
				COALESCE([BKCLOSE],[BookingWindowClosedDate],[BookingWindowClosed]) AS BookingWindowClosedDate_ACT,
				COALESCE([CargoReadyDate],[Origin Port Cut Off / Cargo Hand Over],[Orderreadytoship],[Cargo Shipped]) AS CargoReadyDate_ACT,
				[PhysicalHandoverOfTheGoods] AS PhysicalHandoverOfTheGoods_ACT,
				COALESCE([ActualDeliveryDate],[DueDeliveryDate],[RequiredDeliveryDate],[RQDDEL],[Delivered],[Warehouse Delivery],[DCdeliverycompleted],[DC delivery completed]) AS RequiredDeliveryDate_ACT,
				[Sailingdateorigin] AS SailingDateOrigin_ACT,
				COALESCE([CFS],[CFSCargoLoaded(CF)],[CFS Arrival],[ReceivedToConsolCentre],[CFSLOADED],[CFSReceiving],[ ]) AS CFSLOADED_ACT,
				COALESCE([PO booked],[PObooked],[Booked]) AS POBooked_ACT,
				COALESCE([SFD],[UK Customs Clearance],[Customs Clearance Date],[SFD Customs Clearance Date],[Customs Cleared]) AS CustomClearanceDate_ACT,
				COALESCE([MerchSAApproval],[MCHSAAPVL],[SA approved],[SAAVL]) AS MerchSAApproval_ACT,
				[NOA] AS [NOA_ACT],
				COALESCE([CCD],[Customs cleared],[CustomsClearanceDate],[Customscleared],[SFDCustomsClearanceDate],[UK Customs Clearance],[SFD]) AS [SFD_ACT],
				COALESCE([Approved],[POapproved],[PO Acknowledged to Imports],[PA]) AS [Approved_ACT],
				COALESCE([BKOPEN],[BookingWindowOpen]) AS BookingWindowOpen_ACT,
				COALESCE([GOLDSEAL],[Gold Seal Approved / Pre Shipment Booked]) AS GoldSealApproval_ACT,
				[ILCSAAPVL] AS ILCSAApproval_ACT,
				COALESCE([CNTRRLS],[ContainerReleased(CR)])								AS ContainerReleasedCR_ACT,
				COALESCE([DC booking made],[DCbookingmade])								AS  DCbookingmade_ACT,
				COALESCE([DC booking confirmed],[DCbookingconfirmed])					AS  DCbookingconfirmed_ACT, 
				COALESCE(PORaised, [POraisedorderplacement], [POraised/orderplacement])	AS	PORaised_ACT
				FROM   
				(
					SELECT M.OrderNumber, A.ClientId, A.[Key] Name, M.ActualDate, ISNULL(NULLIF(NULLIF(M.Lot, '0'), ''),'1') AS LOT
					FROM Ligentix4.dbo.OrderCriticalDate M WITH (NOLOCK)
						 INNER JOIN Ligentix4.dbo.ClientCriticalDate A WITH (NOLOCK) ON M.ClientCriticalDateId=A.ClientCriticalDateId --WHERE M.ActualDate IS NOT NULL clientId=LigClientId
				) P  
				PIVOT  
				(  
					MAX (P.ActualDate)  
					FOR P.Name IN  
					([Ex-Works],
					[GoodsBookedinLigentixbysupplier],
					[RequiredHandover],
					[ETD],
					[ETA],
					[CFSGATED],
					[ATPInspection],
					[S/OconfirmedbyLigentiaoriginoffice],
					[BookingWindowClosedDate],
					[CargoReadyDate],
					[PhysicalHandoverOfTheGoods],
					[ActualDeliveryDate],
					[Sailingdateorigin],
					[CFS],
					[PObooked],
					[Customscleared],
					[MerchSAApproval],
					[NOA],
					[SFD],
					[Approved],
					[CYcontainergatedinatdepartureport],
					[BOOKED],
					[GoodsBookedIntoLigentix],
					[HANDOVER],
					[BKCLOSE],
					[PO booked],
					[CCD],
					[CY],
					[SOCFRM],
					[DueDeliveryDate],
					[Customs cleared],
					[MCHSAAPVL],
					[SubmitDateofBooking],
					[SOconfirmedbyLigentiaoriginoffice],
					[RequiredDeliveryDate],
					[CustomsClearanceDate],
					[RQDDEL],
					[SFDCustomsClearanceDate],
					[ProductionDate],
					[POapproved],
					[UK Customs Clearance],
					[Latest Booking],
					[Origin Port Cut Off / Cargo Hand Over],
					[Departure],
					[Arrived Date / Booked for Delivery],
					[Arrival],
					[Arrivaldatedestination],
					[CFSCargoLoaded(CF)],
					[CFS Arrival],
					[ReceivedToConsolCentre],
					[CFSLOADED],
					[CFSReceiving],
					[ ],
					[Production Commenced],
					[Productionstartdate],
					[Preshipment Inspection passed],
					[VendorInspectionDate],
					[Inspectioncompletedpasseddate],
					[BookingWindowClosed],
					[Orderreadytoship],
					[Delivered],
					[Warehouse Delivery],
					[DCdeliverycompleted],
					[DC delivery completed],
					[Customs Clearance Date],
					[SFD Customs Clearance Date],
					[SA approved],
					[SAAVL],
					[PO Acknowledged to Imports],
					[PA],
					[BKOPEN],
					[BookingWindowOpen],
					[GOLDSEAL],
					[ILCSAAPVL],
					[CNTRRLS],
					[ContainerReleased(CR)],
					[DC booking made],
					[DCbookingmade],
					[DC booking confirmed],
					[DCbookingconfirmed],
					[Inspectionapprovaldate],
					[LibraInspectionDate],
					[Cargo Shipped],
					[Gold Seal Approved / Pre Shipment Booked], 
					PORaised, 
					[POraisedorderplacement], 
					[POraised/orderplacement]
				)
			) AS pvt)ACT ON M.OrderNumber=ACT.OrderNumber AND M.LigClientId=ACT.ClientId AND ISNULL(NULLIF(NULLIF(M.LOT, ''), '0'),'1')= ISNULL(NULLIF(NULLIF(ACT.LOT, ''), '0'),'1')--AND (M.LOT=ACT.LOT OR ACT.LOT is null )
			LEFT JOIN
			(
				SELECT pvt.OrderNumber, pvt.ClientId, pvt.LOT,
				[Ex-Works] AS EX_WORKS_EST,
				COALESCE([BOOKED],[GoodsBookedinLigentixbysupplier],[GoodsBookedIntoLigentix],[SubmitDateofBooking],[Latest Booking]) AS SUPPLIER_BOOKED_GOODS_EST,
				COALESCE([HANDOVER],[RequiredHandover],[PhysicalHandoverOfTheGoods],[Origin Port Cut Off / Cargo Hand Over]) AS RequiredHandover_EST,
				COALESCE([ETD],[Departure]) AS ETD_EST,
				COALESCE([ETA],[Arrived Date / Booked for Delivery],[Arrival],[Arrivaldatedestination]) AS ETA_EST, --right
				COALESCE([CFSGATED],[CY],[CYcontainergatedinatdepartureport]) AS CYContainerGatedDeparturePort_EST,
				COALESCE([ProductionDate],[Production Commenced],[Productionstartdate]) AS ProductionDate_EST,
				COALESCE([ATPInspection],[Preshipment Inspection passed],[VendorInspectionDate],[Inspectioncompletedpasseddate],[Inspectionapprovaldate],[LibraInspectionDate]) AS ATPInspection_EST,
				COALESCE([S/OconfirmedbyLigentiaoriginoffice],[SOCFRM],[SOconfirmedbyLigentiaoriginoffice]) AS SOConfirmedByLigentiaOriginOffice_EST,
				COALESCE([BKCLOSE],[BookingWindowClosedDate],[BookingWindowClosed]) AS BookingWindowClosedDate_EST,
				COALESCE([CargoReadyDate],[Origin Port Cut Off / Cargo Hand Over],[Orderreadytoship],[Cargo Shipped]) AS CargoReadyDate_EST,
				[PhysicalHandoverOfTheGoods] AS PhysicalHandoverOfTheGoods_EST,
				COALESCE([ActualDeliveryDate],[DueDeliveryDate],[RequiredDeliveryDate],[RQDDEL],[Delivered],[Warehouse Delivery],[DCdeliverycompleted],[DC delivery completed]) AS RequiredDeliveryDate_EST,
				[Sailingdateorigin] AS SailingDateOrigin_EST,
				COALESCE([CFS],[CFSCargoLoaded(CF)],[CFS Arrival],[ReceivedToConsolCentre],[CFSLOADED],[CFSReceiving],[ ]) AS CFSLOADED_EST,
				COALESCE([PO booked],[PObooked],[Booked]) AS POBooked_EST,
				COALESCE([SFD],[UK Customs Clearance],[Customs Clearance Date],[SFD Customs Clearance Date],[Customs Cleared]) AS CustomClearanceDate_EST,
				COALESCE([MerchSAApproval],[MCHSAAPVL],[SA approved],[SAAVL]) AS MerchSAApproval_EST,
				[NOA] AS [NOA_EST],
				COALESCE([CCD],[Customs cleared],[CustomsClearanceDate],[Customscleared],[SFDCustomsClearanceDate],[UK Customs Clearance],[SFD]) AS [SFD_EST],
				COALESCE([Approved],[POapproved],[PO Acknowledged to Imports],[PA]) AS [Approved_EST],
				COALESCE([BKOPEN],[BookingWindowOpen]) AS BookingWindowOpen_EST,
				COALESCE([GOLDSEAL],[Gold Seal Approved / Pre Shipment Booked])						AS GoldSealApproval_EST,
				[ILCSAAPVL] AS ILCSAApproval_EST,
				COALESCE([CNTRRLS],[ContainerReleased(CR)])											AS ContainerReleasedCR_EST,
				COALESCE([DC booking made],[DCbookingmade])											AS  DCbookingmade_EST,
				COALESCE([DC booking confirmed],[DCbookingconfirmed])								AS  DCbookingconfirmed_EST,
				COALESCE(PORaised, [POraisedorderplacement], [POraised/orderplacement])				AS	PORaised_EST
				FROM   
				(
					SELECT 
						M.OrderNumber, A.ClientId, A.[Key] Name, M.CurrentDate, ISNULL(NULLIF(NULLIF(M.Lot, '0'), ''),'1') AS LOT
					FROM 
						Ligentix4.dbo.OrderCriticalDate M WITH (NOLOCK)
						INNER JOIN Ligentix4.dbo.ClientCriticalDate A WITH (NOLOCK) ON M.ClientCriticalDateId=A.ClientCriticalDateId --WHERE M.EstimatedDate IS NOT NULL
				) P  
				PIVOT  
				(  
					MAX (P.CurrentDate)  
					FOR P.Name IN  
					([Ex-Works],
					[GoodsBookedinLigentixbysupplier],
					[RequiredHandover],
					[ETD],
					[ETA],
					[CFSGATED],
					[ATPInspection],
					[S/OconfirmedbyLigentiaoriginoffice],
					[BookingWindowClosedDate],
					[CargoReadyDate],
					[PhysicalHandoverOfTheGoods],
					[ActualDeliveryDate],
					[Sailingdateorigin],
					[CFS],
					[PObooked],
					[Customscleared],
					[MerchSAApproval],
					[NOA],
					[SFD],
					[Approved],
					[CYcontainergatedinatdepartureport],
					[BOOKED],
					[GoodsBookedIntoLigentix],
					[HANDOVER],
					[BKCLOSE],
					[PO booked],
					[CCD],
					[CY],
					[SOCFRM],
					[DueDeliveryDate],
					[Customs cleared],
					[MCHSAAPVL],
					[SubmitDateofBooking],
					[SOconfirmedbyLigentiaoriginoffice],
					[RequiredDeliveryDate],
					[CustomsClearanceDate],
					[RQDDEL],
					[SFDCustomsClearanceDate],
					[ProductionDate],
					[POapproved],
					[UK Customs Clearance],
					[Latest Booking],
					[Origin Port Cut Off / Cargo Hand Over],
					[Departure],
					[Arrived Date / Booked for Delivery],
					[Arrival],
					[Arrivaldatedestination],
					[CFSCargoLoaded(CF)],
					[CFS Arrival],
					[ReceivedToConsolCentre],
					[CFSLOADED],
					[CFSReceiving],
					[ ],
					[Production Commenced],
					[Productionstartdate],
					[Preshipment Inspection passed],
					[VendorInspectionDate],
					[Inspectioncompletedpasseddate],
					[BookingWindowClosed],
					[Orderreadytoship],
					[Delivered],
					[Warehouse Delivery],
					[DCdeliverycompleted],
					[DC delivery completed],
					[Customs Clearance Date],
					[SFD Customs Clearance Date],
					[SA approved],
					[SAAVL],
					[PO Acknowledged to Imports],
					[PA],
					[BKOPEN],
					[BookingWindowOpen],
					[GOLDSEAL],
					[ILCSAAPVL],
					[CNTRRLS],
					[ContainerReleased(CR)],
					[DC booking made],
					[DCbookingmade],
					[DC booking confirmed],
					[DCbookingconfirmed],
					[Inspectionapprovaldate],
					[LibraInspectionDate],
					[Cargo Shipped],
					[Gold Seal Approved / Pre Shipment Booked], 
					PORaised, 
					[POraisedorderplacement], 
					[POraised/orderplacement]
					)
				) AS pvt )EST ON M.OrderNumber=EST.OrderNumber AND M.LigClientId=EST.ClientId AND ISNULL(NULLIF(NULLIF(M.LOT, ''), '0'),'1')= ISNULL(NULLIF(NULLIF(EST.LOT, ''), '0'),'1')
			LEFT JOIN
			(
				SELECT pvt.OrderNumber, pvt.ClientId, pvt.LOT,
				[Ex-Works] AS EX_WORKS_ORG,
				COALESCE([BOOKED],[GoodsBookedinLigentixbysupplier],[GoodsBookedIntoLigentix],[SubmitDateofBooking],[Latest Booking]) AS SUPPLIER_BOOKED_GOODS_ORG,
				COALESCE([HANDOVER],[RequiredHandover],[PhysicalHandoverOfTheGoods],[Origin Port Cut Off / Cargo Hand Over]) AS RequiredHandover_ORG,
				COALESCE([ETD],[Departure])																AS ETD_ORG,
				COALESCE([ETA],[Arrived Date / Booked for Delivery],[Arrival],[Arrivaldatedestination]) AS ETA_ORG,
				COALESCE([CFSGATED],[CY],[CYcontainergatedinatdepartureport])							AS CYContainerGatedDeparturePort_ORG,
				COALESCE([ProductionDate],[Production Commenced],[Productionstartdate])					AS ProductionDate_ORG,
				COALESCE([ATPInspection],[Preshipment Inspection passed],[VendorInspectionDate],[Inspectioncompletedpasseddate],[Inspectionapprovaldate],[LibraInspectionDate]) AS ATPInspection_ORG,
				COALESCE([S/OconfirmedbyLigentiaoriginoffice],[SOCFRM],[SOconfirmedbyLigentiaoriginoffice]) AS SOConfirmedByLigentiaOriginOffice_ORG,
				COALESCE([BKCLOSE],[BookingWindowClosedDate],[BookingWindowClosed])							AS BookingWindowClosedDate_ORG,
				COALESCE([CargoReadyDate],[Origin Port Cut Off / Cargo Hand Over],[Orderreadytoship],[Cargo Shipped]) AS CargoReadyDate_ORG,
				[PhysicalHandoverOfTheGoods]																AS PhysicalHandoverOfTheGoods_ORG,
				COALESCE([ActualDeliveryDate],[DueDeliveryDate],[RequiredDeliveryDate],[RQDDEL],[Delivered],[Warehouse Delivery],[DCdeliverycompleted],[DC delivery completed]) AS RequiredDeliveryDate_ORG,
				[Sailingdateorigin]																				AS SailingDateOrigin_ORG,
				COALESCE([CFS],[CFSCargoLoaded(CF)],[CFS Arrival],[ReceivedToConsolCentre],[CFSLOADED],[CFSReceiving],[ ]) AS CFSLOADED_ORG,
				COALESCE([PO booked],[PObooked],[Booked])														AS POBooked_ORG,
				COALESCE([SFD],[UK Customs Clearance],[Customs Clearance Date],[SFD Customs Clearance Date],[Customs Cleared]) AS CustomClearanceDate_ORG,
				COALESCE([MerchSAApproval],[MCHSAAPVL],[SA approved],[SAAVL])									AS MerchSAApproval_ORG,
				[NOA]																							AS [NOA_ORG],
				COALESCE([CCD],[Customs cleared],[CustomsClearanceDate],[Customscleared],[SFDCustomsClearanceDate],[UK Customs Clearance],[SFD]) AS [SFD_ORG],
				COALESCE([Approved],[POapproved],[PO Acknowledged to Imports],[PA])								AS [Approved_ORG],
				COALESCE([BKOPEN],[BookingWindowOpen])															AS BookingWindowOpen_ORG,
				COALESCE([GOLDSEAL],[Gold Seal Approved / Pre Shipment Booked])									AS GoldSealApproval_ORG,
				[ILCSAAPVL]																						AS ILCSAApproval_ORG,
				COALESCE([CNTRRLS],[ContainerReleased(CR)])														AS ContainerReleasedCR_ORG,
				COALESCE([DC booking made],[DCbookingmade])														AS  DCbookingmade_ORG,
				COALESCE([DC booking confirmed],[DCbookingconfirmed])											AS  DCbookingconfirmed_ORG, 
				COALESCE(PORaised, [POraisedorderplacement], [POraised/orderplacement])							AS	PORaised_ORG
				FROM   
				(
					SELECT M.OrderNumber, A.ClientId, A.[Key] Name, M.EstimatedDate, ISNULL(NULLIF(NULLIF(M.Lot, '0'), ''),'1') LOT
					FROM Ligentix4.dbo.OrderCriticalDate M WITH(NOLOCK)
					INNER JOIN Ligentix4.dbo.ClientCriticalDate A WITH (NOLOCK) ON M.ClientCriticalDateId=A.ClientCriticalDateId --WHERE M.CurrentDate IS NOT NULL
				) P  
				PIVOT  
					(  
						MAX (P.EstimatedDate)  
						FOR P.Name IN  
						([Ex-Works],
						[GoodsBookedinLigentixbysupplier],
						[RequiredHandover],
						[ETD],
						[ETA],
						[CFSGATED],
						[ATPInspection],
						[S/OconfirmedbyLigentiaoriginoffice],
						[BookingWindowClosedDate],
						[CargoReadyDate],
						[PhysicalHandoverOfTheGoods],
						[ActualDeliveryDate],
						[Sailingdateorigin],
						[CFS],
						[PObooked],
						[Customscleared],
						[MerchSAApproval],
						[NOA],
						[SFD],
						[Approved],
						[CYcontainergatedinatdepartureport],
						[BOOKED],
						[GoodsBookedIntoLigentix],
						[HANDOVER],
						[BKCLOSE],
						[PO booked],
						[CCD],
						[CY],
						[SOCFRM],
						[DueDeliveryDate],
						[Customs cleared],
						[MCHSAAPVL],
						[SubmitDateofBooking],
						[SOconfirmedbyLigentiaoriginoffice],
						[RequiredDeliveryDate],
						[CustomsClearanceDate],
						[RQDDEL],
						[SFDCustomsClearanceDate],
						[ProductionDate],
						[POapproved],
						[UK Customs Clearance],
						[Latest Booking],
						[Origin Port Cut Off / Cargo Hand Over],
						[Departure],
						[Arrived Date / Booked for Delivery],
						[Arrival],
						[Arrivaldatedestination],
						[CFSCargoLoaded(CF)],
						[CFS Arrival],
						[ReceivedToConsolCentre],
						[CFSLOADED],
						[CFSReceiving],
						[ ],
						[Production Commenced],
						[Productionstartdate],
						[Preshipment Inspection passed],
						[VendorInspectionDate],
						[Inspectioncompletedpasseddate],
						[BookingWindowClosed],
						[Orderreadytoship],
						[Delivered],
						[Warehouse Delivery],
						[DCdeliverycompleted],
						[DC delivery completed],
						[Customs Clearance Date],
						[SFD Customs Clearance Date],
						[SA approved],
						[SAAVL],
						[PO Acknowledged to Imports],
						[PA],
						[BKOPEN],
						[BookingWindowOpen],
						[GOLDSEAL],
						[ILCSAAPVL],
						[CNTRRLS],
						[ContainerReleased(CR)],
						[DC booking made],
						[DCbookingmade],
						[DC booking confirmed],
						[DCbookingconfirmed],
						[Inspectionapprovaldate],
						[LibraInspectionDate],
						[Cargo Shipped],
						[Gold Seal Approved / Pre Shipment Booked], 
						PORaised, 
						[POraisedorderplacement], 
						[POraised/orderplacement]
						)
					) AS pvt
				)CUR ON M.OrderNumber=CUR.OrderNumber AND M.LigClientId=CUR.ClientId 
				AND ISNULL(NULLIF(NULLIF(LTRIM(RTRIM(M.LOT)), ''), '0'),'1')=ISNULL(NULLIF(NULLIF(LTRIM(RTRIM(CUR.LOT)), ''),'0'),'1') --AND (M.LOT=CUR.LOT OR CUR.LOT is null)
			--WHERE 
			--	CL.ConsigneeEDICode='ESOHENPLGDN'  AND M.OrderNumber='2017/00426' --AND M.LOT='1'
			ORDER BY 
				CL.ConsigneeEDICode, 
				M.OrderNumber, 
				M.LOT


--SELECT TOP 10 * FROM [BI_Builder].[dbo].[BIMileStoneFactPivot_new]

--SELECT T.Customer, T.OrderNumber, T.Lot, COUNT(1)
--FROM #TEMP_MILESTONE T
--GROUP BY T.Customer, T.OrderNumber, T.Lot
--HAVING COUNT(1) > 1

--SELECT T.Customer, T.OrderNumber, T.Lot, T.*
--FROM #TEMP_MILESTONE T
--WHERE T.Customer='WHISTUGBLON' AND T.OrderNumber='PO00031273' AND T.LOT=1

--SELECT T.*, C.[Key], C.ClientId FROM Ligentix4.dbo.OrderCriticalDate T 
--JOIN Ligentix4.dbo.ClientCritiCalDATE C ON T.ClientCriticalDateId=C.ClientCriticalDateId
--WHERE T.OrderNumber='2017/00426' --AND T.LOT=2 

--SELECT * FROM #TEMP_MILESTONE WHERE lOT IS NULL

--SELECT * FROM [Ligentix_Purchase_Orders].dbo.OrderHeader WHERE OrderNumber='2017/00426' 
--SELECT * FROM [Ligentix_Purchase_Orders].dbo.OrderLine WHERE HeaderId=2062222

--SELECT DISTINCT OrderNumber,ClientId,Lot 
--FROM [BI_Builder].dbo.ShipOrder WHERE OrderNumber='2017/00426' 

--SELECT * FROM Ligentix4.dbo.BookingHeader

--SELECT * FROM SYS.OBJECTS WHERE NAME like 'usp_Generic_MileStone_Report_InsertData%'


--exec sp_rename 'BIMileStoneFactPivot', 'BIMileStoneFactPivot_BKP'
--exec sp_rename 'BIMileStoneFactPivot_New', 'BIMileStoneFactPivot'


