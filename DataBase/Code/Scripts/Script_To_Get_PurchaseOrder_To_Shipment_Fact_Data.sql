
USE [BI_Builder]
GO
	IF NOT EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE NAME ='BIBookingToShipmentFact')
	BEGIN 

		CREATE TABLE BIBookingToShipmentFact
		(
			Customer					VARCHAR(100),
			ShipmentNumber				VARCHAR(100),
			BookingNumber				VARCHAR(100),
			CONSTRAINT PK_BIPOToShipmentFact_Customer_PONumber_Lot_ShipmentNumber PRIMARY KEY (Customer, ShipmentNumber, BookingNumber)
		)
	END 
	GO

	TRUNCATE TABLE BIBookingToShipmentFact
	INSERT INTO BIBookingToShipmentFact (Customer, ShipmentNumber, BookingNumber)
	SELECT   DISTINCT
		BC.ConsigneeEDICode				AS Customer, 
		SH.UniqueReference,					 
		BH.BK_NO				
	FROM 
		Ligentix4.[dbo].[Shipment] SH 
		JOIN Ligentix4.dbo.BookingHeader BH ON  BH.cw_ref = SH.UniqueReference AND SH.[LocalClientID]=BH.ClientId
		JOIN BI_Builder.[dbo].[ShipOrder] SO ON SH.[ShipmentID]=SO.[ShipmentID] 
		JOIN Ligentix4.dbo.Client C ON SH.[LocalClientID]=C.ClientId
		JOIN BI_Builder.dbo.[BIClient] BC ON C.[ConsigneeEDICode]=BC.[ConsigneeEDICode]
	WHERE 
		ShipmentCancelled=0


	



	SELECT   
		BC.ConsigneeEDICode				AS Customer, 
		SH.UniqueReference,					 
		BH.BK_NO
	FROM 
		Ligentix4.[dbo].[Shipment] SH 
		JOIN Ligentix4.dbo.BookingHeader BH ON  BH.cw_ref = SH.UniqueReference AND SH.[LocalClientID]=BH.ClientId
		JOIN Ligentix4.dbo.Client C ON SH.[LocalClientID]=C.ClientId
		JOIN BI_Builder.dbo.[BIClient] BC ON C.[ConsigneeEDICode]=BC.[ConsigneeEDICode]
		WHERE 
			SH.UniqueReference='S00501886' AND BH.BK_NO='090118008'

		SELECT SH.UniqueReference , * FROM Ligentix4.[dbo].[Shipment] SH
		WHERE SH.UniqueReference='S00610003'

		SELECT BH.CW_REF, * FROM Ligentix4.dbo.BookingHeader BH
		WHERE BH.BK_NO='090118008'

	SELECT * FROM Ligentix4.dbo.Client  WHERE ClientId=17799

	SELECT * FROM BIShipmentFact where ConsoleId IN ('72243', '74044')

	SELECT * FROM [Ligentix4].[dbo].[Consol]

	SELECT * FROM [CW_ONE].[cw1test1db].[dbo].JobShipment js
	INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConShipLink JN WITH(NOLOCK)  On JN.JN_JS=JS.JS_PK
	INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsol JK WITH(NOLOCK)  On JK.JK_PK=JN.JN_JK	
	INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobContainer JC WITH(NOLOCK)  ON JC.JC_JK=	JK.JK_PK
	INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefContainer RC WITH(NOLOCK)  On JC.JC_RC=RC.RC_PK AND JC.JC_JK=JK.JK_PK --AND JC.JC_ContainerNum=JO.JO_ContainerNumber
	where JS.JS_UniqueConsignRef='S00501886'