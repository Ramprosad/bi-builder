SELECT
			M.ClientId AS ClientId,
			M.OrderNumber,
			ISNULL(NULLIF(M.LOT, ''), '1')	AS LOT,
			MAX(M.ETA)						AS ETA,
			MAX(M.ETD)						AS ETD,
			M.POL,
			M.POD,
			M.ModeType,
			SUM(M.ShipQty)					AS ShipQty,
			SUM(M.ShipVolume)				AS ShipVolume,
			M.[Country],
			MAX(M.BookingRef)				AS BookingRef,
			M.Carrier,
			M.MOT,
			SUM(ShipWeight)					AS ShipWeight
			INTO [BI_Builder].dbo.Shipment_Test
			FROM (
				SELECT DISTINCT spo.OrderNumber
					,S.[LocalClientID] AS ClientId
					,S.[BookingRef]
					,spo.LOT
					,mv.POL
					,mv.POD
					,mv.ETD
					,mv.ETA
					,S.ModeType
					,com.ContainerID
					,[Country]
					, C3.ClientName							AS Carrier
					,SUM(spol.ShipQty)						AS ShipQty
					,SUM(spol.ShipVolume)					AS ShipVolume
					,LU_MOT.[Description]					AS MOT
					,ISNULL(SUM(spol.ShipWeight), 0)		AS ShipWeight    --RPM 2019-01-22
					,LineNumber
					,ItemCode
				FROM
					[dbo].[Shipment] S WITH(NOLOCK)
					INNER JOIN MotherVoyage mv WITH(NOLOCK) ON mv.MotherVoyageID = S.MotherVoyageID
					INNER JOIN Shipment_PurchaseOrder spo WITH(NOLOCK) ON spo.ShipmentID = S.ShipmentID
					INNER JOIN Shipment_PurchaseOrderLine spol WITH(NOLOCK) ON spol.HeaderID = spo.SPO_ID
					INNER JOIN Container_OrderLine_Match com WITH(NOLOCK) ON com.OrderLineID = spol.LineID
					INNER JOIN 
						(SELECT DISTINCT OrderNumber,ClientId,Lot,ShipmentID FROM [BI_Builder].dbo.ShipOrder)
					SO ON S.ShipmentID = SO.ShipmentID AND spo.OrderNumber = SO.OrderNumber AND SO.ClientId=S.[LocalClientID] 
						  AND (CASE WHEN spo.Lot='' THEN '1' ELSE ISNULL(spo.Lot,'1') END) = (CASE WHEN SO.Lot='' THEN '1' ELSE ISNULL(SO.Lot,'1') END) --Avoid duplicate data --Asish 2019-02-11
					LEFT JOIN 
						(SELECT DISTINCT PortCode,CASE [Country]
								WHEN ' Aichi",Japan' THEN 'Japan'
								WHEN ' Java",Indonesia' THEN 'Indonesia'
								WHEN ' Johor",Malaysia' THEN 'Malaysia'
								WHEN '"Korea, Republic of"' THEN 'Korea'
								WHEN 'Kingdom of Cambodia' THEN 'CAMBODIA'
								WHEN 'Viet Nam' THEN 'Vietnam'
								WHEN ' Cebu",Philippines' THEN 'Philippines'
								WHEN ' Georgetown",United States' THEN 'United States'
								ELSE [Country] END AS [Country]
					FROM [dbo].[Ports] WITH(NOLOCK))Port ON Port.[PortCode]=mv.POL --Avoid duplicate data --Asish 2019-02-11
					LEFT JOIN  Consol L WITH(NOLOCK) on L.ConsolID = S.ConsolID --INNER TO LEFT    --RPM 2019-01-22
					LEFT JOIN Client C3 WITH(NOLOCK) ON C3.ClientID = L.CarrierID
					LEFT JOIN Ligentix4.dbo.luShipment_TransportMode LU_MOT WITH(NOLOCK) ON S.ModeOfTransportID=LU_MOT.ID --INNER TO LEFT    --RPM 2019-01-22
				WHERE 
					S.[LocalClientID] IS NOT NULL
				GROUP BY 
					spo.OrderNumber, S.[LocalClientID], S.[BookingRef]
					, mv.POL, mv.POD, mv.ETD, mv.ETA, S.ModeType
					, com.ContainerID
					, [Country]
					, spo.LOT, C3.ClientName, LU_MOT.[Description]
					, LineNumber,ItemCode
				)M
				INNER JOIN 
				(
					SELECT DISTINCT M.OrderNumber, I.LigClientId AS ClientId, A.LOT,A.lineNum,A.ItemCode--, C.CW_REF AS ShipmentID
					FROM Ligentix_Purchase_Orders.dbo.OrderHeader M WITH(NOLOCK)
							INNER JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH(NOLOCK) ON M.HeaderID=A.HeaderID
							INNER JOIN Ligentix4.dbo.BookingItem B WITH(NOLOCK) ON A.LineId=B.OrderLineId
							INNER JOIN Ligentix4.dbo.BookingHeader C WITH(NOLOCK) ON B.BookingId=C.BookingId
							INNER JOIN Ligentix_Purchase_Orders.dbo.SourceFile AS G WITH(NOLOCK) ON G.FileID = M.FileID
							INNER JOIN Ligentix_Purchase_Orders.dbo.ClientFeed AS H WITH(NOLOCK) ON H.FeedID = G.FeedID
							INNER JOIN Ligentix_Purchase_Orders.dbo.Client AS I WITH(NOLOCK) ON I.ClientID = H.ClientID
					WHERE C.statusid NOT IN (2,4) AND B.StatusId NOT IN (3,6) --To avoid cancel booking
				) BI ON BI.OrderNumber = M.OrderNumber AND BI.ClientId = M.ClientId AND (CASE M.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(M.Lot,'1') END) = (CASE BI.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(BI.Lot,'1') END)
						AND ((M.ClientId = 2 AND ((M.LineNumber=BI.lineNum) AND (M.ItemCode=BI.ItemCode))) OR M.ClientId <> 2)
			GROUP BY 
				M.ClientId,
				M.OrderNumber,
				M.LOT,
				M.[BookingRef],
				M.POL,
				M.POD,
				M.ModeType,
				M.[Country],
				M.Carrier,
				M.MOT
			ORDER BY 
				M.ClientId, M.OrderNumber, M.LOT