		DECLARE @UserDessesionTmp TABLE (Hob VARCHAR(200) NULL,
								 BsName VARCHAR(200) NULL,
								 BuyerName VARCHAR(200) NULL,
								 Diss VARCHAR(200) NULL);
		INSERT INTO @UserDessesionTmp
		SELECT bg.Name AS Hob,bs.Name AS BsName,sb.Name AS BuyerName,sb.Diss
		FROM BuyingGroup bg WITH (NOLOCK)
			 INNER JOIN Buyership bs WITH (NOLOCK) ON bs.BuyinggroupId = bg.Id
			 INNER JOIN subbuyer sb WITH (NOLOCK) ON sb.BuyershipID = bs.Id
				
		SELECT DISTINCT
				M.LigClientId AS ClientId,
				M.OrderNumber,
				M.LOT,
				EX_WORKS_ACT,
				EX_WORKS_EST,
				EX_WORKS_CUR,
				SUPPLIER_BOOKED_GOODS_ACT,
				SUPPLIER_BOOKED_GOODS_EST,
				SUPPLIER_BOOKED_GOODS_CUR,
				RequiredHandover_ACT,
				RequiredHandover_EST,
				RequiredHandover_CUR,
				ETD_ACT,
				ETD_EST,
				ETD_CUR,
				ETA_ACT,
				ETA_EST,
				ETA_CUR,
				CYContainerGatedDeparturePort_ACT,
				CYContainerGatedDeparturePort_EST,
				CYContainerGatedDeparturePort_CUR,
				ProductionDate_ACT,
				ProductionDate_EST,
				ProductionDate_CUR,
				ATPInspection_ACT,
				ATPInspection_EST,
				ATPInspection_CUR,
				SOConfirmedByLigentiaOriginOffice_ACT,
				SOConfirmedByLigentiaOriginOffice_EST,
				SOConfirmedByLigentiaOriginOffice_CUR,
				BookingWindowClosedDate_ACT,
				BookingWindowClosedDate_EST,
				BookingWindowClosedDate_CUR,
				CargoReadyDate_ACT,
				CargoReadyDate_EST,
				CargoReadyDate_CUR,
				PhysicalHandoverOfTheGoods_ACT,
				PhysicalHandoverOfTheGoods_EST,
				PhysicalHandoverOfTheGoods_CUR,
				RequiredDeliveryDate_ACT,
				RequiredDeliveryDate_EST,
				RequiredDeliveryDate_CUR,
				SailingDateOrigin_ACT,
				SailingDateOrigin_EST,
				SailingDateOrigin_CUR,
				CFSLOADED_ACT,
				CFSLOADED_EST,
				CFSLOADED_CUR,
				POBooked_ACT,
				POBooked_EST,
				POBooked_CUR,
				CustomClearanceDate_ACT,
				CustomClearanceDate_EST,
				CustomClearanceDate_CUR,
				MerchSAApproval_ACT,
				MerchSAApproval_EST,
				MerchSAApproval_CUR,
				NOA_ACT,
				NOA_EST,
				NOA_CUR,
				SFD_ACT,
				SFD_EST,
				SFD_CUR,
				Approved_ACT,
				Approved_EST,
				Approved_CUR,
				BookingWindowOpen_ACT,
				BookingWindowOpen_EST,
				BookingWindowOpen_CUR,
				GoldSealApproval_ACT,
				GoldSealApproval_EST,
				GoldSealApproval_CUR,
				ILCSAApproval_ACT,
				ILCSAApproval_EST,
				ILCSAApproval_CUR,
				ContainerReleasedCR_ACT,
				ContainerReleasedCR_EST,
				ContainerReleasedCR_CUR,
				DCbookingmade_ACT,
				DCbookingmade_EST,
				DCbookingmade_CUR,
				DCbookingconfirmed_ACT,
				DCbookingconfirmed_EST,
				DCbookingconfirmed_CUR,
				Carrier,
				Vessel,
				Mode,
				LoadType,
				POL,
				POD,
				HOB,
				BuyerName,
				WarehouseName,
				ShipmentRef_CW,
				Drops,
				ContainerNo,
				SupplierName,
				OrderQty,
				ApprovedQty,
				ShipQty,
				CountSA,
				CountETA,
				CountReBooking
			INTO [BI_Builder].dbo.Milestone_Test
			FROM
				(SELECT DISTINCT 
					 M.OrderNumber
					,I.LigClientId,A.LOT
					,VS.carrier AS Carrier
					,VS.Vessel AS Vessel
					,VS.ContainerMode AS Mode
					,VS.POL
					,VS.POD
					,dis.Hob AS HOB
					,dis.BuyerName AS BuyerName
					,LTRIM(RTRIM(landside.WarehouseName)) AS WarehouseName
					,VS.UniqueReference AS ShipmentRef_CW
					,SUM(Drops) AS Drops
					,VS.ContainerNumber AS ContainerNo
					,MAX(M.SupplierName) AS SupplierName
					,SUM(A.OrderQty) AS OrderQty
					,SUM(bi.ApprovedQty) AS ApprovedQty
					,SUM(VS.ShipQty) AS ShipQty
					,SUM(ISNULL(bi.BookedCBM,0)) AS OrderedCBM
					,SUM(bi.BookedCBM) AS ApprovedCBM
					,SUM(VS.ShippedCBM) AS ShippedCBM
					,SUM(CountSA) AS CountSA
					,SUM(CountETA) AS CountETA
					,MAX(CountReBooking) AS CountReBooking
					,VS.DeliveryMode AS LoadType
				FROM 
					Ligentix_Purchase_Orders.dbo.OrderHeader M WITH (NOLOCK)
					INNER JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH (NOLOCK) ON M.HeaderID=A.HeaderID
					INNER JOIN Ligentix4.dbo.BookingItem B WITH (NOLOCK) ON A.LineId=B.OrderLineId AND (CASE A.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END) = (CASE B.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(B.Lot,'1') END)
					INNER JOIN Ligentix4.dbo.BookingHeader C WITH (NOLOCK) ON B.BookingId=C.BookingId
					INNER JOIN Ligentix_Purchase_Orders.dbo.SourceFile G WITH (NOLOCK) ON G.FileID = M.FileID
					INNER JOIN Ligentix_Purchase_Orders.dbo.ClientFeed H WITH (NOLOCK) ON H.FeedID = G.FeedID
					INNER JOIN Ligentix_Purchase_Orders.dbo.Client I WITH (NOLOCK) ON I.ClientID = H.ClientID
					INNER JOIN 
						(SELECT DISTINCT OrderNumber,ClientId,Lot FROM [BI_Builder].dbo.ShipOrder)
					SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=I.LigClientId AND (CASE SH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(SH.Lot,'1') END) =  (CASE A.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)
					LEFT JOIN BookingVessel BV WITH (NOLOCK) ON B.BookingId=BV.BookingId AND BV.Type=1
					LEFT JOIN 
					( 
						SELECT bh.clientid,bi.bookingid,bi.ordernumber,bi.lot,SUM(ISNULL(bi.inpkg,0)) AS ApprovedQty,SUM(ISNULL(bi.cbm,0)) AS BookedCBM
						FROM bookingheader bh WITH (NOLOCK)
							 INNER JOIN bookingitem bi WITH (NOLOCK) on bh.bookingid=bi.bookingid
						WHERE bh.statusid not in (2,4) and bi.statusid not in (3,6) 
						GROUP BY bh.clientid,bi.bookingid,bi.ordernumber,bi.lot
					) bi ON bi.clientid = I.LigClientID AND bi.ordernumber=M.ordernumber AND bi.lot=A.lot
					LEFT JOIN 
					(
						SELECT ClientID,OrderNumber,Lot,ContainerNumber,DeliveryMode,ContainerMode,carrier,Vessel,POL,POD,UniqueReference,LineNumber,ItemCode,SUM(ShipQty) AS ShipQty,SUM(ShipVolume) AS ShippedCBM
						FROM vw_ShippingComplete WITH (NOLOCK)
						GROUP BY ClientID, OrderNumber, Lot, ContainerNumber, DeliveryMode, ContainerMode,carrier,Vessel,POL,POD,UniqueReference,LineNumber,ItemCode
					) VS ON I.LigClientID=VS.ClientId and M.OrderNumber = VS.OrderNumber AND (CASE A.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END) = (CASE VS.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(VS.Lot,'1') END) AND VS.LineNumber=A.lineNum AND VS.ItemCode=A.itemcode
					LEFT JOIN 
					(
						SELECT ClientId, bookingid, OrderNumber, COUNT(OrderNumber) AS CountSA
						FROM OrderAudit WITH (NOLOCK)
						WHERE LogType IN (9, 10) GROUP BY ClientId, bookingid, OrderNumber
					) SA ON I.LigClientID=SA.ClientId AND M.OrderNumber = SA.OrderNumber AND C.bookingid = SA.bookingid
					LEFT JOIN 
					(
						SELECT ClientId, bookingid, OrderNumber, COUNT(OrderNumber) AS CountETA
						FROM OrderAudit WITH (NOLOCK) 
						WHERE LogType IN (6) GROUP BY ClientId, bookingid, OrderNumber
					) ETA ON I.LigClientID=ETA.ClientId AND M.OrderNumber = ETA.OrderNumber AND C.bookingid = ETA.bookingid
					LEFT JOIN 
					(
						SELECT lsb.ClientId, lsol.OrderNumber, lsol.Lot, lsc.ContainerNumber, whs.WarehouseName, MAX(lsb.ReBookCount) AS CountReBooking, COUNT(lsb.LandSideBookingId) AS Drops
						FROM LandSideBooking lsb WITH (NOLOCK)
							 JOIN LandSideContainer lsc WITH (NOLOCK) ON lsc.LandSideContainerId = lsb.LandSideContainerId
							 JOIN LandSideOrderLine lsol WITH (NOLOCK) ON lsol.LandSideContainerId = lsc.LandSideContainerId AND lsol.LandSideBookingId = lsb.LandSideBookingId
							 LEFT JOIN warehouse whs WITH (NOLOCK) ON lsb.WarehouseId = whs.WarehouseId
						GROUP BY lsb.ClientId,lsol.OrderNumber, lsol.Lot, lsc.ContainerNumber, whs.WarehouseName
					) AS landside ON I.LigClientID = landside.ClientId AND M.OrderNumber = landside.OrderNumber AND (CASE A.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END) = (CASE landside.Lot WHEN '' THEN '1'  WHEN '0' THEN '1' ELSE ISNULL(landside.Lot,'1') END) AND VS.ContainerNumber = landside.ContainerNumber		
					LEFT JOIN @UserDessesionTmp dis ON M.BusinessUnit = dis.Diss
				GROUP BY 
					M.OrderNumber, I.LigClientId,A.LOT,VS.carrier,VS.Vessel,VS.POL,VS.POD,dis.Hob,dis.BuyerName,landside.WarehouseName,VS.UniqueReference,VS.ContainerNumber,VS.DeliveryMode,VS.ContainerMode
			)M
			LEFT JOIN
			(
				SELECT pvt.OrderNumber, pvt.ClientId, pvt.LOT,
				[Ex-Works] AS EX_WORKS_ACT,
				COALESCE([BOOKED],[GoodsBookedinLigentixbysupplier],[GoodsBookedIntoLigentix],[SubmitDateofBooking],[Latest Booking]) AS SUPPLIER_BOOKED_GOODS_ACT,
				COALESCE([HANDOVER],[RequiredHandover],[PhysicalHandoverOfTheGoods],[Origin Port Cut Off / Cargo Hand Over]) AS RequiredHandover_ACT,
				COALESCE([ETD],[Departure]) AS ETD_ACT,
				COALESCE([ETA],[Arrived Date / Booked for Delivery],[Arrival],[Arrivaldatedestination]) AS ETA_ACT,
				COALESCE([CFSGATED],[CY],[CYcontainergatedinatdepartureport]) AS CYContainerGatedDeparturePort_ACT,
				COALESCE([ProductionDate],[Production Commenced],[Productionstartdate]) AS ProductionDate_ACT,
				COALESCE([ATPInspection],[Preshipment Inspection passed],[VendorInspectionDate],[Inspectioncompletedpasseddate],[Inspectionapprovaldate],[LibraInspectionDate]) AS ATPInspection_ACT,
				COALESCE([S/OconfirmedbyLigentiaoriginoffice],[SOCFRM],[SOconfirmedbyLigentiaoriginoffice]) AS SOConfirmedByLigentiaOriginOffice_ACT,
				COALESCE([BKCLOSE],[BookingWindowClosedDate],[BookingWindowClosed]) AS BookingWindowClosedDate_ACT,
				COALESCE([CargoReadyDate],[Origin Port Cut Off / Cargo Hand Over],[Orderreadytoship],[Cargo Shipped]) AS CargoReadyDate_ACT,
				[PhysicalHandoverOfTheGoods] AS PhysicalHandoverOfTheGoods_ACT,
				COALESCE([ActualDeliveryDate],[DueDeliveryDate],[RequiredDeliveryDate],[RQDDEL],[Delivered],[Warehouse Delivery],[DCdeliverycompleted],[DC delivery completed]) AS RequiredDeliveryDate_ACT,
				[Sailingdateorigin] AS SailingDateOrigin_ACT,
				COALESCE([CFS],[CFSCargoLoaded(CF)],[CFS Arrival],[ReceivedToConsolCentre],[CFSLOADED],[CFSReceiving],[ ]) AS CFSLOADED_ACT,
				COALESCE([PO booked],[PObooked],[Booked]) AS POBooked_ACT,
				COALESCE([SFD],[UK Customs Clearance],[Customs Clearance Date],[SFD Customs Clearance Date],[Customs Cleared]) AS CustomClearanceDate_ACT,
				COALESCE([MerchSAApproval],[MCHSAAPVL],[SA approved],[SAAVL]) AS MerchSAApproval_ACT,
				[NOA] AS [NOA_ACT],
				COALESCE([CCD],[Customs cleared],[CustomsClearanceDate],[Customscleared],[SFDCustomsClearanceDate],[UK Customs Clearance],[SFD]) AS [SFD_ACT],
				COALESCE([Approved],[POapproved],[PO Acknowledged to Imports],[PA]) AS [Approved_ACT],
				COALESCE([BKOPEN],[BookingWindowOpen]) AS BookingWindowOpen_ACT,
				COALESCE([GOLDSEAL],[Gold Seal Approved / Pre Shipment Booked]) AS GoldSealApproval_ACT,
				[ILCSAAPVL] AS ILCSAApproval_ACT,
				COALESCE([CNTRRLS],[ContainerReleased(CR)]) AS ContainerReleasedCR_ACT,
				COALESCE([DC booking made],[DCbookingmade]) AS  DCbookingmade_ACT,
				COALESCE([DC booking confirmed],[DCbookingconfirmed]) AS  DCbookingconfirmed_ACT
				FROM   
				(
					SELECT M.OrderNumber, A.ClientId, A.[Key] Name, M.ActualDate, ISNULL(M.Lot,'1') AS LOT
					FROM OrderCriticalDate M WITH (NOLOCK)
						 INNER JOIN ClientCriticalDate A WITH (NOLOCK) ON M.ClientCriticalDateId=A.ClientCriticalDateId --WHERE M.ActualDate IS NOT NULL clientId=LigClientId
				) P  
				PIVOT  
				(  
					MAX (P.ActualDate)  
					FOR P.Name IN  
					([Ex-Works],
					[GoodsBookedinLigentixbysupplier],
					[RequiredHandover],
					[ETD],
					[ETA],
					[CFSGATED],
					[ATPInspection],
					[S/OconfirmedbyLigentiaoriginoffice],
					[BookingWindowClosedDate],
					[CargoReadyDate],
					[PhysicalHandoverOfTheGoods],
					[ActualDeliveryDate],
					[Sailingdateorigin],
					[CFS],
					[PObooked],
					[Customscleared],
					[MerchSAApproval],
					[NOA],
					[SFD],
					[Approved],
					[CYcontainergatedinatdepartureport],
					[BOOKED],
					[GoodsBookedIntoLigentix],
					[HANDOVER],
					[BKCLOSE],
					[PO booked],
					[CCD],
					[CY],
					[SOCFRM],
					[DueDeliveryDate],
					[Customs cleared],
					[MCHSAAPVL],
					[SubmitDateofBooking],
					[SOconfirmedbyLigentiaoriginoffice],
					[RequiredDeliveryDate],
					[CustomsClearanceDate],
					[RQDDEL],
					[SFDCustomsClearanceDate],
					[ProductionDate],
					[POapproved],
					[UK Customs Clearance],
					[Latest Booking],
					[Origin Port Cut Off / Cargo Hand Over],
					[Departure],
					[Arrived Date / Booked for Delivery],
					[Arrival],
					[Arrivaldatedestination],
					[CFSCargoLoaded(CF)],
					[CFS Arrival],
					[ReceivedToConsolCentre],
					[CFSLOADED],
					[CFSReceiving],
					[ ],
					[Production Commenced],
					[Productionstartdate],
					[Preshipment Inspection passed],
					[VendorInspectionDate],
					[Inspectioncompletedpasseddate],
					[BookingWindowClosed],
					[Orderreadytoship],
					[Delivered],
					[Warehouse Delivery],
					[DCdeliverycompleted],
					[DC delivery completed],
					[Customs Clearance Date],
					[SFD Customs Clearance Date],
					[SA approved],
					[SAAVL],
					[PO Acknowledged to Imports],
					[PA],
					[BKOPEN],
					[BookingWindowOpen],
					[GOLDSEAL],
					[ILCSAAPVL],
					[CNTRRLS],
					[ContainerReleased(CR)],
					[DC booking made],
					[DCbookingmade],
					[DC booking confirmed],
					[DCbookingconfirmed],
					[Inspectionapprovaldate],
					[LibraInspectionDate],
					[Cargo Shipped],
					[Gold Seal Approved / Pre Shipment Booked]
				)
			) AS pvt)ACT ON M.OrderNumber=ACT.OrderNumber AND M.LigClientId=ACT.ClientId AND ISNULL(M.LOT,'1')=ISNULL(ACT.LOT,'1') --AND (M.LOT=ACT.LOT OR ACT.LOT is null )
			LEFT JOIN
			(
				SELECT pvt.OrderNumber, pvt.ClientId, pvt.LOT,
				[Ex-Works] AS EX_WORKS_EST,
				COALESCE([BOOKED],[GoodsBookedinLigentixbysupplier],[GoodsBookedIntoLigentix],[SubmitDateofBooking],[Latest Booking]) AS SUPPLIER_BOOKED_GOODS_EST,
				COALESCE([HANDOVER],[RequiredHandover],[PhysicalHandoverOfTheGoods],[Origin Port Cut Off / Cargo Hand Over]) AS RequiredHandover_EST,
				COALESCE([ETD],[Departure]) AS ETD_EST,
				COALESCE([ETA],[Arrived Date / Booked for Delivery],[Arrival],[Arrivaldatedestination]) AS ETA_EST,
				COALESCE([CFSGATED],[CY],[CYcontainergatedinatdepartureport]) AS CYContainerGatedDeparturePort_EST,
				COALESCE([ProductionDate],[Production Commenced],[Productionstartdate]) AS ProductionDate_EST,
				COALESCE([ATPInspection],[Preshipment Inspection passed],[VendorInspectionDate],[Inspectioncompletedpasseddate],[Inspectionapprovaldate],[LibraInspectionDate]) AS ATPInspection_EST,
				COALESCE([S/OconfirmedbyLigentiaoriginoffice],[SOCFRM],[SOconfirmedbyLigentiaoriginoffice]) AS SOConfirmedByLigentiaOriginOffice_EST,
				COALESCE([BKCLOSE],[BookingWindowClosedDate],[BookingWindowClosed]) AS BookingWindowClosedDate_EST,
				COALESCE([CargoReadyDate],[Origin Port Cut Off / Cargo Hand Over],[Orderreadytoship],[Cargo Shipped]) AS CargoReadyDate_EST,
				[PhysicalHandoverOfTheGoods] AS PhysicalHandoverOfTheGoods_EST,
				COALESCE([ActualDeliveryDate],[DueDeliveryDate],[RequiredDeliveryDate],[RQDDEL],[Delivered],[Warehouse Delivery],[DCdeliverycompleted],[DC delivery completed]) AS RequiredDeliveryDate_EST,
				[Sailingdateorigin] AS SailingDateOrigin_EST,
				COALESCE([CFS],[CFSCargoLoaded(CF)],[CFS Arrival],[ReceivedToConsolCentre],[CFSLOADED],[CFSReceiving],[ ]) AS CFSLOADED_EST,
				COALESCE([PO booked],[PObooked],[Booked]) AS POBooked_EST,
				COALESCE([SFD],[UK Customs Clearance],[Customs Clearance Date],[SFD Customs Clearance Date],[Customs Cleared]) AS CustomClearanceDate_EST,
				COALESCE([MerchSAApproval],[MCHSAAPVL],[SA approved],[SAAVL]) AS MerchSAApproval_EST,
				[NOA] AS [NOA_EST],
				COALESCE([CCD],[Customs cleared],[CustomsClearanceDate],[Customscleared],[SFDCustomsClearanceDate],[UK Customs Clearance],[SFD]) AS [SFD_EST],
				COALESCE([Approved],[POapproved],[PO Acknowledged to Imports],[PA]) AS [Approved_EST],
				COALESCE([BKOPEN],[BookingWindowOpen]) AS BookingWindowOpen_EST,
				COALESCE([GOLDSEAL],[Gold Seal Approved / Pre Shipment Booked]) AS GoldSealApproval_EST,
				[ILCSAAPVL] AS ILCSAApproval_EST,
				COALESCE([CNTRRLS],[ContainerReleased(CR)]) AS ContainerReleasedCR_EST,
				COALESCE([DC booking made],[DCbookingmade]) AS  DCbookingmade_EST,
				COALESCE([DC booking confirmed],[DCbookingconfirmed]) AS  DCbookingconfirmed_EST
				FROM   
				(
					SELECT M.OrderNumber, A.ClientId, A.[Key] Name, M.EstimatedDate, ISNULL(M.Lot,'1') AS LOT
					FROM OrderCriticalDate M WITH (NOLOCK)
						 INNER JOIN ClientCriticalDate A WITH (NOLOCK) ON M.ClientCriticalDateId=A.ClientCriticalDateId --WHERE M.EstimatedDate IS NOT NULL
				) P  
				PIVOT  
				(  
					MAX (P.EstimatedDate)  
					FOR P.Name IN  
					([Ex-Works],
					[GoodsBookedinLigentixbysupplier],
					[RequiredHandover],
					[ETD],
					[ETA],
					[CFSGATED],
					[ATPInspection],
					[S/OconfirmedbyLigentiaoriginoffice],
					[BookingWindowClosedDate],
					[CargoReadyDate],
					[PhysicalHandoverOfTheGoods],
					[ActualDeliveryDate],
					[Sailingdateorigin],
					[CFS],
					[PObooked],
					[Customscleared],
					[MerchSAApproval],
					[NOA],
					[SFD],
					[Approved],
					[CYcontainergatedinatdepartureport],
					[BOOKED],
					[GoodsBookedIntoLigentix],
					[HANDOVER],
					[BKCLOSE],
					[PO booked],
					[CCD],
					[CY],
					[SOCFRM],
					[DueDeliveryDate],
					[Customs cleared],
					[MCHSAAPVL],
					[SubmitDateofBooking],
					[SOconfirmedbyLigentiaoriginoffice],
					[RequiredDeliveryDate],
					[CustomsClearanceDate],
					[RQDDEL],
					[SFDCustomsClearanceDate],
					[ProductionDate],
					[POapproved],
					[UK Customs Clearance],
					[Latest Booking],
					[Origin Port Cut Off / Cargo Hand Over],
					[Departure],
					[Arrived Date / Booked for Delivery],
					[Arrival],
					[Arrivaldatedestination],
					[CFSCargoLoaded(CF)],
					[CFS Arrival],
					[ReceivedToConsolCentre],
					[CFSLOADED],
					[CFSReceiving],
					[ ],
					[Production Commenced],
					[Productionstartdate],
					[Preshipment Inspection passed],
					[VendorInspectionDate],
					[Inspectioncompletedpasseddate],
					[BookingWindowClosed],
					[Orderreadytoship],
					[Delivered],
					[Warehouse Delivery],
					[DCdeliverycompleted],
					[DC delivery completed],
					[Customs Clearance Date],
					[SFD Customs Clearance Date],
					[SA approved],
					[SAAVL],
					[PO Acknowledged to Imports],
					[PA],
					[BKOPEN],
					[BookingWindowOpen],
					[GOLDSEAL],
					[ILCSAAPVL],
					[CNTRRLS],
					[ContainerReleased(CR)],
					[DC booking made],
					[DCbookingmade],
					[DC booking confirmed],
					[DCbookingconfirmed],
					[Inspectionapprovaldate],
					[LibraInspectionDate],
					[Cargo Shipped],
					[Gold Seal Approved / Pre Shipment Booked]
					)
				) AS pvt )EST ON M.OrderNumber=EST.OrderNumber AND M.LigClientId=EST.ClientId AND ISNULL(M.LOT,'1')=ISNULL(EST.LOT,'1')
			LEFT JOIN
			(
				SELECT pvt.OrderNumber, pvt.ClientId, pvt.LOT,
				[Ex-Works] AS EX_WORKS_CUR,
				COALESCE([BOOKED],[GoodsBookedinLigentixbysupplier],[GoodsBookedIntoLigentix],[SubmitDateofBooking],[Latest Booking]) AS SUPPLIER_BOOKED_GOODS_CUR,
				COALESCE([HANDOVER],[RequiredHandover],[PhysicalHandoverOfTheGoods],[Origin Port Cut Off / Cargo Hand Over]) AS RequiredHandover_CUR,
				COALESCE([ETD],[Departure]) AS ETD_CUR,
				COALESCE([ETA],[Arrived Date / Booked for Delivery],[Arrival],[Arrivaldatedestination]) AS ETA_CUR,
				COALESCE([CFSGATED],[CY],[CYcontainergatedinatdepartureport]) AS CYContainerGatedDeparturePort_CUR,
				COALESCE([ProductionDate],[Production Commenced],[Productionstartdate]) AS ProductionDate_CUR,
				COALESCE([ATPInspection],[Preshipment Inspection passed],[VendorInspectionDate],[Inspectioncompletedpasseddate],[Inspectionapprovaldate],[LibraInspectionDate]) AS ATPInspection_CUR,
				COALESCE([S/OconfirmedbyLigentiaoriginoffice],[SOCFRM],[SOconfirmedbyLigentiaoriginoffice]) AS SOConfirmedByLigentiaOriginOffice_CUR,
				COALESCE([BKCLOSE],[BookingWindowClosedDate],[BookingWindowClosed]) AS BookingWindowClosedDate_CUR,
				COALESCE([CargoReadyDate],[Origin Port Cut Off / Cargo Hand Over],[Orderreadytoship],[Cargo Shipped]) AS CargoReadyDate_CUR,
				[PhysicalHandoverOfTheGoods] AS PhysicalHandoverOfTheGoods_CUR,
				COALESCE([ActualDeliveryDate],[DueDeliveryDate],[RequiredDeliveryDate],[RQDDEL],[Delivered],[Warehouse Delivery],[DCdeliverycompleted],[DC delivery completed]) AS RequiredDeliveryDate_CUR,
				[Sailingdateorigin] AS SailingDateOrigin_CUR,
				COALESCE([CFS],[CFSCargoLoaded(CF)],[CFS Arrival],[ReceivedToConsolCentre],[CFSLOADED],[CFSReceiving],[ ]) AS CFSLOADED_CUR,
				COALESCE([PO booked],[PObooked],[Booked]) AS POBooked_CUR,
				COALESCE([SFD],[UK Customs Clearance],[Customs Clearance Date],[SFD Customs Clearance Date],[Customs Cleared]) AS CustomClearanceDate_CUR,
				COALESCE([MerchSAApproval],[MCHSAAPVL],[SA approved],[SAAVL]) AS MerchSAApproval_CUR,
				[NOA] AS [NOA_CUR],
				COALESCE([CCD],[Customs cleared],[CustomsClearanceDate],[Customscleared],[SFDCustomsClearanceDate],[UK Customs Clearance],[SFD]) AS [SFD_CUR],
				COALESCE([Approved],[POapproved],[PO Acknowledged to Imports],[PA]) AS [Approved_CUR],
				COALESCE([BKOPEN],[BookingWindowOpen]) AS BookingWindowOpen_CUR,
				COALESCE([GOLDSEAL],[Gold Seal Approved / Pre Shipment Booked]) AS GoldSealApproval_CUR,
				[ILCSAAPVL] AS ILCSAApproval_CUR,
				COALESCE([CNTRRLS],[ContainerReleased(CR)]) AS ContainerReleasedCR_CUR,
				COALESCE([DC booking made],[DCbookingmade]) AS  DCbookingmade_CUR,
				COALESCE([DC booking confirmed],[DCbookingconfirmed]) AS  DCbookingconfirmed_CUR
				FROM   
				(
					SELECT M.OrderNumber, A.ClientId, A.[Key] Name, M.CurrentDate, ISNULL(M.Lot,'1') LOT
					FROM OrderCriticalDate M WITH(NOLOCK)
						 INNER JOIN ClientCriticalDate A WITH (NOLOCK) ON M.ClientCriticalDateId=A.ClientCriticalDateId --WHERE M.CurrentDate IS NOT NULL
				) P  
				PIVOT  
					(  
						MAX (P.CurrentDate)  
						FOR P.Name IN  
						([Ex-Works],
						[GoodsBookedinLigentixbysupplier],
						[RequiredHandover],
						[ETD],
						[ETA],
						[CFSGATED],
						[ATPInspection],
						[S/OconfirmedbyLigentiaoriginoffice],
						[BookingWindowClosedDate],
						[CargoReadyDate],
						[PhysicalHandoverOfTheGoods],
						[ActualDeliveryDate],
						[Sailingdateorigin],
						[CFS],
						[PObooked],
						[Customscleared],
						[MerchSAApproval],
						[NOA],
						[SFD],
						[Approved],
						[CYcontainergatedinatdepartureport],
						[BOOKED],
						[GoodsBookedIntoLigentix],
						[HANDOVER],
						[BKCLOSE],
						[PO booked],
						[CCD],
						[CY],
						[SOCFRM],
						[DueDeliveryDate],
						[Customs cleared],
						[MCHSAAPVL],
						[SubmitDateofBooking],
						[SOconfirmedbyLigentiaoriginoffice],
						[RequiredDeliveryDate],
						[CustomsClearanceDate],
						[RQDDEL],
						[SFDCustomsClearanceDate],
						[ProductionDate],
						[POapproved],
						[UK Customs Clearance],
						[Latest Booking],
						[Origin Port Cut Off / Cargo Hand Over],
						[Departure],
						[Arrived Date / Booked for Delivery],
						[Arrival],
						[Arrivaldatedestination],
						[CFSCargoLoaded(CF)],
						[CFS Arrival],
						[ReceivedToConsolCentre],
						[CFSLOADED],
						[CFSReceiving],
						[ ],
						[Production Commenced],
						[Productionstartdate],
						[Preshipment Inspection passed],
						[VendorInspectionDate],
						[Inspectioncompletedpasseddate],
						[BookingWindowClosed],
						[Orderreadytoship],
						[Delivered],
						[Warehouse Delivery],
						[DCdeliverycompleted],
						[DC delivery completed],
						[Customs Clearance Date],
						[SFD Customs Clearance Date],
						[SA approved],
						[SAAVL],
						[PO Acknowledged to Imports],
						[PA],
						[BKOPEN],
						[BookingWindowOpen],
						[GOLDSEAL],
						[ILCSAAPVL],
						[CNTRRLS],
						[ContainerReleased(CR)],
						[DC booking made],
						[DCbookingmade],
						[DC booking confirmed],
						[DCbookingconfirmed],
						[Inspectionapprovaldate],
						[LibraInspectionDate],
						[Cargo Shipped],
						[Gold Seal Approved / Pre Shipment Booked]
						)
					) AS pvt
				)CUR ON M.OrderNumber=CUR.OrderNumber AND M.LigClientId=CUR.ClientId AND ISNULL(M.LOT,'1')=ISNULL(CUR.LOT,'1') --AND (M.LOT=CUR.LOT OR CUR.LOT is null)
			ORDER BY M.LigClientId, M.OrderNumber, M.LOT