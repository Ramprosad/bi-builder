
USE [BI_Builder]
GO



IF NOT EXISTS (SELECT 1 FROM Sys.objects WHERE name ='BIPOToBookingFact_New')
BEGIN
	 CREATE TABLE BIPOToBookingFact_New
	 (
			Customer					VARCHAR(100) NOT NULL, 
			PONumber					VARCHAR(100) NOT NULL,
			lot							VARCHAR(100) NOT NULL,
			BookingNumber				VARCHAR(100) NOT NULL,
			OrderQty					FLOAT,
			OrderCBM					FLOAT, 
			TotalBookedCBM				FLOAT, 
			TotalBookedQTY				FLOAT, 
			CONSTRAINT PK_BIPOToBookingFact_Customer_PONumber_lot_BookingNumber_New PRIMARY KEY (Customer, PONumber, lot, BookingNumber)
	 )
 END 
---------------------------------------------------------
--EXTRACT FOR PO FACT 
---------------------------------------------------------

	DECLARE @ClientId INT =17799
	DECLARE @JLPClient INT =2
	INSERT INTO BIPOToBookingFact_New(Customer, PONumber, lot, BookingNumber, OrderQty, OrderCBM, TotalBookedCBM,TotalBookedQTY)

		SELECT 
				LC.ConsigneeEDICode								AS Customer,
				M.OrderNumber,
				A.LOT, 
				ISNULL(BOOKING.Bk_No, 'N/A')					AS BookingNumber, 
				SUM(ISNULL(A.OrderQty, 0))						AS OrderQty,
				SUM(ISNULL(
					ISNULL(
						CASE C.LigClientId
							WHEN @ClientId THEN A.UnitVolume
							ELSE ISNULL(A.UnitVolume, 1) * A.OrderQty
						END
					,M.TotalVolume)
				, 0))											AS UnitVolume, 
				SUM(ISNULL(BOOKING.CBM, 0))						AS TotalBookedCBM,
				SUM(ISNULL(BOOKING.INPKG, 0))					AS TotalBookedQuantity	
		FROM 
			Ligentix_Purchase_Orders.dbo.OrderHeader M WITH(NOLOCK)
			JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH(NOLOCK) ON M.HeaderID=A.HeaderID AND M.IsLatestVersion = 1 
			JOIN Ligentix_Purchase_Orders.dbo.ClientFeed CF WITH(NOLOCK) ON M.OrderFeedId=CF.FeedId
			JOIN Ligentix_Purchase_Orders.dbo.Client C WITH(NOLOCK) ON cf.ClientID = C.ClientID
			JOIN Ligentix4.dbo.Client LC WITH(NOLOCK) ON C.LigClientId=LC.ClientId
			JOIN [BI_Builder].dbo.BIClient BIC WITH(NOLOCK) ON LC.ConsigneeEDICode=BIC.[ConsigneeEDICode]
			INNER JOIN 
			(
				SELECT DISTINCT OrderNumber,ClientId,Lot FROM [BI_Builder].dbo.ShipOrder WITH(NOLOCK) --List of shipped order for last n number of days. 
			)SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=C.LigClientId 
			AND (CASE SH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(SH.Lot,'1') END) = (CASE A.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)	
			JOIN [dbo].[BIPurchaseOrder] POFACT ON LC.ConsigneeEDICode=POFACT.Customer AND M.OrderNumber=POFACT.[PONumber] AND (CASE A.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)=POFACT.LOT
			LEFT JOIN 
			(
				SELECT BI.OrderLineId, BH.BK_NO, BH.Clientid, BI.CBM, BI.INPKG
				FROM [Ligentix4].dbo.BookingItem BI 
				JOIN [Ligentix4].dbo.BookingHeader BH ON BI.BookingId=BH.BookingId
				JOIN Ligentix4.dbo.Client C ON BH.ClientId=C.Clientid 
				JOIN [dbo].[BIBookingFact] BF ON  C.ConsigneeEDICode=BF.Customer AND BH.Bk_No=BF.[BookingNumber]

			)BOOKING ON  A.LineId=BOOKING.OrderLineId
			left JOIN Ligentix_Purchase_Orders.dbo.Client SUP WITH(NOLOCK) ON M.SupplierClientId=SUP.ClientId
			left join Ligentix4.dbo.Client SC WITH(NOLOCK) ON SUP.LigClientId=SC.ClientId
			left JOIN BI_Builder.dbo.BISupplier BSUP WITH(NOLOCK) ON SC.ConsigneeEDICode=BSUP.SupplierCode
		WHERE
			--BH.Bk_No='080319042' AND 
			M.IsLatestVersion = 1
			AND M.IsCancelled=0
			AND A.IsCancelled=0
			AND ((LC.ClientId=@JLPClient AND  M.OrderDate IS NOT NULL) OR LC.ClientId!=@JLPClient)
			AND ISNULL(NULLIF(A.[LineStatus], ''), '')!='CANCELLED'
			AND ((LC.ConsigneeEDICode	 IN ('SOAKCOGBNTN', 'WAITROGBLON') AND  ISNULL(A.UnitVolume, 0) != 0)
			OR LC.ConsigneeEDICode	 NOT IN('SOAKCOGBNTN', 'WAITROGBLON'))
		GROUP BY 
			LC.ConsigneeEDICode,
			M.OrderNumber,
			A.LOT, 
			BOOKING.Bk_No			
		ORDER BY 
			LC.ConsigneeEDICode,
			M.OrderNumber,
			A.LOT



		--SELECT count(1) FROM BIPOToBookingFact where BookingNumber='020118110'
		--ORDER BY BookingNumber
		--SELECT * from [dbo].[BIPurchaseOrder] WHERE PONumber='836/823486'
		--select top 10 * from BIPurchaseOrder

		--SELECT TOP 100 * FROM [Ligentix4].dbo.BookingItem BI 

		--SELECT COUNT(1) FROM #TEMP_PO_BOOKING --WHERE BookingNumber IS  NULL


		--SELECT COUNT(1) from [dbo].[BIPurchaseOrder]

		--SELECT count(1) FROM BIPOToBookingFact_New

		--DROP TABLE BIPOToBookingFact
		--EXEC SP_RENAME 'BIPOToBookingFact_New', 'BIPOToBookingFact'


	SELECT * FROM [dbo].[BIPurchaseOrder] where OrderQty < 0

	SELECT * FROM [Ligentix_Purchase_Orders].DBO.OrderHeader Where OrderNumber  IN ('600/149034')

	SELECT Sum(orderqty), lot FROM [Ligentix_Purchase_Orders].DBO.OrderLine WHERE HeaderId in ( 2025811)
	group by lot
