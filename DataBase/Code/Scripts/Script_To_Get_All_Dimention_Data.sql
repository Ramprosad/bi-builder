

--SELECT  * 
--FROM [CW_ONE].[cw1test1db].[dbo].OrgHeader OH
--WHERE OH_IsValid=1 AND OH_IsActive=1
--AND OH_IsForwarder=1 AND OH_IsShippingProvider=1

--SELECT * FROM Client WHERE ClientName LIKE 'Soak%'
--SELECT * FROM Client WHERE ParentClientId=2

--select * from 
--SELECT OH_IsConsignee, * FROM [CW_ONE].[cw1test1db].[dbo].OrgHeader OH
--WHERE OH_Code IN ('SMYSPZPLWAW', 'JOHLEWGBLON', 'JAYBROAUSYD', 'SOAKCOGBNTN')

--SELECT OH_IsConsignee,  * FROM [CW_ONE].[cw1test1db].[dbo].OrgHeader OH
--WHERE OH_Code IN ('EUROSHCNCAN', 'PLATOYHKHKG', 'AABDESINICD', 'ACHAMIINAGR')

--SELECT * FROM [CW_ONE].[cw1test1db].[dbo].OrgHeader OH
--WHERE OH_Code IN ('AABDESINICD', 'ACHAMIINAGR')

--select * from [CW_ONE].[cw1test1db].[dbo].[OrgCountryData]
select * from BIDataModel update BIDataModel set ModelSeqNo=10 where ModelId=18
CREATE TABLE BIDataModel
(

	ModelId				INT PRIMARY KEY IDENTITY(1,1),
	ModelType			VARCHAR(100) not null,
	ModelSeqNo			INT,
	ModelName			VARCHAR(100) not null,
	ModelFileName		VARCHAR(500) not null,
	ModelDb				VARCHAR(100) not null,
	ModelQuery			VARCHAR(MAX) not null,
	IsActive			VARCHAR(100) not null,
	CreatedBy			VARCHAR(100) not null,
	CreatedOn			DATETIME not null, 
	ModifiedBY			VARCHAR(100),
	ModifiedOn			DATETIME 
)

INSERT INTO BIDataModel (ModelType, ModelSeqNo, ModelName, ModelFileName, ModelDb, ModelQuery, IsActive, CreatedBy,CreatedOn) 
VALUES('Dimention', 1, 'Country', 'Country.csv', 'Ligentix_Purchase_Orders', 'SELECT Alpha2Code AS CountryCode, Name AS CountryName FROM luCountry', 1, 'System', getdate())

INSERT INTO BIDataModel (ModelType, ModelSeqNo, ModelName, ModelFileName, ModelDb, ModelQuery, IsActive, CreatedBy,CreatedOn) 
VALUES('Dimention', 2, 'Port', 'Port.csv', 'Ligentix4', 'SELECT PortCode, PortName, left(PortCode, 2) AS CountryCode FROM Ports', 1, 'System', getdate())

INSERT INTO [dbo].[BIDataModel]
(
	ModelType, 
	ModelSeqNo, 
	ModelName, 
	ModelFileName, 
	ModelDb, 
	ModelQuery, 
	IsActive, 
	CreatedBy, 
	CreatedOn
)
SELECT 
	'Dimention'							AS ModelType, 
	9									AS ModelSeqNo, 
	'BICarrier'							AS ModelName,
	'Carrier.csv'						AS ModelFileName, 
	'BI_Builder'						AS ModelDb,
	'SELECT * FROM BICarrier'			AS ModelQuery,
	1									AS IsActive,
	'System'							AS CreatedBy, 
	GETDATE()							AS CreatedOn	


INSERT INTO [dbo].[BIDataModel]
(
	ModelType, 
	ModelSeqNo, 
	ModelName, 
	ModelFileName, 
	ModelDb, 
	ModelQuery, 
	IsActive, 
	CreatedBy, 
	CreatedOn
)
SELECT 
	'Dimention'							AS ModelType, 
	10									AS ModelSeqNo, 
	'BIDate'							AS ModelName,
	'DateValue.csv'						AS ModelFileName, 
	'BI_Builder'						AS ModelDb,
	'SELECT * FROM BIDate'				AS ModelQuery,
	1									AS IsActive,
	'System'							AS CreatedBy, 
	GETDATE()							AS CreatedOn	

INSERT INTO [dbo].[BIDataModel]
(
	ModelType, 
	ModelSeqNo, 
	ModelName, 
	ModelFileName, 
	ModelDb, 
	ModelQuery, 
	IsActive, 
	CreatedBy, 
	CreatedOn
)
SELECT 
	'Dimention'							AS ModelType, 
	11									AS ModelSeqNo, 
	'BICity'							AS ModelName,
	'City.csv'							AS ModelFileName, 
	'BI_Builder'						AS ModelDb,
	'SELECT * FROM BICity'				AS ModelQuery,
	1									AS IsActive,
	'System'							AS CreatedBy, 
	GETDATE()							AS CreatedOn	

INSERT INTO [dbo].[BIDataModel]
(
	ModelType, 
	ModelSeqNo, 
	ModelName, 
	ModelFileName, 
	ModelDb, 
	ModelQuery, 
	IsActive, 
	CreatedBy, 
	CreatedOn
)
SELECT 
	'Dimention'							AS ModelType, 
	11									AS ModelSeqNo, 
	'BIContainerSize'					AS ModelName,
	'ContainerSize.csv'					AS ModelFileName, 
	'BI_Builder'						AS ModelDb,
	'SELECT * FROM BIContainerSize'		AS ModelQuery,
	1									AS IsActive,
	'System'							AS CreatedBy, 
	GETDATE()							AS CreatedOn	

INSERT INTO [dbo].[BIDataModel]
(
	ModelType, 
	ModelSeqNo, 
	ModelName, 
	ModelFileName, 
	ModelDb, 
	ModelQuery, 
	IsActive, 
	CreatedBy, 
	CreatedOn
)
SELECT 
	'Dimention'											AS ModelType, 
	12													AS ModelSeqNo, 
	'BIClientGroup'										AS ModelName,
	'ClientGroup.csv'									AS ModelFileName, 
	'BI_Builder'										AS ModelDb,
	'SELECT ClientCWCode AS ClientCode, ClientName, 
	GroupName FROM BIClientGroup'						AS ModelQuery,
	1													AS IsActive,
	'System'											AS CreatedBy, 
	GETDATE()											AS CreatedOn	

INSERT INTO [dbo].[BIDataModel]
(
	ModelType, 
	ModelSeqNo, 
	ModelName, 
	ModelFileName, 
	ModelDb, 
	ModelQuery, 
	IsActive, 
	CreatedBy, 
	CreatedOn
)
SELECT 
	'Dimention'											AS ModelType, 
	 13													AS ModelSeqNo, 
	'BIContainerSize'									AS ModelName,
	'ContainerSize.csv'									AS ModelFileName, 
	'BI_Builder'										AS ModelDb,
	'SELECT * FROM [dbo].[BIContainerSize]'				AS ModelQuery,
	1													AS IsActive,
	'System'											AS CreatedBy, 
	GETDATE()											AS CreatedOn	

--========================================================================
INSERT INTO [dbo].[BIDataModel]
(
	ModelType, 
	ModelSeqNo, 
	ModelName, 
	ModelFileName, 
	ModelDb, 
	ModelQuery, 
	IsActive, 
	CreatedBy, 
	CreatedOn
)
SELECT 
	'Dimention'											AS ModelType, 
	 15													AS ModelSeqNo, 
	'BICLientSupplier'									AS ModelName,
	'ClientSupplier.csv'								AS ModelFileName, 
	'BI_Builder'										AS ModelDb,
	'SELECT * FROM [dbo].[BICLientSupplier]'		AS ModelQuery,
	1													AS IsActive,
	'System'											AS CreatedBy, 
	GETDATE()
--===============================================================================
	INSERT INTO [dbo].[BIDataModel]
	(
		ModelType, 
		ModelSeqNo, 
		ModelName, 
		ModelFileName, 
		ModelDb, 
		ModelQuery, 
		IsActive, 
		CreatedBy, 
		CreatedOn
	)
	SELECT 
		'Dimention'											AS ModelType, 
		 16													AS ModelSeqNo, 
		'BIPurchaseOrder'									AS ModelName,
		'BIPurchaseOrder.csv'								AS ModelFileName, 
		'BI_Builder'										AS ModelDb,
		'SELECT * FROM [dbo].[BIPurchaseOrder]'				AS ModelQuery,
		1													AS IsActive,
		'System'											AS CreatedBy, 
		GETDATE()

	INSERT INTO [dbo].[BIDataModel]
	(
		ModelType, 
		ModelSeqNo, 
		ModelName, 
		ModelFileName, 
		ModelDb, 
		ModelQuery, 
		IsActive, 
		CreatedBy, 
		CreatedOn
	)
	SELECT 
		'Dimention'											AS ModelType, 
		 17													AS ModelSeqNo, 
		'BIMilestoneFact'									AS ModelName,
		'MilestoneFact.csv'									AS ModelFileName, 
		'BI_Builder'										AS ModelDb,
		'SELECT * FROM [dbo].[BIMilestoneFact]'				AS ModelQuery,
		1													AS IsActive,
		'System'											AS CreatedBy, 
		GETDATE()
---------------------------------------------------------
--ClientGroup
---------------------------------------------------------
--TRUNCATE TABLE BIClientGroup
INSERT INTO BIClientGroup(ClientCWCode, ClientName, GroupName)
SELECT 
	ISNULL([CW CODE], OH.OH_Code), 
	ISNULL([Consignee Name CW], OH.OH_FullName), 
	ISNULL([Group], OH.OH_FullName)
FROM 
	CustomerGrouping CG LEFT JOIN [CW_ONE].[cw1test1db].[dbo].OrgHeader OH
	ON CG.[CW CODE] COLLATE SQL_Latin1_General_CP1_CI_AS =OH.OH_Code COLLATE SQL_Latin1_General_CP1_CI_AS 
--WHERE 
--	OH.OH_IsConsignee=1 
--	AND OH.OH_IsActive=1


--Note Duplicate entries noticed 
SELECT * FROM [dbo].[CustomerGrouping]

--SELECT COUNT(1) FROM  [CW_ONE].[cw1test1db].[dbo].OrgHeader OH WHERE OH.OH_IsConsignee=1 AND OH.OH_IsActive=1

---------------------------------------------------------
--1. Clients
--------------------------------------------------------
SELECT * FROM BIClient
INSERT INTO BIClient(ConsigneeEDICode, ClientName, ClientGroupName)
SELECT DISTINCT
	OH_CODE			AS ConsigneeEDICode, 
	OH_FullName		AS ClientName,
	CG.GroupName	AS GroupName
FROM 
	[CW_ONE].[cw1test1db].[dbo].OrgHeader OH
	JOIN BIClientGroup CG ON OH.OH_CODE COLLATE SQL_Latin1_General_CP1_CI_AS = CG.ClientCWCode COLLATE SQL_Latin1_General_CP1_CI_AS
--WHERE 
--	OH_IsActive=1 
--	AND OH_IsConsignee=1

---------------------------------------------------------
--2. Country 
---------------------------------------------------------

INSERT INTO BICountry(CountryCode, CountryName)
SELECT CountryCode, CountryName
FROM
(
	SELECT *, 
	Row_Number() OVER(PARTITION BY CountryCode ORDER BY CountryCode) AS SlNo
	FROM
	(
		SELECT DISTINCT
			LEFT(CASE PortCode WHEN '_NXGG' THEN 'CNXGG' ELSE PortCode END, 2)	AS CountryCode, 
			Country				AS CountryName 
		FROM 
			Ligentix4.dbo.Ports
	)X
)Y
WHERE 
	Y.SlNo=1


UPDATE BICountry SET CountryName='Ghana' WHERE CountryCode='GH' 

-----------------------------------------------------------------------
--City 
-----------------------------------------------------------------------


INSERT INTO BICity(CityCode, CityName, CountryCode)
VALUES ('N/A', 'N/A', 'N/A')

INSERT INTO BICity(CityCode, CityName, CountryCode)
SELECT
	 POL, CITY, CountryCode
FROM 
(
	SELECT 
		POL, 
		CITY, 
		CountryCode, 
		ROW_NUMBER() OVER (PARTITION BY POL ORDER BY POL) AS SlNo
	FROM 
		City 
)C
WHERE
	C.SlNo=1

-------------------------------------------------------
--3. Port
-------------------------------------------------------
--SELECT * FROM BIPort

INSERT INTO BIPort(PortCode, CountryCode, PortName, CityCode,CityName, CountryName)
VALUES ('N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A') --Dummy

INSERT INTO BIPort(PortCode, CountryCode, PortName, CityCode,CityName, CountryName)
SELECT 
	PortCode, 
	CountryCode,
	PortName, 
	ISNULL(CityCode, 'N/A'), 
	ISNULL(CityName, 'N/A'), 
	ISNULL(NULLIF(CountryName, ''), 'N/A')
FROM 
(
	SELECT 
		X.PortCode,
		x.CountryCode,
		x.PortName,
		C.Pol					AS CityCode, 
		C.City					AS CityName, 
		X.Country				AS CountryName, 
		Row_Number() OVER(PARTITION BY x.PortCode ORDER BY  x.PortCode)  AS SlNo
	FROM 
		(
			SELECT DISTINCT
				CASE PortCode WHEN '_NXGG' THEN 'CNXGG' ELSE PortCode END	AS PortCode, 
				LTRIM(RTRIM(PortName))										AS PortName,
				LEFT(CASE PortCode WHEN '_NXGG' THEN 'CNXGG' ELSE PortCode END, 2)	AS CountryCode, 
				LTRIM(RTRIM(Country))										AS Country
			FROM
				Ligentix4.dbo.Ports
		)X
		JOIN BICountry CON ON X.CountryCode=CON.CountryCode
		LEFT JOIN City C ON X.PortCode=C.POL
)Y
WHERE 
	Y.SlNo=1

-------------------------------------------------------
--4. BITransport
-------------------------------------------------------

INSERT INTO BITransport(TransPortCode, TransportName)
VALUES ('N/A', 'Not Available'),
('Sea', 'Sea'),
('Air-C', 'Air-C'), 
('Air-P', 'Air-P'), 
('Road', 'Road'),
('Rail', 'Rail'), 
('Air', 'Air')



------------------------------------------------------
--5. DeliveryMode 
------------------------------------------------------

INSERT INTO BIDeliveryMode(DeliveryModeCode,DeliveryModeName)
VALUES
	('N/A', 'Not Available'),
	('CY', 'Container Yard'),
	('CFS', 'Container Freight Station'),
	('LCL', 'Less Container Load'), 
	('GRP', 'GRP'),
	('BCN', 'BCN'),
	('CFS/CFS', 'CFS/CFS'),
	('CY/CFS', 'CY/CFS'),
	('CFS/CY', 'CFS/CY'),
	('CY/CY', 'CY/CY'),
	('CY/SD','CY/SD')

------------------------------------------------------
--6. BIHOB 
------------------------------------------------------
TRUNCATE TABLE BIHOB
INSERT INTO BIHOB(HOBName, ClientConsigneeEDICode)
VALUES('N/A', 'N/A')

INSERT INTO BIHOB(HOBName, ClientConsigneeEDICode)
SELECT 
	Name, 
	BB.ConsigneeEDICode
FROM 
	Ligentix4.dbo.BuyingGroup BG JOIN Ligentix4.dbo.Client CL ON BG.ClientId=CL.ClientId
	JOIN BI_Builder.dbo.BIClient BB ON CL.ConsigneeEDICode=BB.ConsigneeEDICode
	


---------------------------------------------------------
--7. Suplier 
--------------------------------------------------------
--SELECT TOP 10 * FROM Ligentix4.dbo.client  WHERE ClientName like 'John lew%' ConsigneeEDICode='360DEGHKHKG'

SELECT * FROM BISupplier
INSERT INTO BISupplier(SupplierCode, SupplierName)
VALUES('N/A', 'N/A')

INSERT INTO BISupplier(SupplierCode, SupplierName)
SELECT 
Z.SupplierCode, 
Z.SupplierName
FROM 
(
	SELECT
		SUP.SupplierCWCode								AS SupplierCode,
		SUP.SupplierName								AS SupplierName,
		ROW_NUMBER() OVER(PARTITION BY SUP.SupplierCWCode, SUP.SupplierName ORDER BY SUP.SupplierCWCode, SUP.SupplierName) AS SlNo
	FROM
		(
			SELECT 
				OH.OH_CODE								AS SupplierCWCode,
				OH.OH_FullName							AS SupplierName	
			FROM 
				[CW_ONE].[cw1test1db].[dbo].OrgHeader OH  
				LEFT JOIN Ligentix4.dbo.Client CL ON OH.OH_Code COLLATE SQL_Latin1_General_CP1_CI_AS =ConsigneeEDICode COLLATE SQL_Latin1_General_CP1_CI_AS
			WHERE 
				OH.OH_IsActive=1 
				AND OH.OH_IsConsignor=1
				AND CL.IsCurrent=1
	   )SUP
)Z
WHERE 
	Z.SlNo=1

--SELECT *  FROM BISupplier order by ClientConsigneeEDICode
---------------------------------------------------------------
--Product List 
----------------------------------------------------------------

DECLARE @UserDessesionTmp TABLE 
(
	GroupId INT NOT NULL,
	GroupName VARCHAR(50) NULL,
	ShipId INT NOT NULL,
	ShipName VARCHAR(50) NULL,
	Diss VARCHAR(6) NULL,
	ClientID INT
);

INSERT INTO @UserDessesionTmp
SELECT DISTINCT A.Id GroupId,A.Name GroupName,B.Id ShipId,B.Name ShipName,C.diss Diss,A.ClientID
FROM dbo.BuyingGroup A WITH (NOLOCK)
	INNER JOIN dbo.Buyership B WITH (NOLOCK) ON A.Id = B.BuyinggroupId
	INNER JOIN dbo.SubBuyer C WITH (NOLOCK) ON B.Id = C.BuyershipID
	INNER JOIN dbo.UserDissection D WITH (NOLOCK) ON D.DissectionID = C.Id

SELECT DISTINCT
	LC.ConsigneeEDICode AS CWEDICode,
	ol.ItemCode, 
	ol.Description,
ISNULL(ISNULL(D.Description, BI.Department), 'N/A') AS Department,
(
					SELECT TOP (1) T.GroupName FROM @UserDessesionTmp T 
					WHERE T.Diss = SUBSTRING(bi.OrderNumber, 1, 3) AND T.ClientID=bh.ClientID
) HOB
INTO BI_Builder.dbo.BIProduct
FROM 
	[Ligentix_Purchase_Orders].dbo.OrderLine OL
	JOIN [Ligentix_Purchase_Orders].dbo.OrderHeader OH ON OL.HeaderID = OH.HeaderID
	JOIN Ligentix4.dbo.BookingItem BI ON OL.LineId=BI.OrderLineId
	JOIN Ligentix4.dbo.BookingHeader BH ON BI.BookingId=BH.BookingId
	JOIN Ligentix4.dbo.Client LC ON BH.ClientID=LC.ClientID
	left join Ligentix4.dbo.Department D ON BI.DepartmentId=D.DepartmentId AND  BH.ClientId=D.ClientId
--WHERE 
--	BH.ClientId=17098

---------------------------------------------------------------------
--BIDate
---------------------------------------------------------------------

DECLARE @StartDate	DATE='2017-01-01'
DECLARE @EndDate	DATE='2100-12-31'
WHILE @StartDate <=@EndDate
BEGIN 
	
	INSERT INTO BI_Builder.dbo.BIDate (DateValue, [Year], [Month], [Day])
	VALUES (@StartDate, YEAR(@StartDate), MONTH(@StartDate), DAY(@StartDate))
		
	SET @StartDate=DATEADD(DD, 1, @StartDate)
	
END 

----------------------------------------------------------------------
--Carrier 
----------------------------------------------------------------------
TRUNCATE TABLE BICarrier

INSERT INTO BICarrier (CarrierCode, CarrierName)
VALUES('N/A', 'N/A')

INSERT INTO BICarrier (CarrierCode, CarrierName)
SELECT 
	OH.OH_Code, 
	ISNULL(OH.OH_FullName, 'N/A')
FROM
	[CW_ONE].[cw1test1db].dbo.OrgHeader OH
WHERE 
	OH.OH_IsShippingProvider=1
	AND OH.OH_IsActive=1
	AND OH_Code IN 
	(
		'AIRCNXMN',
		'AIRFRAGBHOU',
		'ALLNIPJPTYO',
		'BIMAIRBDDAC',
		'BORLINGBLIV',
		'BRIAIRGBHOU',
		'BRONELGBBAS',
		'BRULOGGBRDD',
		'BTGSPEATLNZ',
		'CARMARGBTAW',
		'CHALINHKHKG',
		'CMACGMFRMRS',
		'CMACGM_WW',
		'COLSHI_WW',
		'COSCO_WW',
		'DAVTURGBDFD',
		'DAVTURGBCLL',
		'DAVTURGBMNC',
		'DHLGBMNC',
		'ECULINGBEAT',
		'EMIAIRGBHOU',
		'EMISEAHKHKG',
		'ETIHADPKLHE',
		'GLOFALGBRMF',
		'GLOMARSGSIN',
		'HONDRAHKHKG',
		'HONAIRHKHKG',
		'INDCON_WW',
		'JETAIRINBOM',
		'JETAIRGBLON',
		'KLMROYNLAMS',
		'LUCFREHKHKG',
		'MALAIRMYSZB',
		'MARLINGBBEY',
		'MCCTRAHKHKG',
		'NAITRAJPYOK',
		'OMAAIRINMAA',
		'ORILOGCNSZX',
		'PANLOGCNSHA2',
		'PFEEXPGBWIH',
		'QATAIRCNSHA',
		'SAFMAR_WW',
		'SAUARASAJED',
		'SHITRAHKHKG',
		'SHITRAGBLBK',
		'SHITRAPLGDY',
		'SINAIRSGSIN',
		'THAAIRTHBKK',
		'TSLINEVNSGN',
		'TSLINE_WW',
		'TURAIRTRIST',
		'UNSGLOGBLON',
		'VANLOGGBMAL',
		'VANLOGPLGDY',
		'VIRATLGBCWY',
		'VVMJSCVNSGN',
		'WANHAI_WW',
		'XIAAIRCNXMN',
		'ZIM_WW',
		'HAPLLODEHAM',
		'MEDSHI_WW',
		'YANMIN_WW',
		'AIRCANGBLON',
		'MAERSK',
		'OCENETGBSOU',
		'APL_WW',
		'HAMSUD_WW',
		'OOCL_WW',
		'HYUMER_WW',
		'EVERGR_WW',
		'LUFCARDEKEB',
		'CHISOUCNXSA',
		'MOLBRACNCAN',
		'UNIARAKWSAF',
		'NYK_WW',
		'MPCONSVNSGN', 
		'HAPLLO_AU',
		'HAPLLO_WW',
		'HAPLLOCNGZP',
		'HAPLLOCNSZX',
		'HAPLLODEHAM',
		'HAPLLOZADUR1',
		'MEDSHI_AU',
		'MEDSHI_WW',
		'MEDSHICNSZX',
		'MEDSHIHKHKG',
		'YANMIN_WW',
		'YANMINAUBNE',
		'YANMINCNSHA',
		'YANMINHKHKG',
		'YANMINVNHPH',
		'AIRCANGBLON',
		'MAECOLCNTAO',
		'MAELINBNE',
		'MAELINCNCAN',
		'MAELINCNTSN',
		'MAELINDKCPH',
		'MAEPOLPLWAW',
		'MAERSK',
		'MAERSK_WW',
		'MAERSKHKHKG',
		'MAESHICNNGB',
		'MAESHICNXMN',
		'MAEVIEVNSGN',
		'OCENETGBSOU',
		'APL_WW',
		'APLDEHAM',
		'APLLINSYD',
		'APLNETNLRTM',
		'APLNOLVNSGN',
		'APLSPZPLGDY',
		'HAMSUD_WW',
		'HAMSUD_AU',
		'HAMSUDCNCAN',
		'OOCAUS_AU',
		'OOCBRACNCAN',
		'OOCBRACNXMN',
		'OOCCHICNTAO',
		'OOCCOLCNNGB',
		'OOCL_WW',
		'OOCVIEVNSGN',
		'HYUCORGBTEF',
		'HYUCORGBTEF1',
		'HYUMER_WW',
		'HYUMERCNSZX',
		'HYUMERHKHKG',
		'HYUMERMEL',
		'HYUMERVNSGN',
		'EVELINCNCAN',
		'EVEMARHKHKG1',
		'EVERGR_WW',
		'EVEROEGBLON',
		'EVESHIAUBNE',
		'EVESHIPLWAW',
		'LUFCARDEKEB',
		'LUFGERSYD',
		'CHISOUCNXSA',
		'MOLBRACNCAN',
		'MOLBRACNSZX',
		'MOLBRACNSMN',
		'UNIARA_WW',
		'UNIARAAU',
		'UNIARAAUADL',
		'UNIARAKWSAF',
		'NYK_WW',
		'NYKLINAUSYD',
		'ECULINAEDXB',
		'ECULINCNSZX',
		'ECULINCNXMN',
		'ECULINDEHAM',
		'ECULINFRMRS',
		'ECULINGBEAT',
		'ECULINPLGDY',
		'MPCONSVNDAD',
		'MPCONSVNSGN',
		'COSCO_WW',
		'COSCONHKHKG',
		'COSCONVNSGN',
		'COSINLCNTSN',
		'COSINTCNCAN',
		'COSINTCNXMN',
		'COSSHIAUSYD',
		'COSSHICNCTU',
		'COSSHICNNTG',
		'COSSHICNZSN',
		'COSSPZPLGDY'
)

-------------------------------------------------------------
--Container Size
-------------------------------------------------------------
INSERT INTO BIContainerSize (SizeCode, SizeDescription, Volume)
VALUES('N/A', 'N/A', 'N/A')

INSERT INTO BIContainerSize (SizeCode, SizeDescription, Volume)
SELECT
	RC_CODE, RC_CODE, RC_TEU
FROM 
	[CW_ONE].[cw1test1db].dbo.RefContainer

-------------------------------------------------------------
--BIMileStone
-------------------------------------------------------------
--TRUNCATE TABLE BIMileStone
--SELECT * FROM BIMileStone WHERE MileStoneKey ='_ACT')

INSERT INTO BIMileStone (ClientCWEDICode, MileStoneKey, IsStandard)
SELECT 
	C.ConsigneeEDICode			AS ClientCWEDICode, 
	ISNULL(NULLIF(CCD.[KEY], ''), 'CFSArrival')+'_CUR'			AS MileStoneKey,
	0							AS IsStandard
FROM 
	Ligentix4.dbo.ClientCriticalDate  CCD 
	JOIN Ligentix4.dbo.Client C ON CCD.ClientId=C.ClientId
	JOIN BI_Builder.dbo.BIClient BC ON C.ConsigneeEDICode = BC.ConsigneeEDICode
UNION 
	SELECT 
	C.ConsigneeEDICode			AS ClientCWEDICode, 
	ISNULL(NULLIF(CCD.[KEY], ''), 'CFSArrival')+'_EST'			AS MileStoneKey,
	0							AS IsStandard
FROM 
	Ligentix4.dbo.ClientCriticalDate  CCD 
	JOIN Ligentix4.dbo.Client C ON CCD.ClientId=C.ClientId
	JOIN BI_Builder.dbo.BIClient BC ON C.ConsigneeEDICode = BC.ConsigneeEDICode
UNION
SELECT 
	C.ConsigneeEDICode			AS ClientCWEDICode, 
	ISNULL(NULLIF(CCD.[KEY], ''), 'CFSArrival')+'_ACT'			AS MileStoneKey,
	0							AS IsStandard
FROM 
	Ligentix4.dbo.ClientCriticalDate  CCD 
	JOIN Ligentix4.dbo.Client C ON CCD.ClientId=C.ClientId
	JOIN BI_Builder.dbo.BIClient BC ON C.ConsigneeEDICode = BC.ConsigneeEDICode


UPDATE BIMileStone
SET IsStandard=1
WHERE
	LEFT([MileStoneKey], LEN([MileStoneKey])-4) IN 
	(
		'BookingWindowClosed', 
		'BookingWindowOpen',
		'CFSCargoLoaded(CF)', 
		'ContainerReleased(CR)',
		'CYcontainergatedinatdepartureport', 
		'ETA',
		'ETD',
		'GoodsBookedinLigentixbysupplier', 
		'MerchSAApproval', 
		'ActualDeliveryDate', 
		'RequiredHandover', 
		'SFDCustomsClearanceDate',
		'S/OconfirmedbyLigentiaoriginoffice'

	)
	------------------------------------------------------------------------
	--Client Supplier 
	-----------------------------------------------------------------------
	
	INSERT INTO BICLientSupplier(ClientCode, SupplierCode)
	VALUES ('N/A', 'N/A')

	INSERT INTO BIClientSupplier(ClientCode, SupplierCode)
	SELECT DISTINCT
		ClientCode, 
		Z.SupplierCode
	FROM 
	(
		SELECT
			SupplierCWCode									AS SupplierCode, 
			ClientCode										AS ClientCode,
			ROW_NUMBER() OVER(PARTITION BY SupplierCWCode, ClientCode ORDER BY SupplierCWCode, ClientCode ) AS SlNo
		FROM
			(
				SELECT DISTINCT
					OH.OH_CODE							AS SupplierCWCode,
					ISNULL(PC.ConsigneeEDICode, 'N/A')	AS ClientCode
				FROM 
					[CW_ONE].[cw1test1db].[dbo].OrgHeader OH  
					LEFT JOIN Ligentix4.dbo.Client CL ON OH.OH_Code COLLATE SQL_Latin1_General_CP1_CI_AS=ConsigneeEDICode COLLATE SQL_Latin1_General_CP1_CI_AS
					LEFT JOIN Ligentix_Purchase_Orders.dbo.Supplier SUP ON CL.ConsigneeEDICode COLLATE SQL_Latin1_General_CP1_CI_AS = SUP.CWSupplierCode COLLATE SQL_Latin1_General_CP1_CI_AS
					LEFT JOIN Ligentix4.dbo.Client PC ON cl.ParentClientId=PC.ClientId
				WHERE 
					OH.OH_IsActive=1 
					AND OH.OH_IsConsignor=1
					AND CL.IsCurrent=1
					
			)SUP
			
	)Z
	JOIN BIClient BC ON Z.ClientCode COLLATE SQL_Latin1_General_CP1_CI_AS =BC.ConsigneeEDICode COLLATE SQL_Latin1_General_CP1_CI_AS
	JOIN BISupplier SUP2 ON Z.SupplierCode COLLATE SQL_Latin1_General_CP1_CI_AS =SUP2.SupplierCode COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE 
		Z.SlNo=1


--=====================================================================
--Chaloo Queries
--=====================================================================
/*
SELECT * FROM BI_Builder.dbo.BICLientSupplier
WHERE ClientCode='N/A' AND SupplierCode='CREDISHKHKG1'


SELECT * FROM Ligentix4.dbo.Client where ConsigneeEDICode='CREDISHKHKG1'
SELECT TOP 10  * FROM Ligentix4.dbo.BookingItem
SELECT * FROM BookingHeader  BH 

select * from Department 
select * from biCITY

SELECT * FROM BI_Builder.dbo.BIProduct

SELECT top 100 BusinessUnit, * FROM OrderHeader where isnull(BusinessUnit, '')<>''
and  OrderNumber='0100000120'

select Department, * from Ligentix4.dbo.BookingItem where OrderNumber='0100000120'

SELECT POL, COUNT(1) FROM CITY Group By POL having count(1) > 1
SELECT * FROM CITY WHERE POL IN ('CNWUH', 'MGTMM', 'MGTOA') ORDER  BY POL


select top 10 * from OrderHeader  

SELECT top 10 * FROM OrderLine 

Select * from Ligentix4.dbo.Client WHERE ClientName in 
( SELECT Distinct Carrier  FROM [BI_Builder].[dbo].[Shipment])

SELECT Distinct Carrier  FROM [BI_Builder].[dbo].[Shipment]

SELECT OH_IsShippingLine,OH_IsShippingProvider, OH_IsMiscFreightServices, * from  [CW_ONE].[cw1test1db].dbo.OrgHeader WHERE OH_Code collate Latin1_General_CI_AS
in
(
	Select ConSigneeEDICode collate Latin1_General_CI_AS from Ligentix4.dbo.Client 
	WHERE ClientName collate Latin1_General_CI_AS in (SELECT Distinct Carrier collate Latin1_General_CI_AS FROM [BI_Builder].[dbo].[Shipment])
)

	SELECT * FROM Ligentix4.dbo.Client WHERE ConSigneeEDICode='ORILOGCNSZX'
	SELECT TOP 10 * FROM Ligentix4.dbo.Consol WHERE CarrierId=29751

	SELECT * FROM [BI_Builder].[dbo].[Shipment]
	WHERE Carrier='ORIENTAL LOGISTICS GROUP LTD.(SHENZHEN)'

	SELECT * FROM [Ligentix4].[dbo].[Shipment] where BookingRef='180118143'
	select * from Ligentix4.dbo.Consol  where ConsolId=71504

	SELECT * FROM Ligentix4.dbo.Client WHERE ClientName LIKE 'ORIENTAL LOGISTICS GROUP LTD.(SHENZHEN)'
	ClientId=6198

	SELECT TOP 10 * FROM [Ligentix_Purchase_Orders].dbo.OrderHeader 

*/