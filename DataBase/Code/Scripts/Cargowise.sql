IF OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL
			BEGIN
				DROP TABLE #TEMP  
			END  
	
			CREATE TABLE #TEMP
			(
				OA_PK UNIQUEIDENTIFIER ,
				OA_OH UNIQUEIDENTIFIER,
				CONSTRAINT [PK_TEMP01] PRIMARY KEY CLUSTERED 
				(
					OA_PK ASC
				)
			)
			INSERT INTO #TEMP
			SELECT OA_PK, OA_OH FROM openquery([CW_ONE],'SELECT OA_PK, OA_OH FROM [cw1test1db].[dbo].OrgAddress')


			SELECT DISTINCT
				 ClientId, OrderNumber, LOT, OH_CODE
				,JW_RL_NKLoadPort,JC_DeliveryMode,JO_OuterPacks,JO_ActualVolume
				,JS_E_Arv,JS_E_DEP,JS_RL_NKDestination,JS_PackingMode,JA_RL_NKPortOfLoading,OH_FullName,RC_Code
				,REPLACE(RL_PortName, CHAR(10),' ') AS	RL_PortName
				,(
					SELECT TOP 1 P9.P9_ActualDate
					FROM [CW_ONE].[cw1test1db].[dbo].ProcessTasks P9
					WHERE P9.P9_Type = 'MIL' AND P9.P9_SE_NKMilestoneEvent = 'OBL' AND P9.P9_ParentID=M.JS_PK AND P9.P9_ActualDate IS NOT NULL
				 ) AS SurrenderDate
				,(
					SELECT TOP 1 P9.P9_ActualDate
					FROM [CW_ONE].[cw1test1db].[dbo].ProcessTasks P9
					WHERE P9.P9_Type = 'MIL' AND P9.P9_SE_NKMilestoneEvent = 'ATH' AND P9.P9_ParentID=M.JS_PK AND P9.P9_ActualDate IS NOT NULL
				 ) AS ATH_EventDate
				,(
					SELECT TOP 1 P9.P9_ActualDate
					FROM [CW_ONE].[cw1test1db].[dbo].ProcessTasks P9
					WHERE P9.P9_Type = 'MIL' AND P9.P9_SE_NKMilestoneEvent = 'JSS' AND P9.P9_ParentID=M.JS_PK AND P9.P9_ActualDate IS NOT NULL
				 ) AS JSS_EventDate
				,ShipmentID
				,ContainerNumber
			INTO [BI_Builder].[dbo].[CargoWise_Test]
			FROM
				(
					SELECT DISTINCT
						PO_OH.ClientId,
						PO_OH.OrderNumber,
						PO_OH.LOT,
						JW.JW_RL_NKLoadPort,
						JC.JC_DeliveryMode,
						SUM(JO.JO_OuterPacks) AS JO_OuterPacks,
						SUM(JO.JO_ActualVolume) AS JO_ActualVolume,
						JS.JS_E_Arv,
						JS.JS_E_DEP,
						JS_RL_NKDestination,
						JS.JS_PackingMode,
						JA.JA_RL_NKPortOfLoading,
						OH.OH_FullName,
						OH.OH_CODE,
						RC.RC_Code,
						REPLACE(REPLACE(UN.RL_PortName,CHAR(13),' '),CHAR(10),' ') AS RL_PortName, ---replace Enter Character
						JS.JS_PK,
						JS.JS_UniqueConsignRef AS ShipmentID,
						JC.JC_ContainerNum AS ContainerNumber
					FROM
						[CW_ONE].[cw1test1db].[dbo].JobOrderLine JO WITH(NOLOCK) 
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobOrderHeader JD WITH(NOLOCK)  On JD.JD_PK=JO.JO_JD
						INNER JOIN #TEMP OA WITH(NOLOCK)  On JD.JD_OA_BuyerAddress=OA.OA_PK
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].OrgHeader OH WITH(NOLOCK)  On OH.OH_PK=OA.OA_OH --JD_OH_BuyerAddress
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobShipment JS WITH(NOLOCK)  On JS.JS_PK=JD.JD_JS
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConShipLink JN WITH(NOLOCK)  On JN.JN_JS=JS.JS_PK
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsol JK WITH(NOLOCK)  On JK.JK_PK=JN.JN_JK		
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobContainer JC WITH(NOLOCK)  ON JC.JC_JK=	JK.JK_PK
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefContainer RC WITH(NOLOCK)  On JC.JC_RC=RC.RC_PK AND JC.JC_JK=JK.JK_PK AND JC.JC_ContainerNum=JO.JO_ContainerNumber
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobConsolTransport JW WITH(NOLOCK) ON JW.JW_TransportType = 'MAI' AND JW.JW_ParentGUID = JK.JK_PK 
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].RefUNLOCO UN WITH(NOLOCK)  ON UN.RL_Code = JS.JS_RL_NKOrigin
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobSailing JX WITH(NOLOCK)  On JX.JX_PK=JW.JW_JX
						INNER JOIN [CW_ONE].[cw1test1db].[dbo].JobVoyOrigin JA WITH(NOLOCK)  On JA.JA_PK=JX.JX_JA
						INNER JOIN 
						(
							SELECT DISTINCT M.OrderNumber, I.LigClientId AS ClientId, A.LOT, A.ItemCode, CL.ConsigneeEDICode
							FROM Ligentix_Purchase_Orders.dbo.OrderHeader M WITH(NOLOCK)
								 INNER JOIN Ligentix_Purchase_Orders.dbo.OrderLine A WITH(NOLOCK) ON M.HeaderID=A.HeaderID
								 INNER JOIN Ligentix4.dbo.BookingItem B WITH(NOLOCK) ON A.LineId=B.OrderLineId
								 INNER JOIN Ligentix4.dbo.BookingHeader C WITH(NOLOCK) ON B.BookingId=C.BookingId
								 INNER JOIN Ligentix_Purchase_Orders.dbo.SourceFile G WITH(NOLOCK) ON G.FileID = M.FileID
								 INNER JOIN Ligentix_Purchase_Orders.dbo.ClientFeed H WITH(NOLOCK) ON H.FeedID = G.FeedID
								 INNER JOIN Ligentix_Purchase_Orders.dbo.Client I WITH(NOLOCK) ON I.ClientID = H.ClientID
								 INNER JOIN Ligentix4.dbo.Client CL WITH(NOLOCK) ON I.LigClientId = CL.ClientId
								 INNER JOIN 
									(SELECT DISTINCT OrderNumber,ClientId,Lot FROM [BI_Builder].dbo.ShipOrder)
								 SH ON SH.OrderNumber= M.OrderNumber AND SH.ClientId=I.LigClientId AND (CASE SH.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(SH.Lot,'1') END) = (CASE A.Lot WHEN '' THEN '1' WHEN '0' THEN '1' ELSE ISNULL(A.Lot,'1') END)
						)PO_OH ON LEFT(JD.JD_OrderNumber,30) COLLATE Latin1_General_CI_AS = PO_OH.OrderNumber AND OH.OH_CODE COLLATE Latin1_General_CI_AS = PO_OH.ConsigneeEDICode
								--AND PO_OH.ItemCode = JO.JO_Partno COLLATE Latin1_General_CI_AS  
					GROUP BY 
						LEFT(JD.JD_OrderNumber,30),
						PO_OH.ClientId,
						PO_OH.OrderNumber,
						PO_OH.LOT,
						JW.JW_RL_NKLoadPort,
						JC.JC_DeliveryMode,
						JS.JS_E_Arv,
						JS.JS_E_DEP,
						JS_RL_NKDestination,
						JS.JS_PackingMode,
						JA.JA_RL_NKPortOfLoading,
						OH.OH_FullName,
						OH.OH_CODE,
						RC.RC_Code,
						UN.RL_PortName,
						JS.JS_PK,
						JS.JS_UniqueConsignRef,
						JC.JC_ContainerNum
				)M
			ORDER BY 
				ClientId, OrderNumber, LOT