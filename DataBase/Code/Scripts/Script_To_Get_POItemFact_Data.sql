
USE BI_Builder
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects where name ='BIPOItemFact')
BEGIN
	CREATE TABLE BIPOItemFact
	(
		Customer		VARCHAR(100) NOT NULL, 
		ItemCode		VARCHAR(100) NOT NULL, 
		LineNum			VARCHAR(100) NOT NULL, 
		ItemName		VARCHAR(1000) NOT NULL, 
		OrderNumber		VARCHAR(100) NOT NULL,
		Lot				VARCHAR(10), 
		Cost			FLOAT, 
		Quantity		FLOAT, 
		[Weight]		FLOAT, 
		CBM				FLOAT, 
		CONSTRAINT PK_BIPOItemFact_Customer_OrderNumber_Lot_ItemName_ItemCode_LineNum PRIMARY KEY (Customer, ItemCode,LineNum, ItemName, OrderNumber, Lot)
	)
END 
GO

	INSERT INTO BIPOItemFact(Customer,ItemCode,LineNum,ItemName, OrderNumber,Lot, Cost, Quantity,[Weight],CBM)
	SELECT 
		POH.Customer, 
		ISNULL(OL.ItemCode, 'N/A'),
		ISNULL(OL.LineNum, 'N/A'),
		ISNULL(OL.[Description], 'N/A'),
		OH.OrderNumber, 
		ISNULL(OL.Lot,'1'), 
		ISNULL(OL.ItemPrice, 0), 
		ISNULL(OL.OrderQty, 0),
		ISNULL(OL.UnitWeight, 0), 
		ISNULL(OL.UnitVolume, 0)	
	FROM 
		[Ligentix_Purchase_Orders].dbo.OrderHeader OH 
		JOIN [Ligentix_Purchase_Orders].dbo.ClientFeed CF ON OH.OrderFeedId=CF.FeedId
		JOIN [Ligentix_Purchase_Orders].dbo.Client POC ON CF.ClientId=POC.ClientId
		JOIN Ligentix4.dbo.Client CL ON POC.LigClientId=CL.ClientId
		JOIN [Ligentix_Purchase_Orders].dbo.OrderLine OL ON OH.HeaderId=OL.HeaderId 
		JOIN [BI_Builder].dbo.BIPurchaseOrder POH ON OH.OrderNumber=POH.PONumber AND POH.Customer=CL.ConsigneeEDICode
		AND ISNULL(NULLIF(OL.LOT, ''), 1)=POH.Lot
	WHERE
		OL.IsCancelled=0
		AND OH.IsLatestVersion=1
		AND OL.[LineStatus]!='CANCELLED'
		AND ((POH.Customer IN ('SOAKCOGBNTN', 'WAITROGBLON') AND  OL.UnitVolume != 0)
		OR POH.Customer NOT IN('SOAKCOGBNTN', 'WAITROGBLON'))

	--AND OH.OrderNumber='229237'

	--SELECT  * FROM [Ligentix_Purchase_Orders].dbo.OrderLine where headerid=696180

	--TEST QUERY 
	SELECT 
		POH.Customer, 
		OL.ItemCode,
		--OL.LineNum,
		--ol.[Description],
		OH.OrderNumber, 
		OL.Lot,
		--MAX(OL.UnitVolume),
		COUNT(1)
	FROM 
		[Ligentix_Purchase_Orders].dbo.OrderHeader OH 
		JOIN [Ligentix_Purchase_Orders].dbo.ClientFeed CF ON OH.OrderFeedId=CF.FeedId
		JOIN [Ligentix_Purchase_Orders].dbo.Client POC ON CF.ClientId=POC.ClientId
		JOIN Ligentix4.dbo.Client CL ON POC.LigClientId=CL.ClientId
		JOIN [Ligentix_Purchase_Orders].dbo.OrderLine OL ON OH.HeaderId=OL.HeaderId 
		JOIN [BI_Builder].dbo.BIPurchaseOrder POH ON OH.OrderNumber=POH.PONumber AND POH.Customer=CL.ConsigneeEDICode
		AND ISNULL(NULLIF(OL.LOT, ''), 1)=POH.Lot
	WHERE
		OL.IsCancelled=0
		AND OH.IsLatestVersion=1
		and OL.[LineStatus]!='CANCELLED'
	AND ((POH.Customer IN ('SOAKCOGBNTN', 'WAITROGBLON') AND  OL.UnitVolume != 0)
	OR POH.Customer NOT IN('SOAKCOGBNTN', 'WAITROGBLON'))
	GROUP BY 
		POH.Customer, 
		OL.ItemCode,
		OH.OrderNumber, 
		OL.Lot 
		--OL.LineNum,
		--ol.[Description]
		--OL.UnitVolume 
	HAVING 
		COUNT(1) > 1
	ORDER BY 
		OH.OrderNumber



	SELECT oh.HeaderId,
		POH.Customer, 
		OL.ItemCode,
		OH.OrderNumber, 
		OL.Lot
	FROM 
		[Ligentix_Purchase_Orders].dbo.OrderHeader OH 
		JOIN [Ligentix_Purchase_Orders].dbo.ClientFeed CF ON OH.OrderFeedId=CF.FeedId
		JOIN [Ligentix_Purchase_Orders].dbo.Client POC ON CF.ClientId=POC.ClientId
		JOIN Ligentix4.dbo.Client CL ON POC.LigClientId=CL.ClientId
		JOIN [Ligentix_Purchase_Orders].dbo.OrderLine OL ON OH.HeaderId=OL.HeaderId 
		JOIN [BI_Builder].dbo.BIPurchaseOrder POH ON OH.OrderNumber=POH.PONumber AND POH.Customer=CL.ConsigneeEDICode
		AND ISNULL(NULLIF(OL.LOT, ''), 1)=POH.Lot
	WHERE
		OL.IsCancelled=0
		AND OH.IsLatestVersion=1
		AND OH.OrderNumber='273401'
		and ol.ItemCode='0035006'

	SELECT  * FROM [Ligentix_Purchase_Orders].dbo.OrderLine where headerid=2577897
	AND ItemCode='1439405'
	-- and linenum='876778' and [description]='WR 10 GOLD EMBOSSED STAGS CHARITY C 10x1each'

	SELECT * FROM [Ligentix_Purchase_Orders].dbo.OrderHeader WHERE OrderNumber='10751'
	and IsLatestVersion=1



	SELECT TOP 10 * FROM BIPOItemFact WHERE OrderNumber='1040014279-10406192099778'